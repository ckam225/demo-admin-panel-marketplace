module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}",],
  theme: {
    extend: {
      height: {
        '100p': '100px',
      },
      maxHeight: {
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
        '100px': '100px',
        200: '200px',
      },
      maxWidth: {
        100: '100px',
        200: '200px',
        300: '300px',
        400: '400px',
      },
      colors: {
        'blue-fonce': '#050D2E',
        'blue-accent': '#0096c7',
        'red-05': '#e30613',
        'black-10': 'rgba(0,0,0,0.1)',
        'black-20': 'rgba(0,0,0,0.2)',
        'black-30': 'rgba(0,0,0,0.3)',
        'black-40': 'rgba(0,0,0,0.4)',
        'black-50': 'rgba(0,0,0,0.5)',
        'white-5': 'rgba(255,255,255,0.05)',
        'white-10': 'rgba(255,255,255,0.1)',
        'white-20': 'rgba(255,255,255,0.2)',
        'white-30': 'rgba(255,255,255,0.3)',
        'white-40': 'rgba(255,255,255,0.4)',
        'white-50': 'rgba(255,255,255,0.5)',
        'indigo-1': 'rebeccapurple'
      },
      borderColor: {
        'red-05': '#e30613',
      },
      textColor: {
        'red-05': '#e30613',
        'blue-fonce': '#050D2E',
        'blue-accent': '#0096c7',
      },
    },
  },
  variants: {
    extend: {},
    display: ['group-hover'],
  },
  plugins: [require('@tailwindcss/forms')],
}
