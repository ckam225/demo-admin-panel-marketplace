FROM node:14-alpine AS build

ARG API_URL ${API_URL}

ARG  REACT_APP_API_URL ${REACT_APP_API_URL}

ARG  REACT_APP_WEBSOCKET_URL ${WEBSOCKET_URL}

RUN echo ${API_URL}

RUN echo ${REACT_APP_API_URL}

RUN echo ${REACT_APP_WEBSOCKET_URL}

WORKDIR /var/www/html

COPY ./package.json ./yarn.lock ./

RUN yarn install

COPY ./ /var/www/html

RUN yarn build


FROM nginx:stable-alpine as production

COPY --from=build /var/www/html/build /usr/share/nginx/html
COPY --from=build /var/www/html/nginx/nginx.conf /etc/nginx/nginx.conf

EXPOSE 80

ENTRYPOINT [ "nginx", "-g", "daemon off;" ]



