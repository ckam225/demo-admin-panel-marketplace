import { atom, selector } from "recoil"; import storage from '../local'

export const isAuthState = atom({
    key: "is-auth-state",
    default: storage.exists('token'),
});

export const authUserState = atom({
    key: "auth-user-state",
    default: storage.get('user'),
});

export const hideSidebarState = atom({
    key: "sidebar-state",
    default: false,
});

export const toastState = atom({
    key: "toast-state",
    default: false,
});

export const userRoleState = selector({
    key: "is-superuser-state",
    get: ({ get }) => {
        const user = get(authUserState)
        return {
            isSuperuser: user?.role?.name === 'owner',
            isAdmin: user?.role?.name === 'admin'
        }
    }
});
