import { atom } from "recoil";

export const listSyncState = atom({
    key: "sync-list-state",
    default: [],
});

export const currentSyncState = atom({
    key: "current-sync-state",
    default: {},
});