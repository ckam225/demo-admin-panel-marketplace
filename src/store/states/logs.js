import { atom } from "recoil";

export const listLogState = atom({
    key: "list-log-state",
    default: [],
});

export const currentLogState = atom({
    key: "current-log-state",
    default: {},
});