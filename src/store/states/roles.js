import { atom } from "recoil";

export const listRoleState = atom({
    key: "role-list-state",
    default: [],
});

export const currentRoleState = atom({
    key: "current-role-state",
    default: {},
});