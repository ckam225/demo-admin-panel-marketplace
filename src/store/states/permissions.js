import { atom } from "recoil";

export const listPermissionState = atom({
    key: "permission-list-state",
    default: [],
});

export const currentPermissionState = atom({
    key: "current-permission-state",
    default: {},
});