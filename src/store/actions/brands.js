import { useState } from "react";
import * as brandService from "../../api/brands";
import {toast} from "react-toastify";

export const useBrands = () => {
    const [brands, setBrands] = useState([])
    // eslint-disable-next-line no-unused-vars
    const [pageCount, setPageCount] = useState(0)
    const [loading, setLoading] = useState(false)
    const [currentBrand, setCurrentBrand] = useState(null)

    async function fetchBrands(filterParams = null) {
        setLoading(true)
        const res = await brandService.searchBrands(filterParams)
        setLoading(false)
        if (!res.error) {
            setBrands(res.data || [])
            // setPageCount(res.data?.pageCount || 0)
        }
        else
            console.error(res.data);
    }

    async function fetchCurrentBrand(id) {
        const res = await brandService.findBrandById(id)
        if (!res.error) {
            setCurrentBrand(res.data || {})
        }
        else
            console.error(res.data);
    }

    async function createBrand(formData) {
        setLoading(true)
        const response = await brandService.create(formData)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
        }
        else {
            setBrands(brands.concat([response.data]))
            return true
        }
    }

    async function updateBrand(brandId, formData) {
        setLoading(true)
        const response = await brandService.update(brandId, formData)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
            return false
        }
        else
            return true
    }

    async function deleteBrand(brandId) {
        setLoading(true)
        const response = await brandService.destroy(brandId)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
        }
        else {
            setBrands(brands.filter(b => b.id !== brandId))
            return true
        }
    }

    return {
        brands,
        pageCount,
        loading,
        fetchBrands,
        fetchCurrentBrand,
        createBrand,
        updateBrand,
        deleteBrand,
        currentBrand,
        setCurrentBrand,
    }
}

export default useBrands;