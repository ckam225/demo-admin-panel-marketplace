import { useState } from "react";
import * as api from "../../api/properties";
import React from "react";

const useProperties = () => {
    const [properties, setProperties] = useState([])
    // eslint-disable-next-line no-unused-vars
    const [pageCount, setPageCount] = useState(0)
    const [loading, setLoading] = useState(false)
    const [currentProperty, setCurrentProperty] = useState(null)

    async function fetchProperties(filterParams = '') {
        setLoading(true)
        const res = await api.fetchAllProperties(filterParams)
        setLoading(false)
        if (!res.error) {
            setProperties(res.data ?? [])
            // setPageCount(res.data?.pageCount ?? 0)
        }
        else
            console.error(res.data);
    }

    async function fetchCurrentProperty(propertyId) {
        const res = await api.findProperty(propertyId)
        if (!res.error) {
            setCurrentProperty(res.data ?? [])
            // setPageCount(res.data?.pageCount ?? 0)
        }
        else
            console.error(res.data);
    }

    return {
        properties,
        pageCount,
        loading,
        currentProperty,
        fetchProperties,
        fetchCurrentProperty,
        setCurrentProperty,
    }
}

const useEditProperty = () => {
    const [currentProperty, setCurrentProperty] = useState(null)
    const [types, setTypes] = useState([])

    async function fetchCurrentProperty(propertyId) {
        const res = await api.findProperty(propertyId)
        if (!res.error) {
            setCurrentProperty(res.data ?? [])
            // setPageCount(res.data?.pageCount ?? 0)
        }
        else
            console.error(res.data);
    }

    async function fetchTypes() {
        const res = await api.fetchTypes()
        if (!res.error) {
            setTypes(res.data ?? [])
            // setPageCount(res.data?.pageCount ?? 0)
        }
        else
            console.error(res.data);
    }

    async function updateProperty(propertyId, payload) {
        const res = await api.updateProperty(propertyId, payload)
        if (!res.error) {
            return true;
        }
        else
            console.error(res.data);
    }

    async function createProperty(payload) {
        const res = await api.storeProperty(payload)
        if (!res.error) {
            return res.data;
        }
        else
            console.error(res.data);
    }

    React.useEffect(() => {
        fetchTypes()
    }, [])

    return {
        currentProperty,
        fetchCurrentProperty,
        types,
        fetchTypes,
        updateProperty,
        createProperty,
    }
}

const useEditPropertyValues = () => {
    const [loading, setLoading] = useState(false)
    const [values, setValues] = useState([])

    async function fetchValues(propertyId) {
        setLoading(true)
        const res = await api.fetchPropertyValues(propertyId)
        setLoading(false)
        if (!res.error) {
            setValues(res.data)
        }
        else
            console.error(res.data);
    }

    async function updateValue(propertyId, valueId, payload) {
        const res = await api.updatePropertyValue(propertyId, valueId, payload)
        if (!res.error) {
            return true
        }
        else
            console.error(res.data);
    }

    async function deleteValue(propertyId, valueId) {
        const res = await api.deletePropertyValue(propertyId, valueId)
        if (!res.error) {
            setValues(values.filter(v => v.id !== valueId))
        }
        else
            console.error(res.data);
    }

    async function createValue(propertyId, payload) {
        const res = await api.storePropertyValue(propertyId, payload)
        if (!res.error) {
            setValues(values.concat([res.data]))
            return true
        }
        else
            console.error(res.data);
    }

    return {
        loading,
        values,
        fetchValues,
        updateValue,
        deleteValue,
        createValue,
    }
}

export { useProperties, useEditProperty, useEditPropertyValues }