import React from 'react';
import { useRecoilState } from 'recoil'
import { listRoleState, currentRoleState } from '../states/roles'
import * as roleAPI from '../../api/roles'
import { toast } from 'react-toastify'


export function useRoleList() {
    const [loading, setLoading] = React.useState(false);
    const [roles, setRoles] = useRecoilState(listRoleState);

    React.useEffect(() => {
        (async () => {
            setLoading(true)
            const response = await roleAPI.getRoles()
            if (response.error)
                toast.error(response.data.error ?? 'Some errors occured')
            else
                setRoles(response.data)

            setLoading(false)
        })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const updateRoleList = (payload) => {
        let currentRole = roles.find(role => role.id === payload.id)
        if (currentRole) {
            const newRoles = roles.map(role => {
                if (role.id === currentRole.id) {
                    return {
                        ...currentRole,
                        ...payload
                    }
                }
                return role
            })
            setRoles(newRoles)
        } else{
            setRoles([...roles, payload])
        }
    }

    return {
        loading,
        roles,
        updateRoleList
    };
}

export function useCurrentRole(id) {
    const [loading, setLoading] = React.useState(false);
    const [role, setRole] = useRecoilState(currentRoleState);

    React.useEffect(() => {
        (async () => {
            setLoading(true)
            const response = await roleAPI.findRole(id)
            if (response.error)
                toast.error(response.data.error ?? 'Some errors occured')
            else
                setRole(response.data)
            setLoading(false)
        })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    return [
        loading,
        role
    ];
}



export function useRole() {
    const [loading, setLoading] = React.useState(false);
    const [roles, setRoles] = useRecoilState(listRoleState);

    const createRole = async (formData) => {
        setLoading(true)
        const response = await roleAPI.createRole(formData)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error ?? 'Some errors occured')
        }
        else {
            setRoles(roles.concat(response.data))
            return true
        }

    }

    const updateRole = async (roleId, formData) => {
        setLoading(true)
        const response = await roleAPI.updateRole(roleId, formData)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error ?? 'Some errors occured')
            return false
        }
        else
            return true
    }

    const deleteRole = async (roleId) => {
        setLoading(true)
        const response = await roleAPI.destroyRole(roleId)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error ?? 'Some errors occured')
        }
        else {
            setRoles(roles.filter(r => r.id !== roleId))
            return true
        }
    }

    return {
        loading,
        createRole,
        updateRole,
        deleteRole
    };
}


export default useRoleList
