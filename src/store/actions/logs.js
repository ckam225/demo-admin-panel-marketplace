import React from 'react';
import { useRecoilState } from 'recoil'
import { listLogState } from '../states/logs'
import * as logApi from '../../api/logs'
import { toast } from 'react-toastify'


export function useLogs() {
    const [loading, setLoading] = React.useState(false);
    const [logs, setLogs] = useRecoilState(listLogState);

    React.useEffect(() => {
        fetchLogs()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    async function fetchLogs() {
        setLoading(true)
        const response = await logApi.getAllLogs()
        if (response.error)
            toast.error(response.data.error || 'Some errors occured')
        else
            setLogs(response.data)
        setLoading(false)
    }

    const truncate = async (logId) => {
        setLoading(true)
        const response = await logApi.truncateLog(logId)
        setLoading(false)
        if (response.error)
            toast.error(response.data.error || 'Some errors occured')
        else {
            fetchLogs()
            toast.success(`logId successfully cleaned`)
            return response.data
        }
    }

    return {
        loading,
        logs,
        truncate
    };
}


export default useLogs
