import { useState } from "react";
import * as api from "../../api/notification";

export const useNotifications = () => {
    const [currentNotification, setCurrentNotification] = useState()
    const [notifications, setNotifications] = useState([])
    const [loading, setLoading] = useState(false)

    async function fetchNotifications(query) {
        setLoading(true)
        const res = await api.getAllNotifications(query)
        setLoading(false)
        if (!res.error) {
            setNotifications(res.data || [])
        }
        else
            console.error(res.data);
    }

    async function fetchCurrentNotification(notificationId) {
        const res = await api.findNotification(notificationId)
        if (!res.error) {
            setCurrentNotification(res.data)
            updateNotification({ read_at: res.read_at }, notificationId)
        }
        else
            console.error(res.data);
    }

    async function markAsRead(notificationIds) {
        const res = await api.markNotificationAsRead(typeof notificationIds == 'object' ? notificationIds : [notificationIds])
        if (!res.error) {
            updateNotification({ read_at: res.read_at }, notificationIds)
        }
        else
            console.error(res.data);
    }

    async function deleteNotification(notificationIds) {
        const res = await api.deleteNotification(typeof notificationIds == 'object' ? notificationIds : [notificationIds])
        if (!res.error) {
            fetchNotifications()
        }
        else
            console.error(res.data);
    }

    /**
     * Обновление состояния массива уведомлений
     *
     * @param data Новые свойства уведомления
     * @param notificationId Индетификатор или массив с единтификаторами.
     * Если параметр не указан то обновление будет применено для всех уведомлений
     */
    function updateNotification(data, notificationId) {
        setNotifications(notifications.map(notify => {
            if (notificationId !== undefined) {
                if ((Array.isArray(notificationId) && notificationId.includes(notify.id))
                    || notify.id === notificationId) {
                    notify = Object.assign(notify, data)
                }
            } else
                notify = Object.assign(notify, data)
            return notify
        }))
    }

    return {
        notifications,
        setNotifications,
        loading,
        currentNotification,
        fetchNotifications,
        fetchCurrentNotification,
        markAsRead,
        updateNotification,
        deleteNotification,
    };
}

export default useNotifications;