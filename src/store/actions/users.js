import React from 'react';
import { useRecoilState } from 'recoil'
import { listUserState } from '../states/users'
import * as userApi from '../../api/users'
import { toast } from 'react-toastify'
import { navigate } from '@reach/router';



export function useUser() {

    const [users, setUsers] = useRecoilState(listUserState);
    const [loading, setLoading] = React.useState(false);
    // eslint-disable-next-line no-unused-vars
    const [currentUser, setCurrentUser] = React.useState();

    const fetchUsers = async () => {
        setLoading(true)
        const response = await userApi.getAllUsers()
        if (response.error)
            toast.error(response.data.error ?? 'Some errors occured')
        else
            setUsers(response.data)
        setLoading(false)
    }

    const fetchAnyUser = async (userId) => {
        setLoading(true)
        const response = await userApi.findUser(userId)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error ?? 'Some errors occured')
            navigate(`/${userId} NOT FOUND`)
        }
        else
            return response.data

    }

    const createUser = async (formData) => {
        setLoading(true)
        const response = await userApi.createUser(formData)
        setLoading(false)
        if (response.error)
            toast.error(response.data.error ?? 'Some errors occured')
        else
            return response.data
    }

    const updateUser = async (userId, formData) => {
        setLoading(true)
        const response = await userApi.updateUser(userId, formData)
        setLoading(false)
        if (response.error)
            toast.error(response.data.error || 'Some errors occured')
        else
            return response.data
    }

    const deleteUser = async (userId) => {
        setLoading(true)
        const response = await userApi.destroyUser(userId)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
            return false
        }
        return true
    }

    const getUserStatus = (user = currentUser) => {
        if (user?.is_blocked) return { color: 'bg-red-500', title: 'заблокировен' }
        if (user?.is_active) return { color: 'bg-green-500', title: 'активен' }
        return { color: 'bg-yellow-500', title: '' }
    }

    return {
        loading,
        currentUser,
        createUser,
        updateUser,
        deleteUser,
        getUserStatus,
        fetchUsers,
        users,
        fetchAnyUser
    };
}

export function useTestUser() {

    const [loading, setLoading] = React.useState(false);

    const createUser = async (formData) => {
        setLoading(true)
        const response = await userApi.createTestUser(formData)
        setLoading(false)
        if (response.error)
            toast.error(response.data.error || 'Some errors occured')
        else {
            toast.success('Пользователь(-ли) успешно созданы')
            return response.data
        }
    }

    return {
        createUser,
        loading,
    };
}

export default useUser
