import { useEffect, useState } from "react";
import * as api from "../../api/products";
import { toast } from "react-toastify"
import { navigate } from '@reach/router'


export const useActionStatuses = (storeId = null) => {
  const [statuses, setStatuses] = useState([])

  useEffect(() => {
    setTimeout(async () => {
      const q = storeId ? `?store_id=${storeId}` : ''
      const res = await api.getProductsByStatus(q)
      if (!res.error)
        setStatuses(res.data || [])
      else
        console.error(res.data);
    }, 0)
  }, [storeId])

  return {
    statuses,
  }
}

export const useProducts = () => {
  const [products, setProducts] = useState([])
  const [statuses, setStatuses] = useState([])
  const [currentProduct, setCurrentProduct] = useState()
  const [pageCount, setPageCount] = useState(0)
  const [loading, setLoading] = useState(false)
  const [moderationAction, setModerationAction] = useState();
  const [currentProductChanges, setCurrentProductChanges] = useState([])

  async function fetchProducts(filterParams = null) {
    setLoading(true)
    const res = await api.getAllProduct(filterParams)
    setLoading(false)
    if (!res.error) {
      setProducts(res.data?.data || [])
      setPageCount(res.data?.pageCount || 0)
    }
    else
      console.error(res.data);
  }

  async function fetchCurrentProduct(productId) {
    const res = await api.findProduct(productId)
    if (!res.error)
      setCurrentProduct(res.data)
    else
      console.error(res.data);
  }

  async function fetchProductStatuses(productId) {
    const res = await api.getProductsByStatus(productId)
    if (!res.error)
      setStatuses(res.data || [])
    else
      console.error(res.data);
  }


  async function moderateProduct(productId, status, exceptions) {
    const res = await api.updateProduct(productId, {
      status: status,
      exceptions: exceptions
    })
    if (!res.error) {
      setCurrentProduct(res.data)
      toast.success('Статус товара успешно обновлен');
      navigate(`/moderation`);
    } else
      console.error(res.data);
  }


  async function fetchProductChanges(productId) {
    const res = await api.getProductChanges(productId)
    if (!res.error)
      setCurrentProductChanges(res.data)
    else
      console.error(res.data);
  }

  return {
    fetchProducts,
    fetchCurrentProduct,
    fetchProductStatuses,
    statuses,
    products,
    currentProduct,
    pageCount,
    loading,
    moderationAction,
    setModerationAction,
    moderateProduct,
    fetchProductChanges,
    currentProductChanges
  }
}

export default useProducts;