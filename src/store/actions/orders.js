import { useEffect, useState } from "react"
import api from "../../api/base"


export const useOrders = () => {
  const [latestOrders, setLatestOrders] = useState([])

  useEffect(() => {
    (async () => {
      const result = await api.get('/admin/orders/latest?per_page=10')
      setLatestOrders(result.data || [])
    })()
  }, [])

  return {
    latestOrders
  }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  useOrders
}