import React from 'react';
import { useRecoilState } from 'recoil'
import { listPermissionState, currentPermissionState } from '../states/permissions'
import *  as permissionApi from '../../api/permissions'
import { toast } from 'react-toastify'


export function usePermissions() {
    const [loading, setLoading] = React.useState(false);
    const [permissions, setPermissions] = useRecoilState(listPermissionState);

    React.useEffect(() => {
        (async () => {
            setLoading(true)
            const response = await permissionApi.getAllPermissions()
            if (response.error)
                toast.error(response.data.error || 'Some errors occured')
            else
                setPermissions(response.data)
            setLoading(false)
        })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const updatePermissionList = (payload) => {
        if (payload) {
            let currentPerm = permissions.find(perm => perm.id === payload.id)
            if (currentPerm) {
                const newPerms = permissions.map(perm => {
                    if (perm.id === currentPerm.id) {
                        return {
                            ...currentPerm,
                            ...payload
                        }
                    }
                    return perm
                })
                setPermissions(newPerms)
            } else{
                setPermissions([...permissions, payload])
            }
        }
    }

    return {
        loading,
        permissions,
        updatePermissionList
    };
}

export function useCurrentPermission(id) {
    const [loading, setLoading] = React.useState(false);
    const [permission, setUser] = useRecoilState(currentPermissionState);

    React.useEffect(() => {
        (async () => {
            setLoading(true)
            const response = await permissionApi.findPermission(id)
            if (response.error)
                toast.error(response.data.error || 'Some errors occured')
            else
                setUser(response.data)
            setLoading(false)
        })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    return [
        loading,
        permission
    ];
}


export function usePermission() {
    const [loading, setLoading] = React.useState(false);

    const createPermission = async (formData) => {
        setLoading(true)
        const response = await permissionApi.createPermission(formData)
        setLoading(false)
        if (response.error)
            toast.error(response.data.error || 'Some errors occured')
        else
            return response.data
    }

    const updatePermission = async (permissionId, formData) => {
        setLoading(true)
        const response = await permissionApi.updatePermission(permissionId, formData)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
            return false
        }
        else
            return true
    }

    const deletePermission = async (permissionId) => {
        setLoading(true)
        const response = await permissionApi.destroyPermission(permissionId)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
            return false
        }
        else
            return true
    }

    return {
        loading,
        createPermission,
        updatePermission,
        deletePermission
    };
}


export default usePermissions
