import { useEffect, useState } from "react"
import api from "../../api/base"


export const useStats = () => {
  const [storeReport, setStoreReport] = useState(0)
  const [orderReport, setOrderReport] = useState()

  useEffect(() => {
    setTimeout(async () => {
      const result = await api.get('/admin/stores/reports')
      setStoreReport(result.data)
    }, 0)
    setTimeout(async () => {
      const result = await api.get('/admin/orders/reports')
      setOrderReport(result.data)
    }, 0)
  }, [])

  return {
    storeReport,
    orderReport
  }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  useStats
}