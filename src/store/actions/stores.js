import { navigate } from "@reach/router"
import { useEffect, useMemo, useState } from "react"
import * as storeApi from "../../api/stores"
import * as orderApi from "../../api/orders"
import * as amountApi from "../../api/amounts"
import { toast } from "react-toastify"
import useStatuses from "./statuses"

export const storeStatus = {
  WAITLEGAL: 11,
  WAITCHECKING: 12,
  WAITFIXING: 13,
  BLOCKED: 14,
  ACTIVATED: 15
}

export const useLatestStores = () => {
  const [latestStores, setLatestStores] = useState([])

  useEffect(() => {
    (async () => {
      const result = await storeApi.latestStores()
      if (result.error) {
        toast.error(result.data?.error || 'some errors occured')
      } else {
        setLatestStores(result.data || [])
      }
    })()
  }, [])

  return {
    latestStores
  }
}

export const useStoreStatusClass = (currentStore) => {

  const statusClass = useMemo(() => {
    if (currentStore?.status?.name === 'activated')
      return 'bg-green-400 text-white'
    if (currentStore?.status?.name === 'blocked')
      return 'bg-red-400 text-white'
    if (currentStore?.status?.name === 'wait_checking')
      return 'bg-yellow-400 text-white'
    return 'bg-gray-200'
  }, [currentStore])

  const statusTextClass = useMemo(() => {
    if (currentStore?.status?.name === 'activated')
      return 'text-green-500'
    if (currentStore?.status?.name === 'blocked')
      return 'text-red-500'
    if (currentStore?.status?.name === 'wait_checking')
      return 'text-yellow-400'
    return 'text-black'
  }, [currentStore])

  return {
    statusClass,
    statusTextClass
  }
}

export const useStores = () => {
  const [stores, setStores] = useState([])
  const [pageCount, setPageCount] = useState(0)
  const [loading, setLoading] = useState(false)
  const [currentStore, setCurrentStore] = useState()



  async function fetchStores(queries = '') {
    setLoading(true)
    const result = await storeApi.getAllStores(queries)
    if (result.error) {
      toast.error(result.data?.error || 'some errors occured')
    } else {
      setStores(result.data?.data ?? [])
      setPageCount(result.data?.pageCount ?? 1)
    }
    setLoading(false)
  }

  async function findStore(storeId) {
    const result = await storeApi.findStore(storeId)
    if (result.status === 404) {
      navigate('/404')
    } else {
      if (result.error) {
        toast.error(result.data?.error || 'some errors occured')
      } else {
        setCurrentStore(result.data)
      }
    }
  }

  async function partialUpdateStore(storeId, payload) {
    const result = await storeApi.partialUpdateStore(storeId, payload)
    if (result.error) {
      toast.error(result.data?.error || 'some errors occured')
    } else {
      setCurrentStore(result.data)
      toast.success("Данные успешно обновлены")
    }
  }

  async function createStore(formData) {
    setLoading(true)
    const result = await storeApi.createStore(formData)
    if (result.error) {
      toast.error(result.data?.error || 'some errors occured')
    } else {
      fetchStores()
    }
    setLoading(false)
    return true
  }

  return {
    stores,
    fetchStores,
    findStore,
    currentStore,
    loading,
    pageCount,
    createStore,
    partialUpdateStore
  }
}

export const useStoreOrders = () => {
  const [orders, setOrders] = useState([])
  const [pageCount, setPageCount] = useState(0)
  const [loading, setLoading] = useState(false)

  async function fetchOrders(queries) {
    setLoading(true)
    const result = await orderApi.getAllOrders(queries)
    setLoading(false)
    if (result.error) {
      toast.error(result.data?.error || 'some errors occured')
    } else {
      setOrders(result.data?.data || [])
      setPageCount(result.data?.pageCount || 0)
    }
  }

  return {
    orders,
    fetchOrders,
    pageCount,
    loading
  }

}


export const useStoreAmounts = () => {
  const [amounts, setAmounts] = useState([])
  const [pageCount, setPageCount] = useState(0)
  const [loading, setLoading] = useState(false)

  async function fetchAmounts(queries) {
    setLoading(true)
    const result = await amountApi.getAllAmounts(queries)
    setLoading(false)
    if (result.error) {
      toast.error(result.data?.error || 'some errors occured')
    } else {
      setAmounts(result.data?.data || [])
      setPageCount(result.data?.pageCount || 0)
    }
  }


  return {
    amounts,
    fetchAmounts,
    pageCount,
    loading
  }

}

export const useActiveStores = () => {
  const [stores, setStores] = useState([])

  useEffect(() => {
    setTimeout(async () => {
      const res = await storeApi.getAllStores()
      if (!res.error)
        setStores(res.data || [])
      else
        console.error(res.data);
    }, 0)
  }, [])

  return {
    stores,
  }
}

export const useStoreStatuses = () => {
  return useStatuses('?type=stores')
}

export default useStores