
import { useRecoilState } from 'recoil'
import { isAuthState, authUserState } from '../states/base'
import * as authApi from '../../api/auth'
import { toast } from 'react-toastify'
import { useState } from 'react'

export const useAuthentication = () => {
    const [isAuthenticated, setIsAuth] = useRecoilState(isAuthState)
    return {
        isAuthenticated,
        setIsAuth
    }
}

export const useAuthUser = () => {
    // eslint-disable-next-line no-unused-vars
    const [loading, setLoading] = useState(false);
    const [user, setAuthUser] = useRecoilState(authUserState)
    const isSuperuser = user && user?.role?.name === 'owner'
    const isAdmin = user && user?.role?.name === 'admin'

    const logout = async () => {
        setLoading(true)
        const response = await authApi.logout()
        setLoading(false)
        if (!response.error)
            return true
        toast.error(response.data?.error || 'Some errors occured')
    }

    return {
        user,
        setAuthUser,
        isSuperuser,
        isAdmin,
        logout
    }
}




export default useAuthUser