import {useState} from "react";
import * as api from "../../api/tariffs";
import {toast} from "react-toastify";

const useTariffs = () => {
    const [tariffs, setTariffs] = useState([])
    // eslint-disable-next-line no-unused-vars
    const [pageCount, setPageCount] = useState(0)
    const [loading, setLoading] = useState(false)
    const [currentTariff, setCurrentTariff] = useState(null)

    async function fetchTariffs(filterParams = '') {
        setLoading(true)
        const res = await api.fetchAllTariffs(filterParams)
        setLoading(false)
        if (!res.error) {
            setTariffs(res.data || [])
            // setPageCount(res.data?.pageCount || 0)
        }
        else{
            console.error(res.data);
        }
            
    }

    async function fetchCurrentTariff(id) {
        const res = await api.findTariffById(id)
        if (!res.error) {
            setCurrentTariff(res.data || {})
        }
        else{
            console.error(res.data);
        }
    }

    async function createTariff(formData) {
        setLoading(true)
        const response = await api.createTariff(formData)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
        }
        else {
            setTariffs(tariffs.concat([response.data]))
            return true
        }
    }

    async function updateTariff(tariffId, formData) {
        setLoading(true)
        const response = await api.updateTariff(tariffId, formData)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
            return false
        }
        else
            return true
    }

    async function deleteTariff(tariffId) {
        setLoading(true)
        const response = await api.destroyTariff(tariffId)
        setLoading(false)
        if (response.error) {
            toast.error(response.data.error || 'Some errors occured')
        }
        else {
            setTariffs(tariffs.filter(b => b.id !== tariffId))
            return true
        }
    }

    return {
        tariffs,
        pageCount,
        loading,
        currentTariff,
        fetchTariffs,
        fetchCurrentTariff,
        createTariff,
        updateTariff,
        deleteTariff,
        setCurrentTariff,
    }
}

export { useTariffs }