import { useEffect, useState } from "react"
import * as statusApi from "../../api/statuses"


export const useStatuses = (queries = '') => {
  const [statuses, setStatuses] = useState([])
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setTimeout(async () => {
      setLoading(true)
      const result = await statusApi.getAllStatus(queries)
      setStatuses(result.data ?? [])
      setLoading(false)
    }, 0);
  }, [queries])

  return {
    statuses,
    loading
  }
}


export default useStatuses