import {useState, useEffect} from "react";
import * as api from "../../api/categories";

export const useCategories = () => {
    const [categories, setCategories] = useState([])
    // eslint-disable-next-line no-unused-vars
    const [pageCount, setPageCount] = useState(0)
    const [loading, setLoading] = useState(false)
    const [currentCategory, setCurrentCategory] = useState(null)
    const [currentCategoryProps, setCurrentCategoryProps] = useState(null)

    async function fetchCategories(filterParams = '') {
        setLoading(true)
        const res = await api.searchCategories(filterParams)
        setLoading(false)
        if (!res.error) {
            setCategories(res.data || [])
            // setPageCount(res.data?.pageCount || 0)
        }
        else{
            console.error(res.data);
        }
    }

    async function fetchCurrentCategory(categoryId) {
        const res = await api.findCategoryById(categoryId)
        if (!res.error) {
            setCurrentCategory(res.data || [])
            // setPageCount(res.data?.pageCount || 0)
        }
        else{
            console.error(res.data);
        }
    }

    async function fetchCurrentCategoryProps(categoryId) {
        const res = await api.findCategoryByIdWithProps(categoryId)
        if (!res.error) {
            setCurrentCategoryProps(res.data || [])
            // setPageCount(res.data?.pageCount || 0)
        }
        else{
            console.error(res.data);
        }
            
    }

    return {
        categories,
        pageCount,
        loading,
        fetchCategories,
        fetchCurrentCategory,
        currentCategory,
        fetchCurrentCategoryProps,
        currentCategoryProps,
    }
}

export const useSearchCategories = () => {
    const [searchCategories, setSearchCategories] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        fetchSearchCategories()
    }, [])

    async function fetchSearchCategories(filterParams = '') {
        setLoading(true)
        const res = await api.fetchAllCategories(filterParams)
        setLoading(false)
        if (!res.error) {
            setSearchCategories(res.data || [])
            // setPageCount(res.data?.pageCount || 0)
        }
        else
            console.error(res.data);
    }

    return {
        searchCategories,
        loading,
        fetchSearchCategories,
    }
}

export const useEditCategory = () => {
    const [properties, setProperties] = useState([])
    const [loading, setLoading] = useState(false)

    async function fetchCategoryProperties(catId, filterParams = '') {
        setLoading(true)
        const res = await api.fetchCategoryProperties(catId, filterParams)
        setLoading(false)
        if (!res.error) {
            setProperties(res.data || [])
            // setPageCount(res.data?.pageCount || 0)
        }
        else
            console.error(res.data);
    }

    async function deleteCategoryProperty(catId, propId) {
        setLoading(true)
        const res = await api.deleteCategoryProperty(catId, propId)
        setLoading(false)
        if (!res.error) {
            setProperties(properties.filter(p => p.id !== propId))
            return true
        }
        else
            console.error(res.data);
    }

    async function addCategoryProperty(catId, prop) {
        setLoading(true)
        const res = await api.addCategoryProperty(catId, [prop.id])
        setLoading(false)
        if (!res.error) {
            setProperties(properties.concat([prop]))
            return true
        }
        else
            console.error(res.data);
    }

    return {
        properties,
        loading,
        fetchCategoryProperties,
        deleteCategoryProperty,
        addCategoryProperty,
    }
}

export const useCategoriesHasProps = (queries = '') => {
    const [categories, setCategories] = useState([])

    useEffect(() => {
       setTimeout(async () => {
            const res = await api.fetchAllCategoriesHasProps(queries)
            if (!res.error)
                setCategories(res.data?.data || [])
            else
                console.error(res.data);
       }, 0)
    }, [queries])

    return {
        categories,
    }
}
