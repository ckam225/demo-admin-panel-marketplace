import { useState } from "react";
import { useRecoilState } from "recoil";
import api from "../../api/base"
import { toast } from "react-toastify";
import { currentSyncState, listSyncState } from "../states/syncs";

const useSync = () => {

    const [loading, setLoading] = useState(false);
    const [syncs, setSyncs] = useRecoilState(listSyncState);
    const [sync, setSync] = useRecoilState(currentSyncState);
    const [pageCount, setPageCount] = useState(0)

    async function fetchSyncs(query = null) {
        setLoading(true)
        try {
            const q = query || '';
            const response = await api.get(`/admin/sync${q}`);
            setSyncs(response.data?.data || [])
            setPageCount(response.data?.pageCount || 0)
        } catch (e) {
            toast.error(e.response.data?.error)
        } finally {
            setLoading(false)
        }
    }

    async function getSync(syncId) {
        setLoading(true)
        try {
            const response = await api.get(`/admin/sync/${syncId}`);
            setSync(response.data)
        } catch (e) {
            toast.error(e.response.data?.error)
        } finally {
            setLoading(false)
        }
    }

    async function updateSync(syncId, payload) {
        setLoading(true)
        try {
            const response = await api.patch(`/admin/sync/${syncId}`, {
                payload: payload,
                status: 'pending'
            });
            setSync(response.data)
        } catch (e) {
            toast.error(e.response.data?.error)
        } finally {
            setLoading(false)
        }
    }

    async function deleteSync(syncId) {
        setLoading(true)
        try {
            await api.delete(`/admin/sync/${syncId}`);
            return true;
        } catch (e) {
            toast.error(e.response.data?.error)
        } finally {
            setLoading(false)
        }
    }

    return {
        syncs,
        loading,
        sync,
        fetchSyncs,
        getSync,
        updateSync,
        deleteSync,
        pageCount
    }
}

export default useSync