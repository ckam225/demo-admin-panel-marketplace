import * as Yup from "yup";


export const productInitialValue = {
    name: '',
    slug: '',
    barcode: '',
    description: '',
    price: '',
    weight: '',
    length: '',
    height: '',
    width: '',
    vat: '',
    isvisible: '',
    archived: '',
    quantity: '',
    brand_id: '',
    category_id: '',
    exceptions: '',
    images: '',
    properties: ''
}

export const legalInitialValue = {
    form: "ИП",
    address: "",
    director: "",
    inn: "",
    bik: "",
    kpp: "",
    ogrn: "",
    ogrnip: "",
    taxation_system: "ОСНО",
    payment_account: ""
}

export const storeInitialValue = {
    name: "",
    contact: "",
    website: "",
    address: "",
    max_order_time: 0,
    contact_person: "",
    delivery_address: "",
    telegram_bot: [],
    legal: legalInitialValue,
    agree: false
}

export const loginValidationRule = Yup.object({
    email: Yup.string()
        .email("Неверный адрес электронной почты")
        .required("Электронная почта обязательна"),
    password: Yup.string()
        .required("Пароль обязательна")
        .min(4, "Пароль: Минимум 4 символа"),
})


export const createUserValidationRule = Yup.object({
    email: Yup.string()
        .email("Неверный адрес электронной почты")
        .required("Электронная почта обязательна"),
    name: Yup.string()
        .required("Имя обязательно для заполнения"),
    phone: Yup.string()
        .required("Номер телефона обязательно для заполнения"),
    role: Yup.string()
        .required("Роль обязательно для заполнения")
})


export const createTestUserValidationRule = Yup.object({
    email: Yup.string()
        .email("Неверный адрес электронной почты")
        .nullable(),
    name: Yup.string()
        .nullable(),
    count: Yup.number()
        .nullable(),
    password: Yup.string()
        .nullable(),
})


export const roleValidationRule = Yup.object({
    name: Yup.string()
        .required("Назвоние обязательное для заполнения"),
    description: Yup.string()
        .required("Значение обязательное для заполнения")
})


export const productModerationValidationRule = Yup.object({
    exceptions: Yup.string()
})


export const updateProfileValidationRule = Yup.object({
    email: Yup.string()
        .email("Неверный адрес электронной почты")
        .required("Электронная почта обязательна"),
    name: Yup.string()
        .required("Имя обязательно для заполнения"),
    password: Yup.string()
        .required("Пароль обязательна")
        .min(4, "Пароль: Минимум 4 символа"),
})

export const createBrandValidationRule = Yup.object({
    name: Yup.string()
        .required("Значение обязательное для заполнения"),
    description: Yup.string()
        .required("Значение обязательное для заполнения"),
})


export const createLegalInfoValidationRule = Yup.object().shape({
    form: Yup.string().required("Форма собственности обязательна"),
    address: Yup.string().required("Юридический адрес обязательна"),
    director: Yup.string().required("ФИО руководителя обязательна"),
    inn: Yup.string().required("ИНН обязательна").when('form', {
        is: 'ИП',
        then: Yup.string().length(12, "ИНН: Введите 12 символов"),
        otherwise: Yup.string().length(10, "ИНН: Введите 10 символов"),
    }),
    bik: Yup.string().required("КПП компании обязательна")
        .length(9, 'Введите 9 символов'),
    kpp: Yup.string().when('form', {
        is: (val) => val !== 'ИП',
        then: Yup.string()
            .required("БИК банка обязательна")
            .length(9, "БИК банка: Введите 9 символов"),
        otherwise: Yup.string().nullable()
    }),
    ogrn: Yup.string().when('form', {
        is: (val) => val !== 'ИП',
        then: Yup.string()
            .required("ОГРН компании обязательна")
            .length(13, "ОГРН: Введите 13 символов"),
        otherwise: Yup.string().nullable()
    }),
    ogrnip: Yup.string().when('form', {
        is: 'ИП',
        then: Yup.string().required("ОГРНИП обязательна")
            .length(15, "ОГРНИП: Введите 15 символов"),
        otherwise: Yup.string().nullable()
    }),
    taxation_system: Yup.string().required("Система налогообложения обязательна"),
    payment_account: Yup.string().required("Расчетный счет обязательна")
        .length(20, 'Расчетный счет: Введите 20 символов'),
})


export const createStoreValidationRule = Yup.object({
    name: Yup.string().required("Название магазина  обязательна"),
    contact: Yup.string().nullable(),
    website: Yup.string().url('Формат сайта не правильный: ex: http(s)://...').nullable(),
    address: Yup.string().nullable(),
    max_order_time: Yup.number().min(1, "Максимальный срок сборки заказа должен быть больше в часах"),
    contact_person: Yup.string().required("ФИО контактного лица  обязательна"),
    delivery_address: Yup.string().required("Адрес склада  обязательный"),
    telegram_bot: Yup.array().nullable(),
    legal: createLegalInfoValidationRule,
    agree: Yup.boolean().oneOf([true], "Согласитесь на оферту оказания услуг")
})

export const useTariffValidation =  Yup.object({
        name: Yup.string()
            .required("Значение обязательное для заполнения"),
        commission: Yup.number()
            .required("Значение обязательное для заполнения"),
        commission_acquiring: Yup.number()
            .required("Значение обязательное для заполнения"),
        min_commission_RUB: Yup.number()
            .required("Значение обязательное для заполнения"),
 })

export const propertyValidationRule =  Yup.object({
        name: Yup.string()
            .required("Значение обязательное для заполнения"),
        type: Yup.string()
            .required("Значение обязательное для заполнения"),
        description: Yup.string(),
 })

export const propertyValueValidationRule =  Yup.object({
        value: Yup.string()
            .required("Значение обязательное для заполнения"),
        description: Yup.string(),
    })

export default (
    loginValidationRule,
    createUserValidationRule,
    updateProfileValidationRule
)
