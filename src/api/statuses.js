import api from "./base";

export async function getAllStatus(queries = '') {
    try {
        const response = await api.get(`/admin/statuses/${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}
