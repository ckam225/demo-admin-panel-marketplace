import api from "./base";


export async function getAllAmounts(queries = '') {
    try {
        const response = await api.get(`/admin/amounts${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}
