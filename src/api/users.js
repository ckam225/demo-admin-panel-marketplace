import api from "./base";


export async function getAllUsers() {
    try {
        const response = await api.get('/admin/users');
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function createUser(payload) {
    try {
        const response = await api.post('/admin/users', payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function createTestUser(payload) {
    try {
        const response = await api.post('/admin/fixture/user/create', payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function updateUser(userId, payload) {
    try {
        const response = await api.patch(`/admin/users/${userId}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findUser(userId) {
    try {
        const response = await api.get(`/admin/users/${userId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function destroyUser(userId) {
    try {
        const response = await api.delete(`/admin/users/${userId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}
