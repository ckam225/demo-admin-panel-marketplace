import api from "./base";

export async function getAllOrders(queries = '') {
    try {
        const response = await api.get(`/admin/orders${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}