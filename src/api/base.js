import axios from "axios";
import storage from "../store/local";
import { toast } from "react-toastify";
import { navigate } from "@reach/router";

export const bearerToken = `Bearer ${storage.get("token")}`;

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
});

api.interceptors.request.use(
    function (config) {
        if (storage.exists("token")) {
            config.headers.common["Authorization"] = `Bearer ${storage.get("token")}`;
        }
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

api.interceptors.response.use(
    function (response) {
        return response;
    },
    function (error) {
        if (error.message === "Network Error") {
            toast.error(error.message);
        
        }
        if (error.response.status === 401) {
            if (storage.exists('token'))
                toast.error("Invalid token.");
            storage.reset();
            // eslint-disable-next-line no-restricted-globals
            const page = location.pathname === '/login' ? '/' : location.pathname
            navigate("/login?redirect=" + page);
        }
        if (error.response.status === 500) {
            toast.error("Internal error. Contact your developer");
        }
        return Promise.reject(error);
    }
);

api.format = function (response, error = false) {
    return {
        error,
        data: response ? response.data : {},
        status: response ? response.status : 404,
    };
};

export default api;