import api from "./base";



export async function getAllProduct(queries = '') {
    try {
        const response = await api.get(`/admin/products${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function getProductsByStatus(queries = '') {
    try {
        const response = await api.get(`/admin/products/statuses${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findProduct(productId) {
    try {
        const response = await api.get(`/admin/products/${productId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function updateProduct(productId, data) {
    try {
        const response = await api.patch(`/admin/products/${productId}`, data);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function getProductChanges(productId) {
    try {
        const response = await api.get(`/admin/products/${productId}/tracks`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}


