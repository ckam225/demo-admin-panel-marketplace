import storage from "../store/local";
import api from "./base";



export async function login(credentials) {
    try {
        const response = await api.post("/auth/login?admin=true", credentials);
        storage.set("token", response.data.access_token);
        storage.set("user", response.data);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function currentUser() {
    try {
        const response = await api.post("/auth/user/");
        storage.set("token", response.data.token);
        storage.set("user", response.data);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function logout() {
    try {
        const response = await api.post("/auth/logout");
        storage.reset();
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

