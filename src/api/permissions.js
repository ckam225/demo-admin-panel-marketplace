import api from "./base";

export async function getAllPermissions() {
    try {
        const response = await api.get('/admin/permissions');
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}


export async function createPermission(payload) {
    try {
        const response = await api.post('/admin/permissions', payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function updatePermission(permissionId, payload) {
    try {
        const response = await api.put(`/admin/permissions/${permissionId}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findPermission(permissionId) {
    try {
        const response = await api.get(`/admin/permissions/${permissionId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function destroyPermission(permissionId) {
    try {
        const response = await api.delete(`/admin/permissions/${permissionId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}
