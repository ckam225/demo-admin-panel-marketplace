import api from "./base";


export async function getAllStores(queries = '') {
    try {
        const response = await api.get(`/admin/stores${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function latestStores() {
    try {
        const response = await api.get('/admin/stores/latest?per_page=10');
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findStore(roleId) {
    try {
        const response = await api.get(`/admin/stores/${roleId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function createStore(payload) {
    try {
        const response = await api.post(`/admin/stores`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function partialUpdateStore(storeId, payload) {
    try {
        const response = await api.patch(`/admin/stores/${storeId}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}



