import api from "./base";

export async function fetchAllCategories(queries = '') {
    try {
        const response = await api.get(`/admin/categories${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function fetchAllCategoriesHasProps(queries = '') {
    try {
        const response = await api.get(`/admin/categories/has-props${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findCategoryByIdWithProps(id, queries = '') {
    try {
        const response = await api.get(`/admin/categories/${id}/properties${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findCategoryById(id, queries = '') {
    try {
        const response = await api.get(`/admin/categories/${id}/${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function updateCategory(id, payload) {
    try {
        const response = await api.put(`/admin/categories/${id}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function searchCategories(queries = '') {
    try {
        const response = await api.get(`/admin/categories/search${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function fetchCategoryProperties(id, queries = '') {
    try {
        const response = await api.get(`/admin/categories/${id}/properties-list${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function deleteCategoryProperty(catId, propId) {
    try {
        const response = await api.delete(`/admin/categories/${catId}/properties/${propId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function addCategoryProperty(catId, props) {
    try {
        const response = await api.post(`/admin/categories/${catId}/properties`, { props });
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

