import api from "./base";

export async function getUnreadCountNotifications() {
    try {
        const response = await api.get('/notifications/unread-count');
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function getAllNotifications(query = '') {
    try {
        const response = await api.get(`/notifications${query}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findNotification(notificationId) {
    try {
        const response = await api.get(`/notifications/${notificationId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function markNotificationAsRead(notificationIds) {
    try {
        const response = await api.post(`/notifications/mark-as-read`, null, {
            params: {
                notificationIds
            }
        });
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function deleteNotification(notificationIds) {
    try {
        const response = await api.post(`/notifications/delete`, { notificationIds });
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}


