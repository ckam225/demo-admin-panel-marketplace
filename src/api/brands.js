import api from "./base";

export async function fetchAllBrands(queries = '') {
    try {
        const response = await api.get(`/brands${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}



export async function findBrandById(id, queries = '') {
    try {
        const response = await api.get(`/admin/brands/${id}/${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}


export async function searchBrands(queries = '') {
    try {
        const response = await api.get(`/admin/brands/search${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function create(payload) {
    try {
        const response = await api.post(`/admin/brands`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function update(id, payload) {
    try {
        const response = await api.put(`/admin/brands/${id}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function destroy(id) {
    try {
        const response = await api.delete(`/admin/brands/${id}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

