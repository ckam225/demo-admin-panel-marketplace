import api from "./base";

export async function fetchAllProperties(queries = '') {
    try {
        const response = await api.get(`/admin/properties${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findProperty(propertyId) {
    try {
        const response = await api.get(`/admin/properties/${propertyId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function fetchPropertyValues(propertyId) {
    try {
        const response = await api.get(`/admin/properties/${propertyId}/values`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function storePropertyValue(propertyId, payload) {
    try {
        const response = await api.post(`/admin/properties/${propertyId}/values`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function updatePropertyValue(propertyId, valueId, payload) {
    try {
        const response = await api.put(`/admin/properties/${propertyId}/values/${valueId}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function deletePropertyValue(propertyId, valueId) {
    try {
        const response = await api.delete(`/admin/properties/${propertyId}/values/${valueId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function fetchTypes() {
    try {
        const response = await api.get(`/admin/properties/types`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function updateProperty(propertyId, payload) {
    try {
        const response = await api.put(`/admin/properties/${propertyId}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function storeProperty(payload) {
    try {
        const response = await api.post(`/admin/properties`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}
