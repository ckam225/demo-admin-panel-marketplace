import api from "./base";

export async function fetchAllTariffs(queries = '') {
    try {
        const response = await api.get(`/admin/tariffs${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findTariffById(id, queries='') {
    try {
        const response = await api.get(`/admin/tariffs/${id}/${queries}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function createTariff(payload) {
    try {
        const response = await api.post(`/admin/tariffs`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function updateTariff(id, payload) {
    try {
        const response = await api.put(`/admin/tariffs/${id}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function destroyTariff(id) {
    try {
        const response = await api.delete(`/admin/tariffs/${id}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}
