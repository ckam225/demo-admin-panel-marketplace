import api from "./base";


export async function getAllLogs() {
    try {
        const response = await api.get('/admin/logs');
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function truncateLog(logId) {
    try {
        const response = await api.put(`/admin/logs/${logId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

