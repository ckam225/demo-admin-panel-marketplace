import api from "./base";


export async function getRoles() {
    try {
        const response = await api.get('/admin/roles');
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function createRole(payload) {
    try {
        const response = await api.post('/admin/roles/create', payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function updateRole(roleId, payload) {
    try {
        const response = await api.put(`/admin/roles/${roleId}`, payload);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function findRole(roleId) {
    try {
        const response = await api.get(`/admin/roles/${roleId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

export async function destroyRole(roleId) {
    try {
        const response = await api.delete(`/admin/roles/${roleId}`);
        return api.format(response);
    } catch (error) {
        return api.format(error.response, true);
    }
}

