import React from "react";
import { Router } from "@reach/router";
import { Route, NestedRoute } from "./base";

import HomePage from "../views/Home";
import NotFoundPage from "../views/errors/404";
import NotAuthorizedPage from "../views/errors/401";
import ListUserPage from "../views/users/ListUsers";
import ShowUserPage from "../views/users/UserDetail";
import CreateUserPage from "../views/users/CreateUser";
import CreateTestUserPage from "../views/users/CreateTestUser";
import LoginPage from "../views/auth/Login";
import ListRolePage from "../views/permissions/Roles";
import SettingsPage from "../views/settings/Settings";
import LogsPage from "../views/settings/Logs";
import ProfilePage from "../views/settings/Profile";
import NotificationsPage from "../views/settings/Notifications";
import UIKitPage from "../views/uikit/Kits";
import UIKitIconPage from "../views/uikit/Icons";
import SyncPage from "../views/syncs/Sync";
import SyncDetailPage from "../views/syncs/SyncDetail";
import DocsPage from "../views/Docs";
import StoresPage from "../views/stores/AllStores";
import StoreDetailPage from "../views/stores/StoreDetail";
import ProductPage from "../views/products/ListProducts";
import ProductDetailPage from "../views/products/DetailProduct";
import OtherPage from "../views/others/Others";
import ModerationPage from "../views/moderation/Moderation";


const RootRouter = () => {
    return (
        <Router>
            <Route default page={NotFoundPage} />
            <Route path="/" page={HomePage} auth />
            <NestedRoute path="/stores">
                <Route path="/" page={StoresPage} auth />
                <Route path=":storeId" page={StoreDetailPage} auth />
            </NestedRoute>
            <NestedRoute path="/products">
                <Route path="/" page={ProductPage} auth />
                <Route path=":productId" page={ProductDetailPage} auth />
            </NestedRoute>
            <Route path="/login" page={LoginPage} guest />
            <Route path="/unauthorized" page={NotAuthorizedPage} />
            <Route path="/docs" page={DocsPage} />
            <NestedRoute path="/users">
                <Route path="/" page={ListUserPage} auth />
                <Route path="/create" page={CreateUserPage} auth />
                <Route path="/create-test" page={CreateTestUserPage} auth />
                <Route path=":userId" page={ShowUserPage} auth />
            </NestedRoute>
            <Route path="/permissions" page={ListRolePage} auth />
            <NestedRoute path="/settings">
                <Route path="/" page={SettingsPage} auth />
                <Route path="/logs" page={LogsPage} auth />
                <Route path="/profile" page={ProfilePage} auth />
                <Route path="/notifications" page={NotificationsPage} auth />
            </NestedRoute>
            <NestedRoute path="/synchronization">
                <Route path="/" page={SyncPage} auth />
                <Route path=":syncId" page={SyncDetailPage} auth />
            </NestedRoute>
            {/* <NestedRoute path="/moderation">
                <Route path="/" page={ProductPage} isModerationMode={true} auth />
                <Route path=":productId" page={ProductDetailPage} isModerationMode={true} auth />
            </NestedRoute> */}
            <NestedRoute path="/uikit">
                <Route path="/" page={UIKitPage} />
                <Route path="/icons" page={UIKitIconPage} />
            </NestedRoute>
            <Route path="/others" page={OtherPage} auth />
            <Route path="/moderation" page={ModerationPage} auth />
        </Router >
    );
};

export default RootRouter
