import React from 'react'
import MainLayout from '../../components/layouts/Main'
import useStores, {storeStatus } from '../../store/actions/stores'
import { BaseLink } from '../../components/shared/NavItem'
import { currency } from '../../helpers'
import Icon from '../../components/controls/Icon'
import StoreDocuments from './components/StoreDocuments'
import StoreWorktimes from './components/StoreWorktimes'
import StoreOverview from './components/StoreOverview'
import StorePaymentInfo from './components/StorePaymentInfo'
import StoreLegalInfo from './components/StoreLegalInfo'
import StoreAmounts from './components/StoreAmounts'
import StoreOrders from './components/StoreOrders'
import ProductTable from '../products/components/ProductDataTable'
import Tab from '../../components/controls/Tab'
import StoreInfo from './components/StoreInfo'


const StoreDetail = ({ storeId }) => {
    const { currentStore, findStore, partialUpdateStore } = useStores()
    const [updateLoading, setUpdateLoading] = React.useState(false)

    React.useEffect(() => {
        (async ()=> {
            setUpdateLoading(true)
            await findStore(storeId)
            setUpdateLoading(false)
         }
        )()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [storeId])

    const handleUpdate = React.useCallback(async (status) => {
        if (currentStore) {
            setUpdateLoading(true)
            await partialUpdateStore(currentStore.id, {
                status_id: status
            })
            setUpdateLoading(false)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentStore])

    return <MainLayout>
        <MainLayout.Head>
            {currentStore && <title>{currentStore?.name}</title>}
        </MainLayout.Head>
        <MainLayout.Container>
            <StoreInfo store={currentStore} 
            handleUpdate={handleUpdate}
            updateLoading={updateLoading}
            />
        </MainLayout.Container>
    </MainLayout>
}



export default StoreDetail;