import React from 'react'
import styled from 'styled-components'
import MainLayout from '../../components/layouts/Main';
import { useStores } from '../../store/actions/stores'
import useStatuses from '../../store/actions/statuses';
import SearchBox from '../../components/controls/SearchBox';
import Modal from '../../components/controls/Modal';
import CreateStoreForm from './components/CreateStoreForm';
import StoreCollection from './components/StoreCollection';
import { navigate } from '@reach/router';

const AllStores = () => {
    const { statuses } = useStatuses('?type=stores')
    const [status, setStatus] = React.useState('all')
    const [search, setSearch] = React.useState('')
    const { stores, fetchStores, pageCount, loading, createStore } = useStores()
    const [currentPage, setCurrentPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)
    const createModalRef = React.useRef()

    const [storesPaginated, setStoresPaginated] = React.useState([])


    function handleTabClick(t) {
        setStatus(t)
    }

    function handleSearchChange(s) {
        setSearch(s)
    }

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    function handleResetForm() {
        createModalRef.current.close()
    }

    function handleSubmitForm(values, formik) {
        setTimeout(async () => {
            const result = await createStore(values)
            if (result) {
                formik.resetForm()
                createModalRef.current.close()
            }
        }, 200);
    }

    const query = React.useMemo(() => {
        let q = `?page=${currentPage}&per_page=${perPage}`
        q += status !== 'all' ? `&status=${status}` : ''
        if (search.length > 0)
            q += '&name=' + search
        return q
    }, [status, search, currentPage, perPage])

    React.useEffect(() => {
        fetchStores(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

   
   React.useEffect(() => {
       const s = [...storesPaginated ,...stores]
       setStoresPaginated(s)
   }, [stores])

    function handleItemClick(store){
        navigate(`/stores/${store.id}`)
    }

    function handleNextClick() {
        setCurrentPage(prev => prev + 1)
    }

    return <>
        <MainLayout>
            <MainLayout.Head>
                <title>Партнёры</title>
            </MainLayout.Head>
            <MainLayout.Container >
                <Wrapper className="m-3 h-full" >
                    <div className="sticky inset-x-0 top-0 left-0 tab__header  justify-between bg-white" >
                        <div className="flex">
                            <div className={`tab_header_item ${status === 'all' ? 'active' : ''}`} onClick={() => handleTabClick('all')}>Все</div>
                            {statuses.length > 0 && statuses.map(st => <div key={st.id} className={`tab_header_item ${status === st.name ? 'active' : ''}`}
                                onClick={() => handleTabClick(st.name)}>
                                {st.title}
                            </div>)}
                        </div>
                        
                    </div>
                    <div className="bg-white p-3 my-3" >
                        <div className="flex justify-between">
                            <div className="flex items-center border-b pb-2">
                                <SearchBox onChange={handleSearchChange} placeholder="Поиск по назвонию" className="mr-2 py-1 rounded-sm text-sm" />
                                <button className="button" onClick={() => createModalRef.current.open()}>Новый продавец</button>
                            </div>
                            {!loading && <div className=" mx-3">
                                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                                    <option value="10">10</option>
                                    <option value="30" >30</option>
                                    <option value="50" >50</option>
                                    <option value="100" >100</option>
                                </select>
                            </div>}
                        </div>
                        <StoreCollection stores={storesPaginated} loading={loading && pageCount <= 1}  onItemClick={handleItemClick}/>
                        {!loading && <div className="py-2 flex justify-center items-center bg-gray-50">
                            {(stores.length > 0 && pageCount > 1)  &&  <div>
                                {loading ? <span>Loading...</span> : 
                                 <button className="button" onClick={handleNextClick}>Загрузить еще</button>}
                            </div>}
                        </div>}
                    </div>
                </Wrapper>
            </MainLayout.Container>
        </MainLayout>

        <Modal refId={createModalRef} style={{ width: "600px" }} dismissible={false}>
            <Modal.Header>
                <h1>Новый продавца</h1>
            </Modal.Header>
            <Modal.Body>
                <CreateStoreForm loading={loading} onReset={handleResetForm} onSubmit={handleSubmitForm} />
            </Modal.Body>
        </Modal>
    </>
}

const Wrapper = styled.div`
.tab__header{
  background: #fff;
  display: flex;
  border-bottom: 1px solid #ccc;
}
.tab_header_item{
    font-size: 14px;
    font-style: normal;
    font-weight: 300;
    padding: 15px 10px;
    border-bottom: 2px solid transparent;
    color: rgba(0, 0, 0, 0.6)
}
.tab_header_item:hover{
    color: var(--color-accent);
    cursor: pointer;
}
.tab_header_item.active{
    color: var(--color-accent);
    border-color: var(--color-accent);
}
.link__sync::hover{
    color: var(--color-accent);
    text-decoration: underline;
}
table td {
    font-size: 0.70rem/* 12px */;
    line-height: 1rem/* 16px */;
    color: rgba(0, 0, 0, 0.8)
}
`
export default AllStores;