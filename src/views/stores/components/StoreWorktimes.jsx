import React from 'react'
import { russianDays } from '../../../helpers';

const StoreWorktimes = ({ worktimes = [] }) => {
    return <table className="table-3 ">
        <tbody>
            {worktimes.length > 0 && worktimes.map(wkt => <tr key={wkt.id}>
                <td>{russianDays[wkt?.day ?? 0]}</td>
                <td>{wkt?.start ?? '--:--'}</td>
                <td>{wkt?.end ?? '--:--'}</td>
            </tr>)}
        </tbody>
    </table>
}

export default StoreWorktimes;