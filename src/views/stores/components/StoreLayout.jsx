import React from 'react'
import styled from 'styled-components'
import NavTabs from '../../../components/controls/NavTabs';
import MainLayout from '../../../components/layouts/Main';

const StoreLayout = (props) => {
    const routes = [
        {
            path: '/stores',
            title: 'Все',
            exact: true
        }, 
        {
            path: '/stores/wait_legal_infos',
            title: 'Ждём сведений',
            exact:false
        },
        {
            path: '/stores/wait_checking',
            title: 'Ждём проверки',
            exact:false
        },
        {
            path: '/stores/wait_fixing',
            title: 'Ждём исправлений',
            exact:false
        },
        {
            path: '/stores/activated',
            title: 'Активен',
            exact:false
        },
    ]

    return <Wrapper>
        <MainLayout>
            <MainLayout.Head>

            </MainLayout.Head>
            <MainLayout.Container>
                <div className="bg-white m-3">
                    <NavTabs  routes={routes}/>
                    {props.children}
                </div>
            </MainLayout.Container>
        </MainLayout>
    </Wrapper>;
}
const Wrapper = styled.div`
table td {
    font-size: 0.80rem/* 12px */;
    line-height: 1rem/* 16px */;
    color: rgba(0, 0, 0, 0.8)
}
`
export default StoreLayout;