import React from "react"
import styled from "styled-components"
import { useStoreAmounts } from "../../../store/actions/stores"
import { DateTime } from "luxon"
import { currency } from "../../../helpers";
import Paginator from "../../../components/controls/Paginator";
import TableEmptyRow from "../../../components/controls/TableEmptyRow";
import Loader from "../../../components/controls/Loader";


const StoreAmounts = ({ storeId }) => {

    const { amounts, fetchAmounts, pageCount, loading } = useStoreAmounts()
    const [page, setPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    const query = React.useMemo(() => {
        return `?page=${page}&per_page=${perPage}&store_id=${storeId}`
    }, [storeId, page, perPage])

    React.useEffect(() => {
        fetchAmounts(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])


    return <Wrapper><table className="table">
        <thead>
            <tr>
                <th>Дата</th>
                <th>Номер</th>
                <th>Сумма выплаты</th>
            </tr>
        </thead>
        <tbody>
            {loading ? <TableEmptyRow colspan={5} className="flex justify-center  py-5">
                <Loader />
            </TableEmptyRow> : amounts.length > 0 ? amounts.map(am => <tr key={am.id}>
                <td>{DateTime.fromISO(am.created_at).setLocale('ru').toFormat('dd LLL yyyy, hh:mm')}</td>
                <td>{am.number}</td>
                <td>{currency.format(am.amount || 0)}</td>
            </tr>) :
                <TableEmptyRow colspan={5} className="text-sm text-center py-5">Нет выплат</TableEmptyRow>
            }
        </tbody>
    </table>
        {!loading && <div className="py-2 flex justify-between items-center bg-gray-50">
            {(amounts.length > 0 && pageCount > 1) ? <Paginator className="text-blue-accent"
                itemClass=" text-sm rounded-full mx-1 w-6 h-6 focus:outline-none"
                activeItemClass="pagin-item-color"
                pages={pageCount}
                setCurrentPage={setPage} /> : <div />}
            <div className=" mx-3">
                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                    <option value="10">10</option>
                    <option value="30">30</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
        </div>}
    </Wrapper>
}
const Wrapper = styled.div`

table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`
export default StoreAmounts;