import React from "react"

const StoreOverview = ({ store }) => {
    return <table className="table-3 ">
        <tbody>
            <tr>
                <td>Ид</td>
                <td>{store?.id}</td>
            </tr>
            <tr>
                <td>Код</td>
                <td>{store?.code_1c}</td>
            </tr>
            <tr>
                <td>Статус</td>
                <td>{store?.status?.title}</td>
            </tr>
            <tr>
                <td>Название магазина</td>
                <td>{store?.name}</td>
            </tr>
            <tr>
                <td>Логин</td>
                <td>{store?.user?.email}</td>
            </tr>
            <tr>
                <td>Контактное лицо</td>
                <td>{store?.contact_person}</td>
            </tr>
            <tr>
                <td>Контактное телефон</td><td>{store?.contact}</td>
            </tr>
            <tr>
                <td>Сайт компании</td><td>{store?.website}</td>
            </tr>
            <tr>
                <td>Почтовый адрес</td><td>{store?.address}</td>
            </tr>
            <tr>
                <td>Максимальный срок(в часах)</td><td>{store?.max_order_time}</td>
            </tr>
            <tr>
                <td>Тарифф</td><td>{store?.tariff?.name}</td>
            </tr>
            <tr>
                <td>Адрес склада</td><td>{store?.delivery_address}</td>
            </tr>
        </tbody>
    </table>
}

export default StoreOverview;