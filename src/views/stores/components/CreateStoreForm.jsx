import { useFormik } from 'formik';
import { createStoreValidationRule, storeInitialValue } from '../../../hooks/validators';
import * as vars from '../../../helpers/vars'
import NumberField from '../../../components/controls/NumberField';
import Loader from '../../../components/controls/Loader';


const Grid = ({ children }) => <div className="grid grid-cols-2 items-center my-2 text-sm">
    {children}
</div>

const Header = ({ children, className }) => <div className="flex items-center bg-blue-50">
    <div className={[" text-gray-500 mr-3", className].join("")} >{children}</div>
</div>

const Label = ({ children, className }) => <div className={className}> {children} <sup className="text-red-600">*</sup></div>

const CreateStoreForm = ({ onSubmit, onReset, loading }) => {

    const formik = useFormik({
        initialValues: storeInitialValue,
        validationSchema: createStoreValidationRule,
        onSubmit: (values) => {
            onSubmit(values, formik)
        },
        onReset() {
            onReset()
        }
    })



    return <form onSubmit={formik.handleSubmit}>

        <div className="p-5">
            <div>
                <Header>Общие сведения</Header>
                <Grid>
                    <Label className={formik.errors.name && formik.touched.name ? 'text-red-400' : ''} >
                        Название магазина
                    </Label>
                    <div className="flex flex-col">
                        <input type="text"
                            name="name"
                            value={formik.values.name}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors.name && formik.touched.name ? 'border-red-500' : ''].join(" ")} />
                        {formik.errors.name && formik.touched.name &&
                            <small className="text-red-500">{formik.errors.name}</small>}
                    </div>
                </Grid>
                <Grid>
                    <div>Сайт компании</div>
                    <input type="url"
                        name="website"
                        value={formik.values.website}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={["textbox py-1 rounded-none",
                            formik.errors.website && formik.touched.website ? 'border-red-500' : ''].join(" ")} />
                </Grid>
                <Grid>
                    <Label className={formik.errors.name && formik.touched.contact_person ? 'text-red-400' : ''} >ФИО контактного лица</Label>
                    <div className="flex flex-col">
                        <input type="text"
                            name="contact_person"
                            value={formik.values.contact_person}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors.contact_person && formik.touched.contact_person ? 'border-red-500' : ''].join(" ")} />
                        {formik.errors.contact_person && formik.touched.contact_person &&
                            <small className="text-red-500">{formik.errors.contact_person}</small>}
                    </div>
                </Grid>
                <Grid>
                    <div>Контактный телефон</div>
                    <input type="text"
                        name="contact"
                        value={formik.values.contact}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={["textbox py-1 rounded-none",
                            formik.errors.contact && formik.touched.contact ? 'border-red-500' : ''].join(" ")} />
                </Grid>
                <Grid>
                    <div>Почтовый адрес</div>
                    <input type="text"
                        name="address"
                        value={formik.values.address}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={["textbox py-1 rounded-none",
                            formik.errors.address && formik.touched.address ? 'border-red-500' : ''].join(" ")} />
                </Grid>
                <Grid>
                    <div>Максимальный срок(в часах)</div>
                    <input type="number" min="0"
                        name="max_order_time"
                        value={formik.values.max_order_time}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        className={["textbox py-1 rounded-none",
                            formik.errors.max_order_time && formik.touched.max_order_time ? 'border-red-500' : ''].join(" ")} />
                </Grid>
                <Grid>
                    <Label className={formik.errors.delivery_address && formik.touched.delivery_address ? 'text-red-400' : ''} >Адрес склада</Label>
                    <div className="flex flex-col">
                        <input type="text"
                            name="delivery_address"
                            value={formik.values.delivery_address}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors.delivery_address && formik.touched.delivery_address ? 'border-red-500' : ''].join(" ")} />
                        {formik.errors.delivery_address && formik.touched.delivery_address &&
                            <small className="text-red-500">{formik.errors.delivery_address}</small>}
                    </div>
                </Grid>
            </div>
            <div className="my-5">
                <Header>Юридическая информация</Header>
                <Grid>
                    <Label className={formik.errors.legal?.director && formik.touched?.legal?.director ? 'text-red-400' : ''} >ФИО руководителя</Label>
                    <div className="flex flex-col">
                        <input type="text"
                            name="legal.director"
                            value={formik.values.legal.director}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors?.legal?.director && formik.touched?.legal?.director ? 'border-red-500' : ''].join(" ")} />
                        {formik.errors?.legal?.director && formik.touched?.legal?.director &&
                            <small className="text-red-500">{formik.errors?.legal?.director}</small>}
                    </div>
                </Grid>
                <Grid>
                    <Label className={formik.errors.legal?.address && formik.touched?.legal?.address ? 'text-red-400' : ''} >Юридический адрес</Label>
                    <div className="flex flex-col">
                        <input type="text"
                            name="legal.address"
                            value={formik.values.legal.address}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors.legal?.address && formik.touched.legal?.address ? 'border-red-500' : ''].join(" ")} />
                        {formik.errors.legal?.address && formik.touched.legal?.address &&
                            <small className="text-red-500">{formik.errors.legal?.address}</small>}
                    </div>
                </Grid>
                <Grid>
                    <Label className={formik.errors.legal?.form && formik.touched.legal?.form ? 'text-red-400' : ''} >Форма собственности</Label>
                    <div className="flex flex-col">
                        <select
                            name="legal.form"
                            value={formik.values.legal.form}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors?.legal?.form && formik.touched?.legal?.form ? 'border-red-500' : ''].join(" ")} >
                            {vars.FORMOWNERSHIPS.map((ts, i) => <option key={i} value={ts}>{ts}</option>)}
                        </select>
                        {formik.errors?.legal?.form && formik.touched?.legal?.form &&
                            <small className="text-red-500">{formik.errors?.legal?.form}</small>}
                    </div>
                </Grid>
            </div>
            <div className="my-5">
                <Header>Налогообложение</Header>
                <Grid>
                    <Label className={formik.errors.legal?.taxation_system && formik.touched?.legal?.taxation_system ? 'text-red-400' : ''} >Система налогообложения</Label>
                    <div className="flex flex-col">
                        <select
                            name="legal.taxation_system"
                            value={formik.values.legal.taxation_system}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors?.legal?.taxation_system && formik.touched?.legal?.taxation_system ? 'border-red-500' : ''].join(" ")} >
                            {vars.TAXSYSTEMS.map((ts, i) => <option key={i} value={ts}>{ts}</option>)}
                        </select>
                        {formik.errors?.legal?.taxation_system && formik.touched?.legal?.taxation_system &&
                            <small className="text-red-500">{formik.errors?.legal?.taxation_system}</small>}
                    </div>
                </Grid>
            </div>
            <div className="my-5">
                <Header>Платежные реквизиты</Header>
                <Grid>
                    <Label className={formik.errors.legal?.inn
                        && formik.touched?.legal?.inn ? 'text-red-400' : ''}>ИНН компании</Label>
                    <div className="flex flex-col">
                        <NumberField type="text"
                            name="legal.inn"
                            maxLength="12"
                            value={formik.values.legal.inn}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors?.legal?.inn && formik.touched?.legal?.inn ? 'border-red-500' : ''].join(" ")} />
                        {formik.errors?.legal?.inn && formik.touched?.legal?.inn &&
                            <small className="text-red-500">{formik.errors?.legal?.inn}</small>}
                    </div>
                </Grid>
                {formik.values.legal.form === 'ИП' ?
                    <Grid>
                        <Label className={formik.errors.legal?.ogrnip && formik.touched?.legal?.ogrnip ? 'text-red-400' : ''}>ОГРНИП</Label>
                        <div className="flex flex-col">
                            <NumberField type="text"
                                name="legal.ogrnip"
                                maxLength="15"
                                value={formik.values.legal.ogrnip}
                                onChange={formik.handleChange}
                                onBlur={formik.handleBlur}
                                className={["textbox py-1 rounded-none",
                                    formik.errors?.legal?.ogrnip && formik.touched?.legal?.ogrnip ? 'border-red-500' : ''].join(" ")} />
                            {formik.errors?.legal?.ogrnip && formik.touched?.legal?.ogrnip &&
                                <small className="text-red-500">{formik.errors?.legal?.ogrnip}</small>}
                        </div>
                    </Grid>
                    :
                    <>
                        <Grid>
                            <Label className={formik.errors.legal?.kpp && formik.touched?.legal?.kpp ? 'text-red-400' : ''}>КПП компании</Label>
                            <div className="flex flex-col">
                                <NumberField type="text"
                                    name="legal.kpp"
                                    maxLength="9"
                                    value={formik.values.legal.kpp}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    className={["textbox py-1 rounded-none",
                                        formik.errors?.legal?.kpp && formik.touched?.legal?.kpp ? 'border-red-500' : ''].join(" ")} />
                                {formik.errors?.legal?.kpp && formik.touched?.legal?.kpp &&
                                    <small className="text-red-500">{formik.errors?.legal?.kpp}</small>}
                            </div>
                        </Grid>
                        <Grid>
                            <Label className={formik.errors.legal?.ogrn && formik.touched?.legal?.ogrn ? 'text-red-400' : ''}>
                                ОГРН компании
                            </Label>
                            <div className="flex flex-col">
                                <NumberField type="text"
                                    name="legal.ogrn"
                                    maxLength="13"
                                    value={formik.values.legal.ogrn}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    className={["textbox py-1 rounded-none",
                                        formik.errors?.legal?.ogrn && formik.touched?.legal?.ogrn ? 'border-red-500' : ''].join(" ")} />
                                {formik.errors?.legal?.ogrn && formik.touched?.legal?.ogrn &&
                                    <small className="text-red-500">{formik.errors?.legal?.ogrn}</small>}
                            </div>
                        </Grid>
                    </>
                }
                <Grid>
                    <Label className={formik.errors.legal?.payment_account && formik.touched?.legal?.payment_account ? 'text-red-400' : ''}>
                        Расчетный счет компании
                    </Label>
                    <div className="flex flex-col">
                        <NumberField type="text"
                            name="legal.payment_account"
                            maxLength="20"
                            value={formik.values.legal.payment_account}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors?.legal?.payment_account && formik.touched?.legal?.payment_account ? 'border-red-500' : ''].join(" ")} />
                        {formik.errors?.legal?.payment_account && formik.touched?.legal?.payment_account &&
                            <small className="text-red-500">{formik.errors?.legal?.payment_account}</small>}
                    </div>
                </Grid>
                <Grid>
                    <Label className={formik.errors.legal?.bik && formik.touched?.legal?.bik ? 'text-red-400' : ''}>БИК банка</Label>
                    <div className="flex flex-col">
                        <NumberField type="text"
                            name="legal.bik"
                            maxLength="9"
                            value={formik.values.legal.bik}
                            onChange={formik.handleChange}
                            onBlur={formik.handleBlur}
                            className={["textbox py-1 rounded-none",
                                formik.errors?.legal?.bik && formik.touched?.legal?.bik ? 'border-red-500' : ''].join(" ")} />
                        {formik.errors?.legal?.bik && formik.touched?.legal?.bik &&
                            <small className="text-red-500">{formik.errors?.legal?.bik}</small>}
                    </div>
                </Grid>
            </div>
            <div className="flex items-center">
                <input type="checkbox"
                    name="agree"
                    className={[formik.errors.agree && formik.touched.agree ? 'border-red-400' : ''].join(" ")}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    checked={formik.values.agree}
                />
                <span className={["ml-3  text-sm", formik.errors.agree && formik.touched.agree ? 'text-red-500' : ''].join(" ")} >Я принимаю
                    <a className="text-blue-accent hover:underline" target="_blank" rel="noreferrer"
                        href="https://docs.google.com/document/d/1Ij96hEV0Td61abi6zQOzqNCSQYqXxPf5clW3oZ3RCuE/edit"> оферту на оказание услуг
                    </a>
                </span>
            </div>
        </div>




        <div className="flex justify-end py-3 bg-gray-50">
            {loading ? <div className="flex items-center mr-5">
                <small>Подажите...</small>
                <Loader className="w-5 h-5" size={16} />
            </div> : <>
                <button className="button btn-danger" onClick={formik.handleReset}>Отменить</button>
                <button className="button" type="submit">Сохранить</button>

            </>
            }
        </div>

    </form>
}

export default CreateStoreForm;