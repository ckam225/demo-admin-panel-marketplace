import React from 'react';
import Loader from '../../../components/controls/Loader';
import TableEmptyRow from '../../../components/controls/TableEmptyRow';
import StoreTableRow from './StoreTableRow';


const StoreCollection = ({stores = [], loading=false, onItemClick}) => {
    return  <table className="table">
    <thead>
        <tr>
            <th>#</th>
            <th>Код</th>
            <th>Название</th>
            <th>Статус</th>
            <th>нормер телефона</th>
            <th>зарегистрировано</th>
            <th>Баланс</th>
        </tr>
    </thead>
    <tbody>
        {loading ? <TableEmptyRow colspan={7} className="flex justify-center py-5">
            <Loader />
        </TableEmptyRow> :
            stores.length > 0 ? stores.map(store => <StoreTableRow key={store.id} store={store} onItemClick={onItemClick}/>) :
                <TableEmptyRow colspan={7} className="text-center py-5">Нет партнёров</TableEmptyRow>}
    </tbody>
</table>
}
 
export default StoreCollection;