import React from "react"
import styled from "styled-components"
import { useStoreOrders } from "../../../store/actions/stores"
import { DateTime } from "luxon"
import { currency } from "../../../helpers";
import Image from "../../../components/controls/Image";
import Paginator from "../../../components/controls/Paginator";
import TableEmptyRow from "../../../components/controls/TableEmptyRow";
import Loader from "../../../components/controls/Loader";


const StoreOrders = ({ storeId }) => {

    const [tab, setTab] = React.useState(0)
    const [selectedOrder, setSelectedOrder] = React.useState(null)
    const { orders, fetchOrders, pageCount, loading } = useStoreOrders()
    const [page, setPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)

    const handleTabClick = (t) => {
        setTab(t)
        setSelectedOrder(null)
    }

    const handleClickOrder = (order) => {
        setSelectedOrder(order)
    }

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    const query = React.useMemo(() => {
        let q = `?page=${page}&per_page=${perPage}`
        if (tab === 0)
            q += `&store_id=${storeId}`
        if (tab === 1)
            q += `&store_id=${storeId}&type=true`
        if (tab === 2)
            q += `&store_id=${storeId}&type=false`
        return q
    }, [storeId, tab, page, perPage])

    React.useEffect(() => {
        fetchOrders(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])


    return <Wrapper>
        <div className="flex">
            <div className="flex-auto">
                <div className="flex tab-switcher rounded-lg py-3  px-2">
                    <button className={`rounded-l-sm btn-tab-switcher ${tab === 0 ? 'active' : ''}`} onClick={() => handleTabClick(0)}>Все</button>
                    <button className={`btn-tab-switcher ${tab === 1 ? 'active' : ''}`} onClick={() => handleTabClick(1)}>Продажи</button>
                    <button className={`rounded-r-sm btn-tab-switcher ${tab === 2 ? 'active' : ''}`} onClick={() => handleTabClick(2)}>Возраты</button>
                </div>

                <table className="table px-2">
                    <thead>
                        <tr>
                            <th>Номер заказа</th>
                            <th>Статус</th>
                            <th>Дата</th>
                            <th>Кол-во товаров</th>
                            <th>Сумма заказа</th>
                        </tr>
                    </thead>
                    <tbody>
                        {loading ? <TableEmptyRow colspan={5} className="flex justify-center  py-5">
                            <Loader />
                        </TableEmptyRow> : orders.length > 0 ? orders.map(order => <tr key={order.id} onClick={() => handleClickOrder(order)}>
                            <td>{order.number}</td>
                            <td>{order?.status?.title}</td>
                            <td>
                                {order.created_at && <span>{DateTime.fromISO(order.created_at).setLocale('ru').toFormat('dd LLL yyyy, hh:mm')}</span>}
                            </td>
                            <td>{order?.products.length}</td>
                            <td>{currency.format(order.amount || 0)}</td>
                        </tr>) : <TableEmptyRow colspan={5} className="text-sm text-center py-5">Нет заказов</TableEmptyRow>
                        }
                    </tbody>
                </table>

                {!loading && <div className="py-2 flex justify-between items-center bg-gray-50">
                    {(orders.length > 0 && pageCount > 1) ? <Paginator className="text-blue-accent"
                        itemClass=" text-sm rounded-full mx-1 w-6 h-6 focus:outline-none"
                        activeItemClass="pagin-item-color"
                        pages={pageCount}
                        setCurrentPage={setPage} /> : <div />}
                    <div className=" mx-3">
                        <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                            <option value="10">10</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                </div>}
            </div>
            {selectedOrder && <div className="flex flex-col justify-between border-l" style={{ width: selectedOrder ? '400px' : '0px' }}>
                <div className="overflow-y-auto" style={{ maxHeight: "500px" }}>
                    {selectedOrder?.products.map((product, p) => <div className="flex py-2 border-b" key={p}>
                        <div className="relative">
                            <Image src={product.photo} className="mx-2 w-14" alt={product?.name} />
                        </div>
                        <div className="flex flex-col pl-2" style={{ lineHeight: '14px' }}>
                            <span>{product.name}</span>
                            <span><b>{currency.format(product.price || 0)}</b></span>
                            <span>{product.quantity} шт.</span>
                        </div>
                    </div>)}
                </div>
                <div className=" flex flex-col py-4 px-3" style={{ background: '#dacbe9' }}>
                    <div className="flex justify-between">
                        <div>Кол-во товаров:</div>
                        <div>{selectedOrder?.products.length || 0}</div>
                    </div>
                    <div className="flex justify-between">
                        <div>Итоговая цена:</div>
                        <div className="text-2xl">{currency.format(selectedOrder?.amount || 0)}</div>
                    </div>
                </div>
            </div>}
        </div>
    </Wrapper>
}
const Wrapper = styled.div`
.tab-switcher{
 
}
.btn-tab-switcher{
    outline:none;
    border: none;
    padding: 2px 10px;
    background: transparent;
    border:1px solid var(--color-accent);
    color: var(--color-accent);
    font-size: 14px;
}
.btn-tab-switcher:first-child{
    border-right: 0;
}
.btn-tab-switcher:last-child{
    border-left: 0;
}
.btn-tab-switcher.active{
    background: var(--color-accent);
    color: #fff;
}
table{
    width: 100%;
}
table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`
export default StoreOrders;