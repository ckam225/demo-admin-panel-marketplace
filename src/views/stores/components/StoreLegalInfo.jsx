import React from "react"

const StoreLegalInfo = ({legal}) => {
    return  <table className="table-3 ">
    <tbody>
        <tr>
            <td>ФИО  директора(руководителя) магазина</td>
            <td>{legal?.director}</td>
        </tr>
        <tr>
            <td>Юридическйи Адрес</td>
            <td>{legal?.address}</td>
        </tr>
        <tr>
            <td>Форма собственности</td>
            <td>{legal?.form}</td>
        </tr>
        <tr>
            <td>Налогообложение</td>
            <td>{legal?.taxation_system}</td>
        </tr>
    </tbody>
</table>
}
 
export default StoreLegalInfo;