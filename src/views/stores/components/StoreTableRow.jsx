import React from 'react'
import { BaseLink } from '../../../components/shared/NavItem'
import { DateTime } from 'luxon'
import { currency } from '../../../helpers'

const StoreTableRow = ({ store, filter = 'all', onItemClick }) => {

    const statusClass = React.useMemo(() => {
        if (store?.status?.name === 'activated')
            return 'text-green-500'
        if (store?.status?.name === 'wait_checking')
            return 'text-yellow-500'
        if (store?.status?.name === 'blocked')
            return 'text-red-500'
        return ''
    }, [store])

    function handleClick (event) {
        event.stopPropagation()
        if(onItemClick){
            onItemClick(store)
        }
    }

    return <tr key={store.id} onClick={handleClick}>
        <td>{store.id}</td>
        <td>{store.code_1c}</td>
        <td>{store.name}</td>
        <td><span className={statusClass}>{store.status?.title}</span></td>
        <td><span>{store.contact}</span></td>
        <td>
            {store.created_at && <span>{DateTime.fromISO(store.created_at).setLocale('ru').toFormat('dd LLL yyyy, hh:mm')}</span>}
        </td>
        <td>
            <span className="">{currency.format(store.balance)}</span>
        </td>
    </tr>
}

export default StoreTableRow