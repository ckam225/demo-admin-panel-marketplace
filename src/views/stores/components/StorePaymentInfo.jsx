import React from "react"


const StorePaymentInfo = ({legal}) => {
    return <table className="table-3 ">
       <tbody>
            {/* {legal?.inn && <tr>
                <td>ИНН</td><td>{legal.inn}</td>
            </tr>}
            {legal?.ogrnip && <tr>
                <td>ОГРНИП</td><td>{legal?.ogrnip}</td>
            </tr>}
            {legal?.payment_account && <tr>
                <td>Расчетный счет</td><td>{legal?.payment_account}</td>
            </tr>}
            {legal?.bik && <tr>
                <td>БИК банка</td><td>{legal?.bik}</td>
            </tr>}
            {legal?.kpp && <tr>
                <td>КПП</td><td>{legal?.kpp}</td>
            </tr>}
            {legal?.ogrn && <tr>
                <td>ОГРН</td><td>{legal?.ogrn}</td>
            </tr>} */}

            <tr>
                <td>ИНН</td><td>{legal?.inn || '---'}</td>
            </tr>
            <tr>
                <td>ОГРНИП</td><td>{legal?.ogrnip || '---'}</td>
            </tr>
            <tr>
                <td>Расчетный счет</td><td>{legal?.payment_account || '---'}</td>
            </tr>
            <tr>
                <td>БИК банка</td><td>{legal?.bik || '---'}</td>
            </tr>
            <tr>
                <td>КПП</td><td>{legal?.kpp || '---'}</td>
            </tr>
            <tr>
                <td>ОГРН</td><td>{legal?.ogrn || '---'}</td>
            </tr>
        </tbody>
    </table>
}
 
export default StorePaymentInfo;