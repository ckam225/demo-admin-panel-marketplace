
import React from 'react';
import styled from 'styled-components'
import Icon from '../../../components/controls/Icon';
import Modal from '../../../components/controls/Modal';
import Tab from '../../../components/controls/Tab';
import { BaseLink } from '../../../components/shared/NavItem';
import { currency } from '../../../helpers';
import { storeStatus, useStoreStatusClass } from '../../../store/actions/stores';
import ProductDataTable from '../../products/components/ProductDataTable';
import StoreAmounts from './StoreAmounts';
import StoreDocuments from './StoreDocuments';
import StoreLegalInfo from './StoreLegalInfo';
import StoreOrders from './StoreOrders';
import StoreOverview from './StoreOverview';
import StorePaymentInfo from './StorePaymentInfo';
import StoreWorktimes from './StoreWorktimes';

const StoreInfo = ({store, updateLoading, handleUpdate, showBackButton=true, isModerate=false, storeStatuses = []}) => {

    const {statusClass, statusTextClass} = useStoreStatusClass(store)
    const [status, setStatus] = React.useState(store?.status.id)
    const modal = React.useRef()
    const textArea = React.useRef()

    function handleStatusChange(e){
        setStatus(Number.parseInt(e.target.value))
        modal.current.open()
    }

    function handleCancelUpdate(){
        setStatus(store?.status.id)
        modal.current.close()
    }

    function handleSave(){
        if(status !== storeStatus.ACTIVATED && textArea.current.value === ''){
            alert('Вводите вашие замечания')
        }else{
            handleUpdate({
                status_id: status,
                exceptions: textArea.current.value
            })
            textArea.current.value = ''
            modal.current.close()
        }
    }


    return <><Wrapper>
    <div className="bg-white  flex justify-between mx-5 my-4 p-5">
        <div className="flex items-center">
            {showBackButton && <BaseLink to="/stores" className="icon-button border-0 mr-3">
                <Icon name="arrow_left" />
            </BaseLink>}
            <div className={['w-10 h-10 rounded-full flex items-center justify-center', statusClass].join(' ')}>
                <Icon name="user_group" />
            </div>
            <h1 className="text-3xl font-light mx-3">{store?.name}</h1>
        </div>
        <div className="flex flex-col">
            <div className="flex">
                <b className="pr-2">Статус: </b>
                <span className={statusTextClass}>{store?.status?.title}</span>
            </div>
            <div className="flex">
                <b className="pr-2">Баланс: </b>
                <span >{currency.format(store?.balance || 0)}</span>
            </div>

        </div>
    </div>

    <Tab>
        <Tab.Headers className="flex sticky inset-x-0 mx-5 top-0 left-0 bg-white">
            <Tab.Header tab={0}>Сведение о магазине</Tab.Header>
            <Tab.Header tab={1}>Юридическая информация</Tab.Header>
            <Tab.Header tab={2}>График работы склада</Tab.Header>
            <Tab.Header tab={3}>Заказы</Tab.Header>
            <Tab.Header tab={4}>Расчеты</Tab.Header>
            <Tab.Header tab={5}>Товары</Tab.Header>
        </Tab.Headers>
        <Tab.Content className="mx-5">
            <Tab.Item tab={0}>
                <div className="bg-white px-2">
                    <StoreOverview store={store} />
                </div>
            </Tab.Item>
            <Tab.Item tab={1}>
                <div className="bg-white px-2">
                    <div className="pt-8">
                        <StoreLegalInfo legal={store?.legal} />
                    </div>
                    <div className="my-8">
                        <h1 className="font-bold my-3 ml-1 text-gray-900">Платежные реквизиты</h1>
                        <StorePaymentInfo legal={store?.legal} />
                    </div>
                    <div className="my-8">
                        <h1 className="font-bold my-3 ml-1 text-gray-900">Документы</h1>
                        <StoreDocuments documents={store?.documents} />
                    </div>
                </div>
            </Tab.Item>
            <Tab.Item tab={2}>
                <div className="bg-white px-2">
                    <StoreWorktimes worktimes={store?.worktimes} />
                </div>
            </Tab.Item>
            <Tab.Item tab={3}>
                <div className="bg-white">
                    <StoreOrders storeId={store?.id} />
                </div>
            </Tab.Item>
            <Tab.Item tab={4}>
                <div className="bg-white px-2">
                    <StoreAmounts storeId={store?.id} />
                </div>
            </Tab.Item>
            <Tab.Item tab={5}>
                <div className="bg-white px-2">
                    <ProductDataTable storeId={store?.id} />
                </div>
            </Tab.Item>
        </Tab.Content>
    </Tab>

    {isModerate && <div className="flex items-center justify-between mx-5 my-2 p-5 bg-white">
        <div className="flex items-center">
            <div className="flex-none text-red-500 mr-4 text-xs">Сменить статус</div>
            <select  placeholder="Виберите статус" defaultValue={Number.parseInt(store?.status?.id)} onChange={handleStatusChange} >
                {storeStatuses.map(st => <option key={st.id} value={st.id} >
                    {st?.title}</option>
                )}
            </select>
        </div>
      </div>
    }

    

</Wrapper>
{isModerate && <Modal refId={modal} style={{width: "500px"}}>
    <Modal.Header>
       Подтверждающее действие
    </Modal.Header>
    <Modal.Body className="bg-gray-200 p-5">
        <textarea ref={textArea} cols="30" rows="10" placeholder="Вводите вашие замечания"></textarea>
        <div className="flex items-center justify-between p-5 bg-white">
            <button className="button" onClick={handleSave}>
              Сохранить
            </button>
            <button className="button btn-danger" onClick={handleCancelUpdate}>
              Отменить
            </button>
        </div>
    </Modal.Body>
</Modal>}
</>
}

const Wrapper = styled.div`

`
export default StoreInfo;