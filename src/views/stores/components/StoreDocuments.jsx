import React from "react"

const StoreDocuments = ({ documents = [] }) => {
    return <table className="table-3 ">
        <tbody>
            {documents.length > 0 && documents.map(doc => <tr key={doc.id}>
                <td>{doc.title}</td>
                <td>
                    {doc.links.length > 0 ? <ul>
                        {doc?.links.map((doclink, d) =>
                            <li key={d} ><a className="mr-4 text-blue-600 hover:underline" href={doclink} >{doclink}</a></li>
                        )}
                    </ul> : <span> ---- </span>
                    }
                </td>
            </tr>)}
        </tbody>
    </table>
}

export default StoreDocuments;