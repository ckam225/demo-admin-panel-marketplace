import React from 'react'
import SettingLayout from './components/SettingLayout'
import NotificationDataTable from "./components/NotificationDataTable";


const Notifications = () => {
    return (<SettingLayout>
        <div className="flex">
            <div className="flex flex-col flex-none w-full">
                <div className="card my-4 mx-3">
                    <NotificationDataTable />
                </div>
            </div>
        </div>
    </SettingLayout>);
}

export default Notifications;