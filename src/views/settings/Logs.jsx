import React from 'react'
import { useRecoilValue } from 'recoil'
import useLogs from '../../store/actions/logs'
import { userRoleState } from '../../store/states/base'
import SettingLayout from './components/SettingLayout'

const Logs = () => {
    const { logs, truncate } = useLogs()
    const { isSuperuser } = useRecoilValue(userRoleState)

    return <SettingLayout>
        <div className="flex">
            <div className="flex flex-col flex-none w-full">
                <div className="card my-4 mx-3">
                    <table className="table bg-white">
                        <tbody>
                            {logs.length > 0 && logs.map((log, i) => <tr key={i}>
                                <td>
                                    <a href={log.url} className="hover:underline cursor-pointer">{log.name}</a>
                                </td>
                                <td>{log.size} KB</td>
                                <td>
                                    <div className="flex justify-end">
                                        <a
                                            href={log.url}
                                            className="border px-2 hover:cursor-pointer focus:outline-none bg-gray-100 rounded-sm mr-1"
                                        >Скачать</a>
                                        {isSuperuser && <button
                                            className="border px-2 hover:cursor-pointer focus:outline-none bg-gray-100 rounded-sm"
                                            onClick={() => truncate(log.name)}
                                        >Очистить</button>}
                                    </div>
                                </td>
                            </tr>)}
                        </tbody>
                    </table>
                </div>
            </div>
        </div >
    </SettingLayout>
}

export default Logs;