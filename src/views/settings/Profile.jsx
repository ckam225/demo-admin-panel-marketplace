import React from 'react'
import SettingLayout from './components/SettingLayout';
import Icon from '../../components/controls/Icon';
import useAuthUser from '../../store/actions/auth';
import { useFormik } from 'formik'
import { updateProfileValidationRule } from '../../hooks/validators';

const Profile = () => {

    const { user } = useAuthUser()

    const formik = useFormik({
        initialValues: {
            email: Object.assign(user?.email || ""),
            name: Object.assign(user?.name || ""),
            phone: Object.assign(user?.phone || ""),
            password: "",
        },
        validationSchema: updateProfileValidationRule,
        onSubmit: async (values) => {

        },
    })


    return <SettingLayout>
        <div className="flex flex-col ">
            <div className="flex p-10">
                <div className="flex items-start flex-none">
                    <span className="rounded-full h-20 w-20 flex items-center justify-center text-white bg-opacity-80 p-1 bg-blue-accent">
                        <Icon name="user" className="w-8 h-8 " />
                    </span>
                </div>
                <form className="flex-1 ml-5" onSubmit={formik.handleSubmit}>

                    <div>
                        <div className="mb-3">
                            <div className="text-gray-500">Имя</div>
                            <input type="text" className="border-0 ring-0 focus:ring-0 border-b"
                                name="name"
                                value={formik.values.name}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange} />
                            {formik.errors.name && formik.touched.name && (
                                <small className="text-red-500">{formik.errors.name}</small>
                            )}
                        </div>
                        <div className="mb-3">
                            <div className="text-gray-500">Почта</div>
                            <input type="email" className="border-0 ring-0 focus:ring-0 border-b pl-0"
                                name="email"
                                value={formik.values.email}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange} />
                            {formik.errors.email && formik.touched.email && (
                                <small className="text-red-500">{formik.errors.email}</small>
                            )}
                        </div>
                        <div className="mb-3">
                            <div className="text-gray-500">Номер телефона</div>
                            <input type="text" className="border-0 ring-0 focus:ring-0 border-b"
                                name="phone"
                                value={formik.values.phone}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange} />
                            {formik.errors.phone && formik.touched.phone && (
                                <small className="text-red-500">{formik.errors.phone}</small>
                            )}
                        </div>
                    </div>

                    <div  >
                        <div className="mb-3">
                            <div className="text-gray-500">Пароль</div>
                            <input type="password" className="border-0 ring-0 focus:ring-0 border-b pl-0"
                                name="password"
                                value={formik.values.password}
                                onBlur={formik.handleBlur}
                                onChange={formik.handleChange} />
                            {formik.errors.password && formik.touched.password && (
                                <small className="text-red-500">{formik.errors.password}</small>
                            )}
                        </div>
                    </div>
                </form>
            </div>

            <div className="h-20 flex justify-end items-center px-10">
                <button className="button" >Save change</button>
            </div>
        </div>

    </SettingLayout>;
}

export default Profile;