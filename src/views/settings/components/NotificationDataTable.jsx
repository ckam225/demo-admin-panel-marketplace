import React from 'react';
import styled from "styled-components";
import useNotifications from "../../../store/actions/notifications";
import Loader from "../../../components/controls/Loader";
import TableEmptyRow from "../../../components/controls/TableEmptyRow";
import NotificationTableRow from './NotificationTableRow'
import { endOt } from "../../../helpers";
import Modal from "../../../components/controls/Modal";
import NotificationDetail from "./NotificationDetail";
import Paginator from "../../../components/controls/Paginator";

const NotificationDataTable = () => {

    const { notifications, fetchNotifications, loading, pageCount, fetchCurrentNotification, currentNotification, markAsRead, updateNotification, deleteNotification } = useNotifications()
    const [loadingCurrent, setLoadingCurrent] = React.useState(false)
    const modal = React.useRef()
    const [page, setPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)
    const [action, setAction] = React.useState('mark_as_read')

    async function handleSelectNotification(notificationId) {
        modal.current.open()
        setLoadingCurrent(true)
        await fetchCurrentNotification(notificationId)
        setLoadingCurrent(false)
    }

    function handleChecked(e, notificationId) {
        updateNotification({ isChecked: e.target.checked }, notificationId)
    }

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    function handleChangeAll(e) {
        updateNotification({ isChecked: e.target.checked })
    }

    function handleActionChange(e) {
        setAction(e.target.value)
    }

    function handleActionApply(e) {
        const notificationIDs = notifications.filter(notify => notify.isChecked).map(notify => notify.id)
        switch (action) {
            case 'mark_as_read':
                markAsRead(notificationIDs);
                break;
            case 'delete':
                deleteNotification(notificationIDs);
                break;
            default:
                return
        }
        updateNotification({ isChecked: false })
    }

    const query = React.useMemo(() => {
        let q = `?page=${page}&per_page=${perPage}`
        return q
    }, [page, perPage])

    React.useEffect(() => {
        fetchNotifications(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    return <Wrapper>
        <table className="table bg-white">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" onChange={handleChangeAll} />
                    </th>
                    <th>
                        <div className="flex items-center py-4">
                            <div className="mx-3">
                                <select onChange={handleActionChange}>
                                    <option value="mark_as_read">Пометить как прочитаное</option>
                                    <option value="delete">Удалить</option>
                                </select>
                            </div>
                            <button type="button" className="btn" onClick={handleActionApply}>Применить</button>
                        </div>
                    </th>
                    <th>
                        Дата
                    </th>
                </tr>
            </thead>
            <tbody>
                {loading ? <TableEmptyRow colspan={9} className="flex justify-center  py-5">
                    <Loader />
                </TableEmptyRow> : notifications.length > 0 ?
                    notifications.length > 0 && notifications.map(notification => <NotificationTableRow notification={notification} onSelected={handleSelectNotification}
                        isChecked={notification.isChecked} onChecked={handleChecked}
                        className={!notification.read_at ? 'bg-gray-100' : ''}
                        key={notification.id}
                    />) : <TableEmptyRow colspan={9} className="text-sm text-center py-5">Нет уведомлений</TableEmptyRow>}
            </tbody>
        </table>
        {!loading && <div className="py-2 flex justify-between items-center bg-gray-50">
            {(notifications.length > 0 && pageCount > 1) ? <Paginator className="text-blue-accent"
                itemClass=" text-sm rounded-full mx-1 w-6 h-6 focus:outline-none"
                activeItemClass="pagin-item-color"
                pages={pageCount}
                setCurrentPage={setPage} /> : <div />}
            <div className=" mx-3">
                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                    <option value="10">10</option>
                    <option value="30" >30</option>
                    <option value="50" >50</option>
                    <option value="100">100</option>
                </select>
            </div>
        </div>}

        <Modal refId={modal} width="900">
            <Modal.Header>
                <div className="flex justify-between">
                    <span>{endOt(currentNotification?.data.title, 60)}</span>
                </div>
            </Modal.Header>
            <Modal.Body>
                <div className="flex  overflow-hidden overflow-y-auto" style={{ height: '600px' }}>
                    {loadingCurrent ? <Loader /> : <NotificationDetail notification={currentNotification} />}
                </div>
            </Modal.Body>
        </Modal>
    </Wrapper>
}

const Wrapper = styled.div`
table{
    width: 100%;
    user-select: none;
}
table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`

export default NotificationDataTable;