import useNotifications from "../../../store/actions/notifications";

const NotificationTableRow = ({ notification, className, onSelected, isChecked, onChecked }) => {

    const { markAsRead } = useNotifications()

    async function handleClick() {
        await markAsRead(notification.id)
        onSelected(notification?.id)
    }

    function handleCheck(e) {
        onChecked(e, notification.id)
    }

    return <tr className={className}>
        <td><input type="checkbox" checked={isChecked} onChange={handleCheck} /></td>
        {/*<td>{notification?.title}</td>*/}
        <td onClick={handleClick}>{notification?.message}</td>
        <td>{notification?.created_at}</td>
    </tr>
}

export default NotificationTableRow;