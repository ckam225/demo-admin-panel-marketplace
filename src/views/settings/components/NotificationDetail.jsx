import React from 'react';

const NotificationDetail = ({ notification }) => {

    return <div className=" w-full select-none">
                <div className="flex">
                    <table className="table bg-white m-4">
                        <thead>
                        <tr>
                            <th>Поле</th>
                            <th>Значение</th>
                        </tr>
                        </thead>
                        <tbody>
                        {notification ? Object.entries(notification).map(entry => <tr>
                            <td>{entry[0]}</td>
                            <td>{typeof entry[1] == 'string' ? entry[1] : JSON.stringify(entry[1])}</td>
                        </tr>) : null}
                        </tbody>
                    </table>
                </div>
            </div>
}

export default NotificationDetail;