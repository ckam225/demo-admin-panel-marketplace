import React from 'react'
import styled from 'styled-components'
import NavTabs from '../../../components/controls/NavTabs';
import MainLayout from '../../../components/layouts/Main';

const SettingLayout = (props) => {
    const routes = [
        {
            path: '/settings',
            title: 'Общее',
            exact: true
        },
        {
            path: '/settings/logs',
            title: 'Логи',
            exact: false
        },
        {
            path: '/settings/profile',
            title: 'Профиль',
            exact: false
        },
        {
            path: '/settings/notifications',
            title: 'Уведомбления',
            exact: false
        }
    ]

    return <Wrapper>
        <MainLayout>
            <MainLayout.Head>

            </MainLayout.Head>
            <MainLayout.Container>
                <div className="bg-white m-3">
                    <NavTabs routes={routes} />
                    {props.children}
                </div>
            </MainLayout.Container>
        </MainLayout>
    </Wrapper>;
}
const Wrapper = styled.div`

`
export default SettingLayout;