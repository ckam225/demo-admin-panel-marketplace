import React, { useRef } from 'react';
import Modal from '../../../components/controls/Modal';
import SearchBox from '../../../components/controls/SearchBox';
import useStores, { storeStatus, useStoreStatuses } from '../../../store/actions/stores';
import StoreCollection from '../../stores/components/StoreCollection';
import StoreInfo from '../../stores/components/StoreInfo';




const StoreModeration = () => {
    const { stores, fetchStores, loading } = useStores()
    const {statuses} = useStoreStatuses()
    const { partialUpdateStore } = useStores()
    const [perPage, setPerPage] = React.useState(30)
    const [search, setSearch] = React.useState('')
    const [currentPage, setCurrentPage] = React.useState(1)
    const [currentStore, setCurrentStore] = React.useState()
    const [updateLoading, setUpdateLoading] = React.useState(false)

    const modal = useRef()


    const query = React.useMemo(() => {
        let q = `?page=${currentPage}&per_page=${perPage}&status=${storeStatus.WAITCHECKING}`
        if (search.length > 0)
            q += '&name=' + search
        return q
    }, [search, currentPage, perPage])

    React.useEffect(() => {
        fetchStores(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    function handleSearchChange(s) {
        setSearch(s)
    }

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    function handleItemClick(store){
       setCurrentStore(store)
       modal.current.open()
    }

    const handleUpdate = React.useCallback(async (payload) => {
        if (currentStore) {
            setUpdateLoading(true)
            await partialUpdateStore(currentStore.id, payload)
            setUpdateLoading(false)
            setTimeout(fetchStores(query), 0)
            modal.current.close()
        }
        
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentStore])


    return (<>
    <div className="bg-white p-3 my-3" >

        <div className="flex items-center justify-between border-b pb-2">
            <SearchBox onChange={handleSearchChange} placeholder="Поиск по назвонию" className="mr-2 py-1 rounded-sm text-sm" />
            <div className=" mx-3">
                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                    <option value="10">10</option>
                    <option value="30" >30</option>
                    <option value="50" >50</option>
                    <option value="100" >100</option>
                </select>
            </div>
        </div>
        <StoreCollection stores={stores} loading={loading} onItemClick={handleItemClick}/>
    </div>
    <Modal refId={modal} style={{width: '80%'}}>
        <Modal.Header>

        </Modal.Header>
        <Modal.Body className="bg-gray-200">
            {currentStore && <StoreInfo
             store={currentStore}
             handleUpdate={handleUpdate}
             updateLoading={updateLoading}
             showBackButton={false}
             storeStatuses={statuses}
             isModerate={true}
            />}
        </Modal.Body>
    </Modal>
    </>
)
}
 
export default StoreModeration;