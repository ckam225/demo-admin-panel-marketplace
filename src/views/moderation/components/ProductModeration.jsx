import React from 'react';
import ProductDataTable from '../../products/components/ProductDataTable';


const ProductModeration = () => {
    return <div className="m-3 bg-white">
    <div className="flex justify-between px-3  py-2 border-b">
        <div className="text-xl">Список товаров</div>
    </div>
    <ProductDataTable isFullPage={true} isModerationMode={true}/>
</div>
}
 
export default ProductModeration;