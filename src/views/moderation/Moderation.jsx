import React from 'react'
import styled from 'styled-components'
import MainLayout from '../../components/layouts/Main';
import Tab from '../../components/controls/Tab'
import StoreModeration from './components/StoreModeration';
import ProductModeration from './components/ProductModeration';


const ModerationLayout = (props) => {
    
    return <Wrapper>
        <MainLayout>
            <MainLayout.Head>

            </MainLayout.Head>
            <MainLayout.Container>
                <div className="bg-white m-3">
                    <Tab>
                        <Tab.Headers className="flex sticky inset-x-0 top-0 left-0 bg-white">
                            <Tab.Header tab={0}>Продавцы</Tab.Header>
                            <Tab.Header tab={1}>Товары</Tab.Header>
                        </Tab.Headers>
                        <Tab.Content className="mx-5">
                            <Tab.Item tab={0}>
                                 <StoreModeration/>
                            </Tab.Item>
                            <Tab.Item tab={1}>
                                 <ProductModeration/>
                            </Tab.Item>
                        </Tab.Content>
                    </Tab>
                </div>
            </MainLayout.Container>
        </MainLayout>
    </Wrapper>;
}
const Wrapper = styled.div`

`
export default ModerationLayout;