import React from 'react'
import MainLayout from '../components/layouts/Main'
// import SwaggerUI from 'swagger-ui-react'
// import "swagger-ui-react/swagger-ui.css"
// import doc from '../assets/docs/openapi.yaml'


const Docs = () =>
    <MainLayout >
        <MainLayout.Head>
        </MainLayout.Head>
        <MainLayout.Container className="h-full">
        {/* <SwaggerUI url={doc} /> */}
         <iframe src={process.env?.REACT_APP_DOC_URL}  height="100%" title="doc-api"/>
        </MainLayout.Container>
 </MainLayout>
export default Docs;