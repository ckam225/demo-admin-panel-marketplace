import React from 'react'
import Confirm from '../../../components/controls/Confirm';
import Dialog from '../../../components/controls/Dialog';
import Modal from '../../../components/controls/Modal';
import Toast, { ToastContext } from '../../../components/controls/Toast';

const Modals = () => {
    const modal = React.useRef(null);
    const dialog = React.useRef(null)
    const confirmBox = React.useRef(null)

    const { toast } = React.useContext(ToastContext)

    return (
        <div >
            <div className="flex mt-4">
                <button onClick={() => modal.current.open()} className="btn  btn-primary text-sm ml-5">Modal</button>
                <button onClick={() => dialog.current.open()} className="btn btn-primary text-sm ml-5">Dialog</button>
                <button onClick={() => toast.show('Lorem asdkajs')} className="btn btn-primary text-sm ml-5">Toast</button>
                <button onClick={() => confirmBox.current.open()} className="btn btn-primary text-sm ml-5">Confirm</button>

            </div>
            <Modal refId={modal} style={{ width: '600px' }}>
                <Modal.Header>
                    <div className="flex items-center w-full pr-2">
                        <span>Example Modal</span>
                    </div>
                </Modal.Header>
                <Modal.Body>
                    <div className="flex flex-col">
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                        Atque dolor expedita ducimus laudantium inventore,
                        sed, earum quibusdam aliquam ad dolores
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                        Atque dolor expedita ducimus laudantium inventore,
                        sed, earum quibusdam aliquam ad dolores
                    </div>
                </Modal.Body>
            </Modal>

            <Dialog refId={dialog}>
                <Dialog.Header>
                    Are you sure?
                </Dialog.Header>
                <Dialog.Content>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit.
                    Atque dolor expedita ducimus laudantium inventore,
                    sed, earum quibusdam aliquam ad dolores
                </Dialog.Content>
            </Dialog>
            <Toast />
            <Confirm refId={confirmBox} dismissible={false} actions={[
                {
                    label: 'Cancel',
                    click: () => {
                        alert('canceled')
                    }
                }, {
                    label: 'Confirm',
                    className: 'text-red-500',
                    click: () => {
                        alert('confirmed')
                    }
                }
            ]}>
                <div className="text-center font-bold pt-8">Are you sure?</div>
                <div className="text-center my-4">Lorem ipsum, dolor sit amet consectetur adipisicing elit.</div>
            </Confirm>
        </div>
    );
}

export default Modals;