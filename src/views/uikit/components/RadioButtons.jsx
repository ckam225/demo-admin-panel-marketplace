import React from 'react';
import RadioGroup from '../../../components/controls/RadioGroup'
const plans = [
    {
        name: "Startup",
        ram: "12GB",
        cpus: "6 CPUs",
        disk: "160 GB SSD disk",
    },
    {
        name: "Business",
        ram: "16GB",
        cpus: "8 CPUs",
        disk: "512 GB SSD disk",
    },
    {
        name: "Enterprise",
        ram: "32GB",
        cpus: "12 CPUs",
        disk: "1024 GB SSD disk",
    },
];

const RadioButtons = () => {

    function test(e) {
    }
    function test2(e) {
    }

    return (
        <div className="flex flex-col">
            <h1 className="text-blue-500">Simples</h1>
            <RadioGroup onChange={test2} className="flex">
                <RadioGroup.Option value="Python" className="mt-3 p-4" activeClass="border-blue-200">
                    <span className="ml-2 text-gray-700">Python</span>
                </RadioGroup.Option>

                <RadioGroup.Option value="PHP" className="mt-3  p-4" activeClass="border-blue-200">
                    <span className="ml-2 text-gray-700">PHP</span>
                </RadioGroup.Option>

            </RadioGroup>

            <h1 className="text-blue-500">Color</h1>
            <RadioGroup onChange={test}>
                {plans.map((p, i) => {
                    return (
                        <RadioGroup.Option key={i} value={p.name} className="mt-3 border rounded-sm w-auto p-4" activeClass="bg-green-400 text-white">
                            <span className="ml-2 ">{p.name}</span>
                            <span className="ml-2 ">{p.ram}</span>
                        </RadioGroup.Option>
                    )
                })}
            </RadioGroup>

            <h1 className="text-blue-500">Rounded</h1>
            <RadioGroup onChange={test2} className="flex">
                <RadioGroup.Option value="Python" className="mt-3 mr-2 border rounded-full px-10 py-3" activeClass="border-blue-200">
                    <span className="ml-2 text-gray-700">Python</span>
                </RadioGroup.Option>

                <RadioGroup.Option value="PHP" className="mt-3 mr-3 border rounded-full px-6" activeClass="border-blue-200">
                    <span className="ml-2 text-gray-700">PHP</span>
                </RadioGroup.Option>

            </RadioGroup>
        </div>
    )
}

export default RadioButtons
