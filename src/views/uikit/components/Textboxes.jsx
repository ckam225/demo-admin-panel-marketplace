import Button from "../../../components/controls/Button";
import SearchBox from "../../../components/controls/SearchBox";
import Textbox from "../../../components/controls/Textbox";

const Textboxes = () => {
    return (
        <div className="flex flex-col p-10">
            <Textbox className="my-4" placeholder="normal" />
            <Textbox className="my-4" placeholder="readonly" readOnly={true} />
            <SearchBox placeholder="search" className="py-5" />
            <Button>Click me</Button>
        </div>
    );
}

export default Textboxes;