import React from 'react';
import Select from '../../components/controls/Select';
import Layout from '../../components/layouts/Base';
import MenuItems from './components/MenuItem';
import Modals from './components/Modals';
import RadioButtons from './components/RadioButtons'
import Textboxes from './components/Textboxes';
import SelectBox from '../../components/controls/SelectBox';



const TestView = () => {

    return <Layout>
        <Layout.Container>
            <div className="grid grid-cols-3 gap-4">
                <div className="card  p-10">
                    <h1>Radio buttons</h1>
                    <button className="btn btn-primary">Primary</button>
                    <button className="btn btn-default">Default</button>
                    <RadioButtons />
                </div>

                <div className="card  p-10">
                    <h1>Input fields</h1>
                    <Textboxes />
                </div>
                <div className="card  p-10">
                    <h1>Dropdowns</h1>
                    <MenuItems />
                    <div className="w-52">
                        <SelectBox items={['Java', 'Python', 'Go', 'PHP', { nn: 'C#' }]} displayField="nn" className="rounded-sm px-2 py-1" contentClass="rounded-md" onChange={(c) => {
                        }} />
                    </div>

                    <Select items={['Java', 'Python', 'Go', 'PHP', { nn: 'C#' }]} displayField="nn" className="rounded-sm" />
                </div>

                <div className="card  p-10">
                    <h1>Modals</h1>
                    <Modals />
                </div>
            </div>
        </Layout.Container>
    </Layout>
}

export default TestView;