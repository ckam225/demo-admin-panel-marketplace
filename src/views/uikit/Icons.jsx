import React, { useEffect, useMemo, useState } from 'react';
import sprite from '../../assets/sprite.svg'
import Icon from '../../components/controls/Icon'
import SearchBox from '../../components/controls/SearchBox'


const Icons = ({noBanner=false}) => {
    const [icons, setIcons] = useState([])
    const doc = document.createElement('div');
    const [cs, setCs] = useState('')

    const onSearchChange = (s) => {
        setCs(s)
    }

    const filterIcons = useMemo(() => {
        return icons.filter(ico =>   ico.toLowerCase().match(cs.toLowerCase()))
    }, [icons, cs])

    useEffect(()=>{
        fetch(sprite).then(res => res.text())
        .then(res=> {
            doc.innerHTML = res.replaceAll('stroke-linejoin', 'strokeLinejoin')
            .replaceAll('stroke-linecap', 'strokeLinecap')
            .replaceAll('stroke-width', 'strokeWidth')
            doc.style.visibility = 'hidden'
            document.body.appendChild(doc)
            const ics = []
            const spriteNode = document.querySelector("#sprite");
            const symbols = spriteNode.querySelectorAll("symbol");
            symbols.forEach(symbol => ics.push(symbol.id))
            setIcons(ics)
        })
        return () => {
            document.body.removeChild(doc)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return <div>
        {!noBanner && <div className="h-96 flex flex-col items-center justify-center bg-indigo-900">
            <div className="text-5xl text-white mb-2">Icons</div>
            <SearchBox inputClass="py-2" className="w-96" placeholder="поиск" onChange={onSearchChange}/>
            <div style={{ color: 'rgba(225,255,255, 0.4)' }}>{filterIcons.length} / {icons.length}</div>
        </div>}
        <div className="grid grid-cols-9 gap-3 mt-3">
       {filterIcons.map((icon,i) => <div className="flex flex-col items-center text-gray-500 hover:text-indigo-900 zoom" key={i}>
               <div className="border w-20 h-20 flex items-center justify-center hover:border-indigo-900">
                 <Icon name={icon}/>
               </div>
               <span>{icon}</span>
           </div>)}
       </div>
    </div>
}
 
export default Icons;