import React from 'react'
import StackLayout from '../../components/layouts/Stack'
import { useQuery } from '../../hooks/query'
import { navigate } from "@reach/router";
import styled from 'styled-components'

const NotAuthorizedView = () => {
  const { query } = useQuery()

  function redirect() {
    const path = query.get('redirect') || -1
    navigate(path)
  }

  return <Wrapper>
    <StackLayout>
      <StackLayout.Container>
        <h2 className="404" style={{ fontSize: "64px", color: "#fff" }}>401 | UNAUTHORIZED</h2>
        <button className="button" onClick={redirect}>Sign in</button>
      </StackLayout.Container>
    </StackLayout>
  </Wrapper>
}

const Wrapper = styled.div`
.button {
  outline: none;
  background-color: rgba(255, 255, 255, 0.1);
  padding: 5px 25px;
  color: #fff;
  border-radius: 3px;
  transition: all 0.3s;
}
.button:hover {
  background-color: rgba(255, 255, 255, 0.2);
}
`

export default NotAuthorizedView