import React from 'react'
import StackLayout from '../../components/layouts/Stack'


const NotFound = () => {
    return <StackLayout>
        <StackLayout.Head>
            <title>Маркетплейс: Page not found</title>
        </StackLayout.Head>
        <StackLayout.Container>
            <h2 className="404" style={{ fontSize: "64px", color: "#fff" }}>PAGE NOT FOUND</h2>
        </StackLayout.Container>
    </StackLayout>
}

export default NotFound