import React from 'react'
import logo from '../../assets/logo.svg'
import { useFormik } from "formik";
import { navigate } from "@reach/router";
import * as authApi from '../../api/auth'
import { toast } from "react-toastify";
import { loginValidationRule } from '../../hooks/validators'
import { useAuthentication, useAuthUser } from '../../store/actions/auth'
import { useQuery } from '../../hooks/query'
import Layout from '../../components/layouts/Base';

const LoginView = () => {
    const { setIsAuth } = useAuthentication()
    const { setAuthUser } = useAuthUser()
    const { query } = useQuery()

    function redirect() {
        const path = query.get('redirect') || -1
        navigate(path)
    }

    const formik = useFormik({
        initialValues: {
            email: "",
            password: "",
        },
        validationSchema: loginValidationRule,
        onSubmit: async (values) => {
            const response = await authApi.login({
                email: values.email,
                password: values.password,
                device_name: navigator.appName
            })
            if (response.error) {
                toast.error(response.data.error);
            } else {
                setIsAuth(true)
                setAuthUser(response.data)
                toast.success("Login successfully!");
                redirect()
            }
        },
    });
    return <Layout>
        <Layout.Head>
            <title>Авторизация</title>
        </Layout.Head>
        <Layout.Container>
            <div className="flex w-screen h-screen items-center justify-center bg-blue-fonce">
                {/* <div className="flex h-2/3 w-2/3 shadow-2xl">  */}
                <form
                    className="w-1/3 h-2/3 bg-black-30 flex flex-col items-center justify-center shadow-2xl"
                    onSubmit={formik.handleSubmit}>
                    <div className="w-62 h-62 flex flex-col items-center justify-center mb-12">
                        <img src={logo} alt="logo" className="w-32" />
                        <div className="text-3xl text-white">Маркетплейс</div>
                        <span className="text-gray-600 text-2xl">админка</span>
                    </div>

                    <input
                        type="email"
                        className="p-3 rounded-full w-3/5 border-none text-gray-50 bg-white-5 focus:bg-white-20 focus:ring-0"
                        placeholder="Имейл"
                        name="email"
                        required
                        value={formik.values.email}
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.email && formik.touched.email && (
                        <small className="text-red-500">{formik.errors.email}</small>
                    )}
                    <input
                        type="password"
                        className="p-3 m-3 text-gray-50 rounded-full w-3/5  border-none bg-white-10 focus:bg-white-20 focus:ring-0"
                        placeholder="Пароль"
                        name="password"
                        required
                        value={formik.values.password}
                        onBlur={formik.handleBlur}
                        onChange={formik.handleChange}
                    />
                    {formik.errors.password && formik.touched.password && (
                        <small className="text-red-500">{formik.errors.password}</small>
                    )}
                    <div className="flex justify-between w-3/5 items-center">
                        <a
                            href="/auth/forgot-password"
                            className="text-gray-500 transition-all duration-300 ease-in-out hover:text-gray-200"
                        >Забыл пароль?</a>
                        <button
                            type="submit"
                            className="text-white focus:outline-none py-2 px-12 bg-blue-900 rounded-full transition-all duration-300 ease-in-out hover:bg-blue-700"
                        >Войти</button>
                    </div>
                </form>
                {/* </div >  */}
            </div >
        </Layout.Container>
    </Layout>

}

export default LoginView