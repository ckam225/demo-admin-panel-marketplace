import React from 'react';
import Confirm from '../../../components/controls/Confirm';
import Icon from '../../../components/controls/Icon';
import { BaseLink } from '../../../components/shared/NavItem';
import useProducts from '../../../store/actions/products';
import ProductDetail from './ProductDetail';


const ProductInfo = ({ productId, isModerationMode = false, onUpdate }) => {
    const STATUS_ACTIVE = 'activated';
    const STATUS_WRONG = 'wrong';

    const { currentProduct, fetchCurrentProduct, moderateProduct, fetchProductChanges, currentProductChanges } = useProducts();
    const [confirmData, setConfirmData] = React.useState({
        title: '',
        message: '',
        moderationAction: '',
        action: () => { }
    });
    const [moderationExceptions, setModerationExceptions] = React.useState('');

    const confirmBox = React.useRef(null);

    /* moderation */
    function handleAcceptClick() {
        setConfirmData({
            title: "Уверены ли вы?",
            message: 'Вы действительно хотите Подтвердить этот товара?',
            moderationAction: 'accept',
            action: async () => await moderateProduct(productId, STATUS_ACTIVE)
        });
        confirmBox.current.open();
        if(onUpdate){
            onUpdate(currentProduct)
        }
    }

    function handleDeclineClick() {
        setConfirmData({
            title: "Уверены ли вы?",
            message: 'Вы действительно хотите Отклонить этот товара?',
            moderationAction: 'decline',
            action: async (moderationExceptions) => await moderateProduct(productId, STATUS_WRONG, moderationExceptions)
        });
        confirmBox.current.open();
        if(onUpdate){
            onUpdate(currentProduct)
        }
    }

    function handleExceptionsOnChange(e) {
        setModerationExceptions(e.target.value);
    }

    React.useEffect(() => {
        if(productId){
          fetchCurrentProduct(productId)
          fetchProductChanges(productId)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [productId])


    return <>
        <div>
           
                <div className="bg-white  flex justify-between mx-5 my-4 p-5">
                    <div className="flex items-center">
                        {!isModerationMode && <BaseLink to="/products" className="icon-button border-0 mr-3">
                            <Icon name="arrow_left" />
                        </BaseLink>}
                        <h1 className="text-3xl font-light mx-3">{currentProduct?.name}</h1>
                    </div>
                    <div className="flex flex-col">
                        <div className="flex">
                            <b className="pr-2">Статус: </b>
                            <span className="">{currentProduct?.status?.title}</span>
                        </div>
                        <div className="flex">
                            <b className="pr-2">Продавец: </b>
                            <BaseLink to={`/stores/${currentProduct?.store?.id}`}>{currentProduct?.store?.name}</BaseLink>
                        </div>
                    </div>
                </div>
                <div className="bg-white mx-5 my-4 p-5">
                    <ProductDetail product={currentProduct} changes={currentProductChanges} isModerationMode />
                </div>

                {currentProduct?.status?.name === 'checking' && <div className="bg-white mx-5 my-4 p-5">
                    <div className="flex justify-end w-4/4 items-center">
                        <button onClick={handleDeclineClick} className="button  btn-danger">Отклонить</button>
                        <button onClick={handleAcceptClick} className="button">Принять</button>
                    </div>
                </div>}
        </div>
        <Confirm refId={confirmBox} actions={[
            {
                label: 'отменить',
                click: () => { }
            }, {
                label: 'продолжить',
                className: 'text-red-500',
                click: () => {
                    confirmData.action(moderationExceptions);
                }
            }
        ]}>
            <div className="text-center font-bold pt-8">
                {confirmData.title}
            </div>
            <div className="text-center p-4">
                {confirmData.message}
            </div>
            {confirmData.moderationAction === 'decline' && <div className="text-center p-4">
                <label>Причина отклонения</label>
                <textarea value={moderationExceptions} onChange={handleExceptionsOnChange}></textarea>
            </div>}
        </Confirm>
    </>
}
 
export default ProductInfo;