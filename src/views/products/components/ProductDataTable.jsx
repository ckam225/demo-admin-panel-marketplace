import React from 'react'
import styled from 'styled-components'
import SearchBox from '../../../components/controls/SearchBox'
import useProducts, { useActionStatuses } from '../../../store/actions/products'
import ProductTableRow from './ProductTableRow'
import Modal from '../../../components/controls/Modal'
import Loader from '../../../components/controls/Loader'
import { endOt } from '../../../helpers'
import Paginator from '../../../components/controls/Paginator'
import TableEmptyRow from '../../../components/controls/TableEmptyRow'
import SelectBox from '../../../components/controls/SelectBox'
import { useCategoriesHasProps } from '../../../store/actions/categories'
import { useActiveStores } from '../../../store/actions/stores'
import { navigate } from '@reach/router'
import ProductInfo from './ProductInfo'

const ProductDataTable = ({ storeId = null, isModerationMode = false }) => {
    const [state, setState] = React.useState(isModerationMode ? 'checking' : 'all')
    const [search, setSearch] = React.useState('')
    const [category, setCategory] = React.useState()
    const [store, setStore] = React.useState(storeId)
    const { products, fetchProducts, currentProduct, fetchCurrentProduct, pageCount, loading } = useProducts()
    const { categories } = useCategoriesHasProps()
    const { stores } = useActiveStores()
    const { statuses } = useActionStatuses(storeId)
    const modal = React.useRef()
    const [loadingCurrent, setLoadingCurrent] = React.useState(false)
    const [currentPage, setCurrentPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)
    const [productsPaginated, setProductsPaginated] = React.useState([])


    const handleStatusChange = (e) => {
        setState(e.target.value)
    }

    const handleSearchChange = (s) => {
        setSearch(s)
    }

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    const query = React.useMemo(() => {
        let q = `?page=${currentPage}&per_page=${perPage}`
        if (store != null)
            q += `&store_id=${store}`
        if (search.length > 0)
            q = q + "&name=" + search
        if (state && state + "".toLowerCase() !== 'all')
            q = q + "&state=" + state
        if (category)
            q = q + "&category=" + category
        return q
    }, [store, state, search, category,currentPage, perPage])

    const handleItemClick = async (product) => {
        if(isModerationMode){
            modal.current.open()
            setLoadingCurrent(true)
            await fetchCurrentProduct(product?.id)
            setLoadingCurrent(false)
        }else{
            navigate(`/products/${product?.id}`)
        }
    }

    function handleNextClick() {
        setCurrentPage(prev => prev + 1)
    }

    React.useEffect(() => {
        fetchProducts(query)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    React.useEffect(() => {
        const s = [...productsPaginated ,...products]
        console.log(s);
        setProductsPaginated(s)
    }, [products])

    return <Wrapper>
        <div className="flex items-center py-4 border-b">
            {!isModerationMode &&
                <div className=" mx-3">
                    <select onChange={handleStatusChange} className="py-1 focus:ring-0 text-base" placeholder="" value={state}>
                        {statuses.length > 0 && statuses.map((status, s) => <option value={status.value} key={s}>{status?.name} ({status?.productCount})</option>)}
                    </select>
                </div>
            }
            <div className="w-80 mx-3">
                <SelectBox items={categories}
                    onChange={(cat) => setCategory(cat?.id)}
                    displayField="name"
                    className="rounded-md"
                    contentClass="rounded-md"
                    style={{ padding: '6px 6px' }} />
            </div>
            <div className="w-80 mx-3">
                <SelectBox items={stores?.data}
                    onChange={(store) => setStore(store?.id)}
                    displayField="name"
                    className="rounded-md"
                    contentClass="rounded-md"
                    style={{ padding: '6px 6px' }} />
            </div>
            <SearchBox onChange={handleSearchChange} placeholder="Поиск по названию" className="py-1 rounded-sm text-sm" />

            {/* {!loading && <div className=" mx-3">
                                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                                    <option value="10">10</option>
                                    <option value="30" >30</option>
                                    <option value="50" >50</option>
                                    <option value="100" >100</option>
                                </select>
                            </div>} */}
        </div>
        <table className="table px-2 select-none">
            <thead>
                <tr>
                    <th>Ид</th>
                    <th>Фото</th>
                    <th>Код</th>
                    <th>Артикул</th>
                    <th>наименование</th>
                    <th>Категории</th>
                    <th>Бренд</th>
                    <th>Цена</th>
                    <th>На складе</th>
                </tr>
            </thead>
            <tbody>
                {(loading && pageCount <= 1) ? <TableEmptyRow colspan={9} className="flex justify-center  py-5">
                    <Loader />
                </TableEmptyRow> : products.length > 0 ?
                    productsPaginated.map(product => <ProductTableRow product={product} key={product.id} onSelected={handleItemClick}
                        className={[currentProduct?.id === product?.id ? 'bg-gray-100' : ''].join(' ')}
                    />) : <TableEmptyRow colspan={9} className="text-sm text-center py-5">Нет товаров</TableEmptyRow>}
            </tbody>
        </table>

        {!loading && <div className="py-2 flex justify-center items-center bg-gray-50">
            {(products.length > 0 && pageCount > 1)  &&  <div>
                {loading ? <span>Loading...</span> : 
                    <button className="button" onClick={handleNextClick}>Загрузить еще</button>}
            </div>}
        </div>}
        
       

        <Modal refId={modal} style={{ width: '80%' }}>
            <Modal.Header>
                <div className="flex justify-between">
                    <span>{endOt(currentProduct?.name, 60)}</span>
                    <div className="text-sm flex items-center pr-6">
                        <span className="mr-2">Статус:</span>
                        <b>{currentProduct?.status?.title}</b>
                    </div>
                </div>
            </Modal.Header>
            <Modal.Body>
                {/* <div className="flex  overflow-hidden overflow-y-auto" style={{ height: '100%' }}> */}
                    {loadingCurrent ? <Loader /> : 
                    // <ProductDetail product={currentProduct} isModerationMode={isModerationMode}/>
                    <ProductInfo productId={currentProduct?.id} isModerationMode={isModerationMode}/>
                    }
                {/* </div> */}
               
            </Modal.Body>
        </Modal>
    </Wrapper>
}

const Wrapper = styled.div`
table{
    width: 100%;
    user-select: none;
}
table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`

export default ProductDataTable