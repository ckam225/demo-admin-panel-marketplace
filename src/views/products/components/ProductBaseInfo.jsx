import React from 'react';
import { toast } from 'react-toastify';
import EditableLabel from '../../../components/controls/EditableLabel';
import AreaLabel from '../../../components/controls/AreaLabel';
import CategoryPicker from '../../../components/shared/CategoryPicker';
import BrandPicker from '../../../components/shared/BrandPicker';
import styled from 'styled-components';
import * as productApi from '../../../api/products'
import { findCategoryById } from '../../../api/categories'
import { findBrandById } from '../../../api/brands'
import PreviousValue from "./PreviousValue";
import ProductFieldNote from "./ProductFieldNote";

const ProductBaseInfo = ({ product, changes = [], isModerate = false }) => {

    const [newCategory, setNewCategory] = React.useState()
    const [newBrand, setNewBrand] = React.useState()

    const categoryPicker = React.useRef()
    const brandPicker = React.useRef()

    const category = React.useMemo(() => {
        if (newCategory)
            return newCategory
        return product?.category
    }, [product, newCategory])

    const brand = React.useMemo(() => {
        if (newBrand)
            return newBrand
        return product?.brand
    }, [product, newBrand])

    const canEdit = React.useMemo(() => {
        return product?.imported !== 'yml' && isModerate
    }, [product, isModerate])

    const previousChanges = React.useMemo(() => {
        if (typeof changes === 'object' && changes.length > 1) {
            return changes[1].payload
        }
    }, [changes])

    const [previousCategory, setPreviousCategory] = React.useState()
    const [previousBrand, setPreviousBrand] = React.useState()

    const notes = React.useMemo(() => product?.notes || {}, [product])

    React.useEffect(() => {
        if (previousChanges) {
            findCategoryById(previousChanges.category_id).then((category) => setPreviousCategory(category.data))
            findBrandById(previousChanges.brand_id).then((brand) => setPreviousBrand(brand.data))
        }
    }, [previousChanges])

    async function updateProduct(field, payload) {
        const res = await productApi.updateProduct(product?.id, payload)
        if (res.error) {
            toast.error(`Ошибка при обновлении ${field}`)
        } else {
            toast.success(`${field} упешно обновлен(а) `)
        }
    }

    const handleInputChange = (e) => {
        updateProduct(e.name, { [e.name]: e.value })
    }

    const handleCategoryChange = (e) => {
        setNewCategory(e)
        updateProduct('Категории', { category_id: e?.id })
        categoryPicker.current.close()
    }

    const handleBrandChange = (e) => {
        setNewBrand(e)
        updateProduct('Бренд', { brand_id: e?.id })
        brandPicker.current.close()
    }

    const handleNoteChange = (key, value) => {
        updateProduct('notes', { notes: Object.assign(notes, { [key]: value }) })
    }

    const handleNoteRemove = (key) => {
        updateProduct('notes', { notes: Object.assign(notes, { [key]: '' }) })
    }

    return <Wp>
        <div className="text-2xl break-words">
            <EditableLabel name="name" value={product?.name || ''} onChange={handleInputChange} editable={canEdit} />
        </div>
        <div className="text-xs mt-3 text-justify bg-indigo-50 p-2" >
            <AreaLabel rows="5" name="description" value={product?.description || ''} onChange={handleInputChange} editable={canEdit} />
        </div>
        <table className="table t-info">
            <tbody>
                <tr>
                    <td>Ид</td>
                    <td className="input" >
                        {product?.id || '-'}
                    </td>
                </tr>
                <tr>
                    <td>Код</td>
                    <td className="input" >
                        {product?.code_1c || '-'}
                    </td>
                </tr>
                <tr>
                    <td>Артикул</td>
                    <td className="input">
                        <EditableLabel name="slug" value={product?.slug || ''} onChange={handleInputChange} editable={canEdit} />
                        <PreviousValue value={previousChanges?.slug} />
                        <ProductFieldNote note={notes?.slug} field="slug" onChange={handleNoteChange} onRemove={handleNoteRemove} />
                    </td>
                </tr>
                <tr>
                    <td>штрих-код</td>
                    <td className="input">
                        <EditableLabel name="barcode" value={product?.barcode || ''} onChange={handleInputChange} editable={canEdit} />
                        <PreviousValue value={previousChanges?.barcode} />
                        <ProductFieldNote note={notes?.barcode} field="barcode" onChange={handleNoteChange} onRemove={handleNoteRemove} />
                    </td>
                </tr>
                <tr>
                    <td>Категории</td>
                    <td className="input">
                        <div style={{ width: '200px' }} onClick={() => {
                            if (canEdit)
                                categoryPicker.current.open()
                        }}>
                            {category?.name || '-'}
                        </div>
                        <PreviousValue value={previousCategory?.name} />
                        <ProductFieldNote note={notes?.category} field="category" onChange={handleNoteChange} onRemove={handleNoteRemove} />
                    </td>
                </tr>

                <tr>
                    <td>Бренд</td>
                    <td className="input">
                        <div style={{ width: '200px' }} onClick={() => {
                            if (canEdit)
                                brandPicker.current.open()
                        }}>
                            {brand?.name || '-'}
                        </div>
                        <PreviousValue value={previousBrand?.name} />
                        <ProductFieldNote note={notes?.brand} field="brand" onChange={handleNoteChange} onRemove={handleNoteRemove} />
                    </td>
                </tr>
                <tr>
                    <td>Цена</td>
                    <td className="input">
                        <EditableLabel type="number" name="price" value={product?.price || 0} onChange={handleInputChange} min="0" step="0.01" editable={false} />
                        <PreviousValue value={previousChanges?.price} />
                        <ProductFieldNote note={notes?.price} field="price" onChange={handleNoteChange} onRemove={handleNoteRemove} />
                    </td>
                </tr>
                <tr>
                    <td>На складе</td>
                    <td className="input">
                        <EditableLabel type="number" name="quantity" value={product?.quantity || 0} onChange={handleInputChange} min="0" step="1" editable={false} />
                        <PreviousValue value={previousChanges?.quantity} />
                        <ProductFieldNote note={notes?.quantity} field="quantity" onChange={handleNoteChange} onRemove={handleNoteRemove} />
                    </td>
                </tr>
                <tr>
                    <td>НДС</td>
                    <td className="input">
                        <EditableLabel type="number" name="vat" value={product?.vat || 0} onChange={handleInputChange} min="0" step="0.01" editable={false} />
                        <PreviousValue value={previousChanges?.vat} />
                        <ProductFieldNote note={notes?.vat} field="vat" onChange={handleNoteChange} onRemove={handleNoteRemove} />
                    </td>
                </tr>
                <tr>
                    <td>Архив</td>
                    <td className="input" >
                        {product?.archived ? 'да' : 'нет'}
                    </td>
                </tr>
                <tr>
                    <td>Скрытый</td>
                    <td className="input" >
                        {product?.isvisible ? 'нет' : 'да'}
                    </td>
                </tr>
                <tr>
                    <td>Из yaml-файла</td>
                    <td className="input" >
                        {product?.imported === 'yml' ? 'из yaml-файла' : '-'}
                    </td>
                </tr>
            </tbody>
        </table>
        {isModerate && <>
            <CategoryPicker refKey={categoryPicker} onSelected={handleCategoryChange} />
            <BrandPicker refKey={brandPicker} onSelected={handleBrandChange} zIndex={22} />
        </>}
    </Wp>
}

const Wp = styled.div`
.t-info td {
    height: 55px;
    /* font-size: 14px; */
}
.t-info .input{
    font-size: 14px;
    /* font-weight: bold */
}
`

export default ProductBaseInfo;