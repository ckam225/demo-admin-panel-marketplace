import React from 'react';

const PreviousValue = ({ value }) => {
    return <div className={value !== undefined ? 'visible' : 'hidden'}>
        <b className="line-through bg-yellow-100">{value}</b>
    </div>
}

export default PreviousValue;