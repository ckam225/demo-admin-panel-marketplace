import React from 'react';

const ProductFieldNote = ({ note, field, onChange, onRemove }) => {

    const [isEditable, setIsEditable] = React.useState(false)
    const [value, setValue] = React.useState()

    const handleToggleNote = () => {
        setIsEditable(!isEditable)
    }

    const handleSave = () => {
        setIsEditable(false)
        onChange(field, value)
    }

    const handleRemove = () => {
        setValue('')
        setIsEditable(false)
        onRemove(field)
    }

    const handleChange = (e) => {
        setValue(e.target.value)
    }

    React.useEffect(() => setValue(note), [note])

    return <div className="mt-2">
        <div className={[value && !isEditable ? 'visible' : 'hidden'].join(' ')}>
            <span className="font-bold">Замечание:</span> {value}
        </div>
        <button onClick={handleToggleNote} className={[!isEditable ? 'visible' : 'hidden', 'underline'].join(' ')}>Редактировать замечание</button>
        <div className={[isEditable ? 'visible' : 'hidden' ].join(' ')}>
            <textarea value={value} onChange={handleChange} />
            <button onClick={handleSave} className="underline p-2">Сохранить</button>
            <button onClick={handleRemove} className="underline p-2">Удалить</button>
        </div>
    </div>
}

export default ProductFieldNote;