import React from 'react'
import LightBox from '../../../components/controls/LightBox'



const ImageCaroussel = ({ images = [] }) => {

    const [image, setImage] = React.useState()
    const lightbox = React.useRef()
    // const imagesList = React.useMemo(() => {
    //     return images.map(img => img?.url)
    // }, [images])

    // const [state, setState] = React.useState({
    //     photoIndex: 0,
    //     isOpen: false,
    // })
    // const { photoIndex, isOpen } = state;

    function handleClick() {
        lightbox.current.open()
    }

    React.useEffect(() => {
       images.forEach(im => {
        if (im.is_main) {
            setImage(im)
        }
       })
    }, [images])

    return <div className="flex h-full">
        <div className="flex flex-col flex-none justify-center">
            {images.length > 0 && images.map((im, i) => <div key={i} className={[
                "w-12 h-12 border m-2 p-1 hover:border-blue-300 cursor-pointer bg-white ",
                image?.url === im.url ? 'border-blue-300 opacity-100' : 'opacity-40'
            ].join(" ")} onClick={() => setImage(im)}>
                <img className="w-full h-full object-cover"
                    src={im?.url}
                    key={i}
                    alt={im?.tabindex} />
            </div>)}
        </div>
        <div className="flex-auto flex items-center justify-center p-5">
            <button style={{ width: '300px', height: '300px' }} onClick={handleClick}>
                {image && <img className="w-full h-full object-contain zoom"
                    src={image?.url}
                    alt={image.tabindex || 0} />}
            </button>
        </div>

        {/* {isOpen && <Lightbox
                        mainSrc={images[photoIndex]}
                        nextSrc={images[(photoIndex + 1) % images.length]}
                        prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                        onCloseRequest={() => setState({ isOpen: false })}
                        onMovePrevRequest={() =>
                        setState({
                            photoIndex: (photoIndex + images.length - 1) % images.length,
                        })
                        }
                        onMoveNextRequest={() =>
                        setState({
                            photoIndex: (photoIndex + 1) % images.length,
                        })
                        }
                />} */}

        <LightBox refId={lightbox} url={image?.url} />
    </div>
}

export default ImageCaroussel
