import React from 'react'
import ImageCaroussel from './ImageCaroussel'
import ProductBaseInfo from "./ProductBaseInfo";
import Tab from '../../../components/controls/Tab'

function formatPropValue(prop) {
    if (prop.type === 'multiple' && Array.isArray(prop?.value)) {
        return prop.value.join(', ')
    }
    return prop.value
}

const ProductDetail = ({ product, changes = [], showOption = false, isModerationMode = false }) => {

    const properties = React.useMemo(() => {
        const output = []
        for (let p in product?.properties) {
            output.push({
                name: p,
                props: product?.properties[p]
            })
        }
        return output
    }, [product])


    return <div className=" w-full select-none">
        <div className="flex">
            <div style={{ width: '500px' }}>
                <ImageCaroussel images={product?.images} />
            </div>
            <div className="p-3 flex-auto" >
                <ProductBaseInfo product={product} changes={changes} isModerate={isModerationMode} />
            </div>
        </div>

        <Tab>
            <Tab.Headers className="flex sticky inset-x-0 mx-5 top-0 left-0 bg-white">
                <Tab.Header tab={0}>Характеристики</Tab.Header>
            </Tab.Headers>
            <Tab.Content className="mx-5">
                <Tab.Item tab={0}>
                    <div className="py-3 " style={{ columnCount: 1 }}>
                        <div className="flex flex-col mb-8">
                            <div className="text-left text-2xl">Вес и габариты товара</div>
                            <div className="grid grid-cols-2 item-list-o">
                                <div>Вес товара</div>
                                <div>{product?.weight} г</div>
                            </div>
                            <div className="grid grid-cols-2 item-list-o">
                                <div>Длина товара</div>
                                <div>{product?.length} мм</div>
                            </div>
                            <div className="grid grid-cols-2 item-list-o">
                                <div>Ширина товара</div>
                                <div>{product?.width} мм</div>
                            </div>
                            <div className="grid grid-cols-2 item-list-o">
                                <div>высота товара</div>
                                <div>{product?.height} мм</div>
                            </div>
                        </div>

                        {properties.length > 0 && properties.map((group, gp) => <div className="flex flex-col mb-8" key={gp}>
                            <div className="text-left text-2xl">{group?.name}</div>
                            {group?.props.map((prop, p) => <div key={p} className="grid grid-cols-2 item-list-o">
                                <div>{prop?.name}</div>
                                <div>{formatPropValue(prop) || '-'}</div>
                            </div>)}
                        </div>)}
                    </div>
                </Tab.Item>
            </Tab.Content>
        </Tab>

    </div>
}

export default ProductDetail