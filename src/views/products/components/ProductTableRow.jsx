import React from 'react'
import Image from '../../../components/controls/Image'
import { endOt } from '../../../helpers'

const ProductTableRow = ({ product, onSelected, className, isFullPage = false }) => {

    const handleClick = () => {
        onSelected(product)
    }

    const logo = React.useMemo(() => {
        return product?.images?.find(el => el?.is_main)?.url;
    }, [product]);

    return <tr onClick={handleClick} className={className}>
        <td>#{product?.id}</td>
        <td>
            <Image src={logo} alt={product?.slug} width="16" height="16" />
        </td>
        <td>{product?.code_1c}</td>
        <td>{product?.slug}</td>
        <td>{endOt(product?.name, 30)}</td>
        <td>{endOt(product?.category?.name, 30)}</td>
        <td>{product?.brand?.name}</td>
        <td>{product?.price}</td>
        <td>{product?.quantity}</td>
    </tr>
}

export default ProductTableRow