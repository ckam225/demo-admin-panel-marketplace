import React from 'react';
import MainLayout from '../../components/layouts/Main';
import { BaseLink } from '../../components/shared/NavItem';
import Icon from '../../components/controls/Icon';
import useProducts from '../../store/actions/products';
import ProductDetail from './components/ProductDetail';
import Confirm from "../../components/controls/Confirm";
import ProductInfo from './components/ProductInfo';

const DetailProduct = ({ productId, isModerationMode = false }) => {

    return <>
        <MainLayout>
            <MainLayout.Head>

            </MainLayout.Head>
            <MainLayout.Container>
                <ProductInfo productId={productId} isModerationMode={isModerationMode}/>
            </MainLayout.Container>
        </MainLayout>
    </>
}

export default DetailProduct;