import React from 'react';
import MainLayout from '../../components/layouts/Main';
import ProductDataTable from './components/ProductDataTable';

const ListProducts = ({ isModerationMode=false }) => {
    return <MainLayout>
    <MainLayout.Head>
        <title>Список товаров</title>
    </MainLayout.Head>
    <MainLayout.Container >
        <div className="m-3 bg-white">
            <div className="flex justify-between px-3  py-2 border-b">
                <div className="text-xl">Список товаров</div>
            </div>
          <ProductDataTable isFullPage={true} isModerationMode={isModerationMode}/>
        </div>
    </MainLayout.Container>
    </MainLayout>
}
 
export default ListProducts;