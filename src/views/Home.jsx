import React from 'react'
import MainLayout from '../components/layouts/Main'
import styled from 'styled-components'
import Stats from './dashboard/Stats'
import RecentOrders from './dashboard/RecentOrders'
import ActiveStores from './dashboard/ActiveStores'
import { BaseLink } from '../components/shared/NavItem'


const Home = () => {
    return <MainLayout>
        <MainLayout.Head>
            <title>Дашбоар</title>
        </MainLayout.Head>
        <MainLayout.Container>
            <Wrapper>
                <div className="m-10">

                    <div className="">
                        <h1 className="font-bold my-3 text-gray-900">Обзор</h1>
                        <Stats />
                    </div>
                    <div className="flex">
                        <div className="flex-auto">
                            <div className="flex justify-between items-center">
                                <h1 className="font-bold my-3 text-gray-900">Активные партнеры</h1>
                                <BaseLink to="/stores" className="text-blue-400 text-sm mr-4">Все</BaseLink>
                            </div>
                            <ActiveStores />
                        </div>

                        <div className="ml-5 " style={{ width: '350px' }}>
                            <h1 className="font-bold my-3 text-gray-900">Последние заказы</h1>
                            <RecentOrders />
                            <h1 className="font-bold my-3 text-gray-900">Популярные товары</h1>
                            {/* <PopularProduct /> */}
                        </div>
                    </div>
                </div>
            </Wrapper>
        </MainLayout.Container>
    </MainLayout>
}

const Wrapper = styled.div`

`
export default Home