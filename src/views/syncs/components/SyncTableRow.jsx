import React from "react";
import StatusButton from "../../../components/controls/StatusButton";
import { BaseLink } from "../../../components/shared/NavItem"
import { DateTime } from 'luxon'

const SyncTableRow = ({ sync }) => {

    // const { isSuperuser } = useRecoilValue(userRoleState)
    // const hasError = sync.status === 'warning' || sync.status === 'error'

    return <tr>
        <td>
            #<BaseLink className="link__sync" to={`/synchronization/${sync.id}`}>{sync.id}</BaseLink>
        </td>
        <td title={sync.status}>
            <StatusButton status={sync.status} iconOnly={true} iconSize={24} />
        </td>
        <td>{sync.source}</td>
        <td>{DateTime.fromISO(sync.created_at).setLocale('ru').toFormat('dd LLL yyyy hh:mm:ss')}</td>
        <td>{DateTime.fromISO(sync.updated_at).setLocale('ru').toRelative()}</td>
        <td>
            {/* {(hasError && isSuperuser) && <button className="border rounded-full p-1 focus:outline-none" title="Перенести в очереди">
                <Icon name="refresh" size={18} className="text-blue-400" />
            </button>} */}
        </td>
    </tr >
}

export default SyncTableRow;