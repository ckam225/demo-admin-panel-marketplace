import React from "react";
import { useRecoilValue } from "recoil";
import Confirm from "../../../components/controls/Confirm";
import Icon from "../../../components/controls/Icon";
import Modal from "../../../components/controls/Modal";
import { userRoleState } from '../../../store/states/base'


const SyncEditModal = ({ refKey, sync, onUpdate }) => {

    const modal = React.useRef(null);
    const [payload, setPayload] = React.useState('')
    const [editable, setEditable] = React.useState(false)
    const { isSuperuser } = useRecoilValue(userRoleState)

    const confirmBox = React.useRef(null)

    React.useEffect(() => {
        setPayload(JSON.stringify(sync.payload, undefined, 3))
        setEditable(sync.status === 'error')
    }, [sync])

    React.useImperativeHandle(refKey, () => ({
        show() {
            modal.current.open();
        },
        close() {
            modal.current.close();
        },
    }));

    const handleRetry = () => {
        confirmBox.current.open();
    }

    const handleUpdate = async () => {
        await onUpdate(payload)
        setEditable(!editable)
        modal.current.close()
    }

    return <>
        <Modal refId={modal} style={{ width: '800px' }}>
            <Modal.Header>
                <div className="flex ">
                    <span>Sync <span className="text-gray-500">{`#${sync?.id}`}</span></span>
                    {isSuperuser && <button className="icon-button border-0 ml-3 text-gray-500 hover:text-blue-400" onClick={() => setEditable(!editable)}>
                        <Icon name={editable ? 'x' : 'pencil'} />
                    </button>}
                </div>
            </Modal.Header>
            <Modal.Body>
                {editable ?
                    <div className="flex flex-col justify-between h-full">
                        {isSuperuser && <textarea rows="20" className="w-full focus:outline-none border-2"
                            value={payload} onChange={(e) => setPayload(e.target.value)}>
                        </textarea>}
                        <div className="flex justify-between items-center  p-5">
                            <p className="text-sm ml-2">
                                <b>Внимание: </b>
                                <span className="text-red-500">изменение данных таким образом может привести к ошибкам.</span>
                            </p>
                            <button
                                className="button p-3"
                                onClick={handleRetry}>Сохранить и отправить</button>
                        </div>
                    </div> :
                    <div style={{ height: '100%' }} className="overflow-x-auto overflow-y-auto">
                        <code className="h-full"> {JSON.stringify(sync)}</code>
                    </div>}
            </Modal.Body>

        </Modal >
        <Confirm refId={confirmBox} actions={[
            {
                label: 'отменить',
                click: () => { }
            }, {
                label: 'продолжать',
                className: 'text-red-500',
                click: handleUpdate
            }
        ]}>
            <div className="text-center font-bold pt-8">
                Уверены ли вы?
            </div>
            <div className="text-center p-4">
                Вы действительно хотите перенести в очереди это? <em className="text-red-600">это действие необратимо.</em>
            </div>
        </Confirm>
    </>
}

export default SyncEditModal;