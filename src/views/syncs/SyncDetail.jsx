import React from "react";
import { useRecoilValue } from "recoil";
import styled from 'styled-components'
import Confirm from "../../components/controls/Confirm";
import Icon from "../../components/controls/Icon";
import Loader from "../../components/controls/Loader";
import StatusButton from "../../components/controls/StatusButton";
import MainLayout from "../../components/layouts/Main";
import { BaseLink } from "../../components/shared/NavItem";
import useSync from "../../store/actions/sync";
import { userRoleState } from "../../store/states/base";
import SyncEditModal from "./components/SyncEditModal";
import { DateTime } from 'luxon'
import axios from 'axios'
import Accordion from "../../components/controls/Accordion";


const SyncDetail = ({ syncId, navigate }) => {
    const { sync, getSync, loading, updateSync, deleteSync } = useSync()
    const modal = React.useRef(null);
    const confirmBox = React.useRef()
    const { isSuperuser } = useRecoilValue(userRoleState)
    const [confirmData, setConfirmData] = React.useState({
        title: '',
        message: '',
        action: () => { }
    })
    const errorTheme = sync?.status === 'error' ? 'bg-red-200 text-red-700' : 'bg-yellow-200 text-yellow-700'
    const [markd, setMarkd] = React.useState('')
    const [loadingPayload, setLoadingPayload] = React.useState(false)


    React.useEffect(() => {
        loadSync()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [syncId])

    React.useEffect(() => {
        loadSyncPayload()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [sync])

    function loadSync() {
        setTimeout(async () => {
            await getSync(syncId)
        }, 0);
    }


    function loadSyncPayload() {


        setLoadingPayload(true)
        if (sync?.payload && sync?.payload?.url) {
            setTimeout(async () => {
                axios.get(sync?.payload?.url).then(res => {
                    setMarkd(JSON.stringify(res.data, undefined, 2))
                    setLoadingPayload(false)
                }).catch(err => {
                    console.error(err.response);
                    setLoadingPayload(false)
                })
            }, 0);
        } else {
            setTimeout(() => {
                setMarkd(JSON.stringify(sync?.payload, undefined, 2))
                setLoadingPayload(false)
            }, 0);
        }
    }


    function handleUpdate(payload) {
        setTimeout(async () => {
            await updateSync(syncId, payload)
        }, 0);
    }

    const handleDelete = () => {
        setConfirmData({
            title: "Уверены ли вы?",
            message: 'Вы действительно хотите Удалить это? Удаление данных, это действие необратимо',
            action: async () => {
                await deleteSync(syncId)
                navigate('/synchronization')
            }
        })
        confirmBox.current.open()
    }

    const handleRefresh = () => {
        setConfirmData({
            title: "Уверены ли вы?",
            message: 'Вы действительно хотите перенести в очереди это?',
            action: async () => {
                await updateSync(syncId, JSON.stringify(sync?.payload))
            }
        })
        confirmBox.current.open()
    }

    return <>

        <MainLayout>
            <MainLayout.Head>
                <title>Sync #{sync?.id}</title>
            </MainLayout.Head>
            <MainLayout.Container>
                <Wrapper>

                    {loading ? <div className="card m-2 flex p-16 justify-center"><Loader /></div> :
                        <div className="card m-2">
                            <div className="flex bg-white items-center justify-between px-2 py-5">
                                <div className="flex">
                                    <BaseLink to="/synchronization" className="icon-button border-0 mr-3">
                                        <Icon name="arrow_left" />
                                    </BaseLink>
                                    <StatusButton status={sync.status} />
                                    <span className="px-2">Sync #<b>{sync.id}</b></span>
                                    <span className="pl-2">Источник: <b className="text-gray-500">{sync.source}</b></span>
                                    <span className="pl-2 text-gray-400">{DateTime.fromISO(sync.created_at).setLocale('ru').toLocaleString(DateTime.DATETIME_FULL)}</span>
                                </div>
                                <div className="flex">
                                    {sync?.source === 'backend' && <button title="Payload" className="icon-button mr-1 text-gray-500  hover:text-blue-400"
                                        onClick={() => modal.current.show()}>
                                        <Icon name="code" />
                                    </button>}

                                    {sync?.source === '1c' && <a href={sync?.payload?.url} title="Скачать" className="icon-button mr-1 text-gray-500  hover:text-blue-400">
                                        <Icon name="download" />
                                    </a>}
                                    {(sync?.source === 'backend' && isSuperuser && sync?.status !== 'success') &&
                                        <button title="перенести в очереди"
                                            className="icon-button mr-1 text-gray-500 hover:text-blue-400" onClick={handleRefresh}>
                                            <Icon name="refresh" />
                                        </button>}
                                    {isSuperuser && <button title="Удалить" className="icon-button mr-1 text-gray-500 hover:text-red-400" onClick={handleDelete}>
                                        <Icon name="trash" />
                                    </button>}
                                </div>
                            </div>
                            <div >
                                {sync?.errors?.length > 0 && <div className={`p-5 ${errorTheme}`}>
                                    {sync?.errors.map((err, e) => <li key={e}>{err}</li>)}
                                </div>}
                                <div className="markdown">
                                    <Accordion>
                                        <Accordion.Header className="p-3 bg-gray-400  border-b border-blue-accent flex justify-between">
                                            <span>Payload</span>
                                            {loadingPayload ? <Loader /> : <Icon name="chevron_down" />}
                                        </Accordion.Header>
                                        <Accordion.Content>
                                            {loadingPayload ? <Loader /> : <div className="bg-gray-200 text-blue-fonce">
                                                <pre className="p-5" >{markd}</pre>
                                            </div>}
                                        </Accordion.Content>
                                    </Accordion>

                                    <Accordion>
                                        <Accordion.Header className="p-3 bg-gray-400  border-b border-blue-accent flex justify-between">
                                            <span>Response</span>
                                            {loadingPayload ? <Loader /> : <Icon name="chevron_down" />}
                                        </Accordion.Header>
                                        <Accordion.Content>
                                            {!loading && sync.response &&
                                                <pre className="p-5" >{JSON.stringify(sync.response, undefined, 3)}</pre>}
                                        </Accordion.Content>
                                    </Accordion>

                                    <Accordion>
                                        <Accordion.Header className="p-3 bg-gray-400 flex justify-between">
                                            <span>Exception</span>
                                            {loadingPayload ? <Loader /> : <Icon name="chevron_down" />}
                                        </Accordion.Header>
                                        <Accordion.Content>
                                            {!loading && sync.exceptions ?
                                                <pre className="p-5" >{JSON.stringify(sync.exceptions, undefined, 3)} </pre> :
                                                <div className="text-center p-5">No data</div>}
                                        </Accordion.Content>
                                    </Accordion>


                                </div>
                            </div>
                            {sync && <div className="flex flex-col items-end px-1 py-3">
                                <div className="text-sm"><em>Дата создания:  </em><b>{DateTime.fromISO(sync.created_at).setLocale('ru').toFormat('dd LLL yyyy hh:mm:ss')} </b></div>
                                <div className="text-sm"><em>Обновлено:  </em><b>{DateTime.fromISO(sync.updated_at).setLocale('ru').toRelative()}</b></div>
                            </div>}
                        </div>}
                </Wrapper>

            </MainLayout.Container>
        </MainLayout>
        <SyncEditModal refKey={modal} sync={sync} onUpdate={handleUpdate} />
        <Confirm refId={confirmBox} actions={[
            {
                label: 'отменить',
                click: () => { }
            }, {
                label: 'продолжать',
                className: 'text-red-500',
                click: confirmData.action
            }
        ]}>
            <div className="text-center font-bold pt-8">
                {confirmData.title}
            </div>
            <div className="text-center p-4">
                {confirmData.message}
            </div>
        </Confirm>
    </>
}

const Wrapper = styled.div`
.markdown{
    min-height: 50vh;
    /* background: #000;
    color: #fff; */
}
`

export default SyncDetail;