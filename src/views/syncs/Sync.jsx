import React from 'react'
import MainLayout from '../../components/layouts/Main'
import styled from 'styled-components'
import useSync from '../../store/actions/sync'
import SyncTableRow from './components/SyncTableRow'
import TableEmptyRow from '../../components/controls/TableEmptyRow'
import Loader from '../../components/controls/Loader'
import { DateTime } from 'luxon'
import { DatePicker } from '../../components/controls/DatePicker'
import Paginator from '../../components/controls/Paginator'



function getStatusValue(tab) {
    switch (tab) {
        case 1:
            return 'pending'
        case 2:
            return 'started'
        case 3:
            return 'success'
        case 4:
            return 'warning'
        case 5:
            return 'error'
        default:
            return null
    }
}

const Syncs = () => {
    const [tab, setTab] = React.useState(0)
    const { loading, syncs, fetchSyncs, pageCount } = useSync()
    const [startDate, setStartDate] = React.useState();
    const [endDate, setEndDate] = React.useState();
    const [source, setSource] = React.useState();
    const [currentPage, setCurrentPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)
    const [syncsPaginated, setSyncsPaginated] = React.useState([])


    const query = React.useMemo(() => {
        let q = `?page=${currentPage}&per_page=${perPage}&sort=created_at&order=desc`
        const state = getStatusValue(tab)
        q += state ? `&status=${state}` : ''
        if (source) {
            q += `&source=${source}`
        }
        if (startDate) {
            q += `&start_date=${DateTime.fromJSDate(startDate).toFormat('yyyy-MM-dd')}`
        }
        if (endDate) {
            q += `&end_date=${DateTime.fromJSDate(endDate).toFormat('yyyy-MM-dd')}`
        }
        return q
    }, [tab, startDate, endDate, source, currentPage, perPage])

    const handleTabClick = (selectedTab) => {
        setTab(selectedTab)
    }

    const handleStatusChange = ({ target }) => {
        if (target.value !== 'all')
            setSource(target.value)
        else setSource()
    }

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    function handleNextClick() {
        setCurrentPage(prev => prev + 1)
    }

    React.useEffect(() => {
        fetchSyncs(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    React.useEffect(() => {
        const s = [...syncsPaginated ,...syncs]
        setSyncsPaginated(s)
    }, [syncs])
 

    return <MainLayout>
        <MainLayout.Head>
            <title>Синхронизация</title>
        </MainLayout.Head>
        <MainLayout.Container>
            <Wrapper>

                <div className="card m-3">
                    <div className="sticky inset-x-0 top-0 left-0 tab__header  justify-between bg-white">
                        <div className="flex">
                            <div className={`tab_header_item ${tab === 0 ? 'active' : ''}`} onClick={() => handleTabClick(0)}>Все</div>
                            <div className={`tab_header_item ${tab === 1 ? 'active' : ''}`} onClick={() => handleTabClick(1)}>В очереди</div>
                            <div className={`tab_header_item ${tab === 2 ? 'active' : ''}`} onClick={() => handleTabClick(2)}>Запущены</div>
                            <div className={`tab_header_item ${tab === 3 ? 'active' : ''}`} onClick={() => handleTabClick(3)}>Успех</div>
                            <div className={`tab_header_item ${tab === 4 ? 'active' : ''}`} onClick={() => handleTabClick(4)}>С ошибками</div>
                            <div className={`tab_header_item ${tab === 5 ? 'active' : ''}`} onClick={() => handleTabClick(5)}>Ошибка</div>
                        </div>
                    </div>
                    <div className="content px-3">
                        <div className="flex justify-between">
                            <div className="flex items-center border-b pr-1 py-2">
                                <select onChange={handleStatusChange} className="py-1 mr-2 rounded-sm focus:ring-0 text-base w-36" placeholder="">
                                    <option value="all">все</option>
                                    <option value="1c">1С</option>
                                    <option value="backend">бэкенда</option>
                                </select>
                                <div className=" mr-2">
                                    <DatePicker onChange={setStartDate} placeholder="с" />
                                </div>
                                <div className=" mr-2">
                                    <DatePicker onChange={setEndDate} placeholder="до" />
                                </div>
                            </div>
                            <div className=" mx-3">
                                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                                    <option value="10">10</option>
                                    <option value="30" selected>30</option>
                                    <option value="50" selected>50</option>
                                    <option value="100" selected>100</option>
                                </select>
                            </div>
                        </div>
                        <table className="table no-hover">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Статус</th>
                                    <th scope="col">Источник</th>
                                    <th scope="col">Создано</th>
                                    <th scope="col">Обновлено</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {(loading && pageCount <= 1) ? <TableEmptyRow colspan={8} className="flex justify-center  py-5">
                                    <Loader />
                                </TableEmptyRow> :
                                    syncs.length === 0 ? <TableEmptyRow colspan={7} className="text-center py-5">Нет данных</TableEmptyRow> :
                                    syncsPaginated.map(s => <SyncTableRow key={s.id} sync={s} />)
                                }
                            </tbody>
                        </table>
                        {!loading && <div className="py-2 flex justify-center items-center bg-gray-50">
                            {(syncs.length > 0 && pageCount > 1)  &&  <div>
                                {loading ? <span>Loading...</span> : 
                                 <button className="button" onClick={handleNextClick}>Загрузить еще</button>}
                            </div>}
                        </div>}
                    </div>
                </div>
            </Wrapper>

        </MainLayout.Container>
    </MainLayout>
}

const Wrapper = styled.div`
.content{
  min-height: 50vh;
  background: #fff;
}
.tab__header{
  background: #fff;
  display: flex;
  border-bottom: 1px solid #ccc;
}
.tab_header_item{
    font-size: 14px;
    font-style: normal;
    font-weight: 300;
    padding: 15px 10px;
    border-bottom: 2px solid transparent;
}
.tab_header_item:hover{
    color: var(--color-accent);
    cursor: pointer;
}
.tab_header_item.active{
    color: var(--color-accent);
    border-color: var(--color-accent);
}
.link__sync::hover{
    color: var(--color-accent);
    text-decoration: underline;
}
`

export default Syncs