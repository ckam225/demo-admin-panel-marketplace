import React from 'react';
import Tab from '../../components/controls/Tab'
import MainLayout from '../../components/layouts/Main'
import BrandDataTable from './components/brands/BrandDataTable';
import CategoryDataTable from './components/catgories/CategoryDataTable';
import PropertyDataTable from './components/properties/PropertyDataTable';
import TariffDataTable from './components/tariffs/TariffDataTable';

const Others = () => {
    return <MainLayout>
        <MainLayout.Head>

        </MainLayout.Head>
        <MainLayout.Container>
            <div className="py-5 px-2">
                <Tab>
                    <Tab.Headers className="flex sticky inset-x-0 mx-5 top-0 left-0 bg-white">
                        <Tab.Header tab={0}>Бренды</Tab.Header>
                        <Tab.Header tab={1}>Категории</Tab.Header>
                        <Tab.Header tab={2}>Своиства</Tab.Header>
                        <Tab.Header tab={3}>Тариффы</Tab.Header>
                    </Tab.Headers>
                    <Tab.Content className="my-2 mx-5 bg-white">
                        <Tab.Item tab={0}>
                            <div className="px-2">
                                <BrandDataTable />
                            </div>
                        </Tab.Item>
                        <Tab.Item tab={1}>
                            <div className="px-2">
                                <CategoryDataTable />
                            </div>
                        </Tab.Item>
                        <Tab.Item tab={2}>
                            <div className="px-2">
                                <PropertyDataTable />
                            </div>
                        </Tab.Item>
                        <Tab.Item tab={3}>
                            <div className="px-2">
                                <TariffDataTable />
                            </div>
                        </Tab.Item>
                    </Tab.Content>
                </Tab>

            </div>
        </MainLayout.Container>
    </MainLayout>
}

export default Others;