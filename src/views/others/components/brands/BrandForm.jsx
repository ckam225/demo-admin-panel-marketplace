import { createBrandValidationRule } from "../../../../hooks/validators";
import { useFormik } from "formik";
import useBrands from "../../../../store/actions/brands";
import { toast } from "react-toastify";
import React from "react";

const BrandForm = ({ modalRef, brand = null }) => {

    const { loading, updateBrand, createBrand } = useBrands()

    const formik = useFormik({
        initialValues: {
            name: brand?.name ?? '',
            description: brand?.description ?? ''
        },
        validationSchema: createBrandValidationRule,
        onSubmit: async (values) => {
            if (brand?.id) {
                const success = await updateBrand(brand.id, values)
                if (success) {
                    formik.resetForm()
                    toast.success("Бренд успешно обновлен")
                    modalRef.current.close()
                }
            } else {
                const success = await createBrand(values)
                if (success) {
                    formik.resetForm()
                    toast.success("Бренд успешно создан")
                    modalRef.current.close()
                }
            }
        }
    })

    return <form onSubmit={formik.handleSubmit}>
        <div className="px-2">
            <label>Название</label>
            <input type="text" value={formik.values.name} onChange={formik.handleChange} name="name" className="form-control" />
            {formik.errors.name && formik.touched.name && (
                <small className="text-red-500">{formik.errors.name}</small>
            )}
        </div>
        <div className="px-2">
            <label>Описание</label>
            <textarea value={formik.values.description} onChange={formik.handleChange} name="description" className="form-control" />
            {formik.errors.description && formik.touched.description && (
                <small className="text-red-500">{formik.errors.description}</small>
            )}
        </div>
        <div className="px-2">
            {loading ? <div>...</div> : <button type="submit" className="mr-3 button ">{brand?.id ? 'Обновить' : 'Добавить'}</button>}
        </div>
    </form>
}

export default BrandForm;