const PropertyDetail = ({ property }) => {
    return <div className=" w-full select-none">
        <table className="table px-2 select-none">
            <thead>
            <tr>
                <th>Код 1C</th>
                <th>Описание</th>
                <th>Значение</th>
            </tr>
            </thead>
            <tbody>
            {property?.values?.length > 0 ? property?.values?.map(item => (<tr>
                <td>{item.code_1c}</td>
                <td>{item.description}</td>
                <td>{item.value}</td>
            </tr>)) : null}
            </tbody>
        </table>
    </div>
}

export default PropertyDetail;