import React from "react";
import Dropdown from "../../../../components/controls/Dropdown";
import Icon from "../../../../components/controls/Icon";

const PropertyTableRow = ({ property, onOpen = null, onDelete = null, onEditValues = null, onEdit = null }) => {

    const handleOpen = () => {
        if (onOpen)
            onOpen(property.id)
    }

    const handleDelete = () => {
        if (onDelete)
            onDelete(property.id)
    }

    const handleEditValues = () => {
        if (onEditValues)
            onEditValues(property.id)
    }

    const handleEdit = () => {
        if (onEdit)
            onEdit(property.id)
    }

    const showToolMenu = React.useMemo(
        () => onOpen || onDelete || onEditValues || onEdit,
        [onOpen, onDelete, onEditValues, onEdit]
    )

    return <tr>
        <td>#{property.id}</td>
        <td>{property.name}</td>
        <td>{property.description}</td>
        <td>{property.code_1c}</td>
        <td>
            {showToolMenu ? <Dropdown className="relative">
                <Dropdown.Toggle>
                    <button className="inline-flex items-center justify-center  rounded-md border border-gray-300 shadow-sm w-8 h-8 bg-white text-sm font-medium text-gray-700 focus:outline-none focus:ring-1 focus:ring-offset-gray-500 focus:ring-blue-accent">
                        <Icon name="dotv" className="w-6 h-6 text-gray-600" />
                    </button>
                </Dropdown.Toggle>
                <Dropdown.Content className="dp absolute mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none right-0 top-5">
                    {onOpen ? <Dropdown.Item className="dropdown-item py-3" onClick={handleOpen}>Просмотр</Dropdown.Item> : null}
                    {onEdit ? <Dropdown.Item className="dropdown-item py-3" onClick={handleEdit}>Редактировать свойство</Dropdown.Item> : null}
                    {onEditValues ? <Dropdown.Item className="dropdown-item py-3" onClick={handleEditValues}>Редактировать значения</Dropdown.Item> : null}
                    {onDelete ? <Dropdown.Item className="dropdown-item py-3" onClick={handleDelete}>Удалить</Dropdown.Item> : null}
                </Dropdown.Content>
            </Dropdown> : null}
        </td>
    </tr>

}

export default PropertyTableRow;