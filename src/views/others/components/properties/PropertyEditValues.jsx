import React from "react";
import TableEmptyRow from "../../../../components/controls/TableEmptyRow";
import Loader from "../../../../components/controls/Loader";
import { useEditPropertyValues } from "../../../../store/actions/properties";
import PropertyValueTableRow from "./PropertyValueTableRow";
import { useFormik } from "formik";
import { propertyValueValidationRule } from "../../../../hooks/validators";
import { toast } from "react-toastify";

const PropertyEditValues = ({ property }) => {
    const editPropertyValues = useEditPropertyValues()

    const handleDelete = (value) => {
        editPropertyValues.deleteValue(property.id, value.id)
    }

    const formik = useFormik({
        initialValues: {
            value: "",
            description: "",
        },
        validationSchema: propertyValueValidationRule,
        onSubmit: async (values) => {
            if (await editPropertyValues.createValue(property.id, {
                value: values.value,
                description: values.description
            })) {
                formik.resetForm()
                toast.success('Значение свойства успешно создано')
            }
        },
    });

    React.useEffect(() => {
        if (property) {
            editPropertyValues.fetchValues(property.id)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [property])

    return <div className=" w-full select-none">
        <div className="w-80 mx-3">
            <form onSubmit={formik.handleSubmit}>
                <div className="px-2">
                    <input type="text" value={formik.values.value} onChange={formik.handleChange} name="value" className="form-control" placeholder="Значение" />
                    {formik.errors.value && formik.touched.value && (
                        <small className="text-red-500">{formik.errors.value}</small>
                    )}
                </div>
                <div className="px-2">
                    <textarea type="number" value={formik.values.description} onChange={formik.handleChange} name="description" className="form-control" placeholder="Описание" />
                    {formik.errors.description && formik.touched.description && (
                        <small className="text-red-500">{formik.errors.description}</small>
                    )}
                </div>

                <div className="px-2 mt-2">
                    {editPropertyValues.loading ? <Loader /> : <button type="submit" className="mr-3 button ">Создать</button>}
                </div>
            </form>
            {/*<button type="button" onClick={handleButtonAdd} className="btn btn-success">Добавить</button>*/}
        </div>
        <table className="table px-2 select-none">
            <thead>
                <tr>
                    <th>Ид</th>
                    <th>Значение</th>
                    <th>Описание</th>
                    <th>Код 1С</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {editPropertyValues.loading ? <TableEmptyRow colspan={9} className="flex justify-center  py-5">
                    <Loader />
                </TableEmptyRow> : editPropertyValues.values?.length > 0 ?
                    editPropertyValues.values?.map(value => <PropertyValueTableRow value={value} key={value.id} onDelete={handleDelete}
                    />) : <TableEmptyRow colspan={9} className="text-sm text-center py-5">Нет свойств</TableEmptyRow>}
            </tbody>
        </table>
    </div>
}

export default PropertyEditValues;