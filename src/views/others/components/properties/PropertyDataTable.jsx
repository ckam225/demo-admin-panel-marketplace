import React from "react";
import { useProperties } from "../../../../store/actions/properties";
import styled from "styled-components";
import SearchBox from '../../../../components/controls/SearchBox'
import TableEmptyRow from "../../../../components/controls/TableEmptyRow";
import Loader from "../../../../components/controls/Loader";
import Paginator from "../../../../components/controls/Paginator";
import { endOt } from "../../../../helpers";
import Modal from "../../../../components/controls/Modal";
import PropertyTableRow from "./PropertyTableRow";
import PropertyDetail from "./PropertyDetail";
import PropertyEdit from "./PropertyEdit";
import PropertyEditValues from "./PropertyEditValues";
import Icon from "../../../../components/controls/Icon";

const PropertyDataTable = () => {

    const [search, setSearch] = React.useState('')
    const [page, setPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)
    const [loadingCurrent, setLoadingCurrent] = React.useState(false)
    const modalView = React.useRef()
    const modalEdit = React.useRef()
    const modalEditValues = React.useRef()
    const { properties, pageCount, loading, currentProperty, fetchProperties, fetchCurrentProperty, setCurrentProperty } = useProperties()

    const query = React.useMemo(() => {
        let q = `?page=${page}&per_page=${perPage}`
        if (search != null)
            q += `&q=${search}`
        return q
    }, [search, page, perPage])

    const handleSearchChange = (s) => {
        setSearch(s)
    }

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    const handleDoubleClick = async (propertyId) => {
        modalView.current.open()
        setLoadingCurrent(true)
        await fetchCurrentProperty(propertyId)
        setLoadingCurrent(false)
    }

    const handleEdit = async (propertyId) => {
        modalEdit.current.open()
        setLoadingCurrent(true)
        await fetchCurrentProperty(propertyId)
        setLoadingCurrent(false)
    }

    const handleEditValues = async (propertyId) => {
        modalEditValues.current.open()
        setLoadingCurrent(true)
        await fetchCurrentProperty(propertyId)
        setLoadingCurrent(false)
    }

    const handleEditSubmit = () => {
        modalEdit.current.close()
        fetchProperties(query)
    }

    const handleCreate = async () => {
        modalEdit.current.open()
        setLoadingCurrent(true)
        await setCurrentProperty(null)
        setLoadingCurrent(false)
    }

    React.useEffect(() => {
        fetchProperties(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    return <Wrapper>
        <div className="flex items-center py-4 border-b">
            <SearchBox onChange={handleSearchChange} placeholder="Поиск по названию" className="py-1 rounded-sm text-sm" />
            <button onClick={handleCreate}
                className="button mx-3 flex items-center px-3"
                title="Роли и доступ">
                <Icon name="plus" className=" mr-2" size="16" />
                <span className="text-xs">Создать</span>
            </button>
        </div>
        <table className="table px-2 select-none">
            <thead>
                <tr>
                    <th>Ид</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Код 1С</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {loading ? <TableEmptyRow colspan={9} className="flex justify-center  py-5">
                    <Loader />
                </TableEmptyRow> : properties?.length > 0 ?
                    properties.map(property => <PropertyTableRow property={property} key={property.id} onOpen={handleDoubleClick} onEdit={handleEdit} onEditValues={handleEditValues}
                    />) : <TableEmptyRow colspan={9} className="text-sm text-center py-5">Нет свойств</TableEmptyRow>}
            </tbody>
        </table>
        {!loading && <div className="py-2 flex justify-between items-center bg-gray-50">
            {(properties.length > 0 && pageCount > 1) ? <Paginator className="text-blue-accent"
                itemClass=" text-sm rounded-full mx-1 w-6 h-6 focus:outline-none"
                activeItemClass="pagin-item-color"
                pages={pageCount}
                setCurrentPage={setPage} /> : <div />}
            <div className=" mx-3">
                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                    <option value="10">10</option>
                    <option value="30">30</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
        </div>}

        <Modal refId={modalView} style={{ width: '80%', height: '90%' }}>
            <Modal.Header>
                <div className="flex justify-between">
                    <span>Свойства "{endOt(currentProperty?.name, 60)}"</span>
                </div>
            </Modal.Header>
            <Modal.Body>
                <div className="flex  overflow-hidden overflow-y-auto" style={{ height: '100%' }}>
                    {loadingCurrent ? <Loader /> : <PropertyDetail property={currentProperty} />}
                </div>
            </Modal.Body>
        </Modal>

        <Modal refId={modalEdit} style={{ width: '80%', height: '90%' }}>
            <Modal.Header>
                <div className="flex justify-between">
                    <span>{currentProperty ? endOt(currentProperty?.name, 60) : 'Создание свойства'}</span>
                </div>
            </Modal.Header>
            <Modal.Body>
                <div className="flex  overflow-hidden overflow-y-auto" style={{ height: '100%' }}>
                    {loadingCurrent ? <Loader /> : <PropertyEdit property={currentProperty} onSubmit={handleEditSubmit} />}
                </div>
            </Modal.Body>
        </Modal>

        <Modal refId={modalEditValues} style={{ width: '80%', height: '90%' }}>
            <Modal.Header>
                <div className="flex justify-between">
                    <span>Свойства "{endOt(currentProperty?.name, 60)}"</span>
                </div>
            </Modal.Header>
            <Modal.Body>
                <div className="flex  overflow-hidden overflow-y-auto" style={{ height: '100%' }}>
                    {loadingCurrent ? <Loader /> : <PropertyEditValues property={currentProperty} />}
                </div>
            </Modal.Body>
        </Modal>
    </Wrapper>
}

const Wrapper = styled.div`
table{
    width: 100%;
    user-select: none;
}
table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`

export default PropertyDataTable;