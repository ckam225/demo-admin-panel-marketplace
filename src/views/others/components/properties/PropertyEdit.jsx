import React from "react";
import { useEditProperty } from "../../../../store/actions/properties";
import { propertyValidationRule } from "../../../../hooks/validators";
import { useFormik } from "formik";

const PropertyEdit = ({ property, onSubmit = null }) => {
    const editPropertyAction = useEditProperty()

    const formik = useFormik({
        initialValues: {
            type: property?.type ?? '',
            name: property?.name ?? '',
            description: property?.description ?? '',
        },
        validationSchema: propertyValidationRule,
        onSubmit: async (values) => {
            let response;
            if (property) {
                response = await editPropertyAction.updateProperty(property.id, values)
            } else {
                response = await editPropertyAction.createProperty(values)
            }
            if (response) {
                if (onSubmit)
                    onSubmit(response)
            }
        },
    });

    return <div className=" w-full select-none">
        <form className="card my-4 mx-3 flex flex-col" onSubmit={formik.handleSubmit}>
            <div className="flex" >
                <div className="flex flex-col w-1/3 p-5">
                    <div className="flex flex-col mt-2 ">
                        <label htmlFor="" className="mr-2 text-gray-600">Тип</label>
                        <select
                            className=" p-2  flex-auto "
                            name="type"
                            value={formik.values.type}
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                        >
                            <option value="" className="text-gray-300" disabled>--  Выберите тип  --</option>
                            {editPropertyAction.types?.length > 0 && editPropertyAction.types?.map(type => {
                                return (<option value={type} key={type}>{type}</option>)
                            })}

                        </select>
                        {formik.errors.type && formik.touched.type && (
                            <small className="text-red-500">{formik.errors.type}</small>
                        )}
                    </div>
                    <div className="flex flex-col mt-2">
                        <label htmlFor="" className="mr-2 text-gray-600">Название</label>
                        <input
                            type="text"
                            className="border p-2   flex-auto "
                            name="name"
                            value={formik.values.name}
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.name && formik.touched.name && (
                            <small className="text-red-500">{formik.errors.name}</small>
                        )}
                    </div>
                    <div className="flex flex-col mt-2">
                        <label htmlFor="" className="mr-2 text-gray-600">Описание</label>
                        <textarea
                            className="  flex-auto "
                            name="description"
                            value={formik.values.description}
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.description && formik.touched.description && (
                            <small className="text-red-500">{formik.errors.description}</small>
                        )}
                    </div>
                </div>

            </div>
            <div className="flex border-t bg-gray-50 py-2 px-4 mt-5">
                <span></span>
                {editPropertyAction.loading ? <div>Loading...</div> : <button
                    type="submit"
                    className="button rounded-sm"
                >
                    {property ? 'Сохранить' : 'Создать'}
                </button>}
            </div>
        </form>
    </div>
}

export default PropertyEdit;