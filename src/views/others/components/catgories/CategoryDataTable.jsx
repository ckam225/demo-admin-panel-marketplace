import React from "react";
import { useCategories, useEditCategory, useSearchCategories } from "../../../../store/actions/categories";
import styled from "styled-components";
import SearchBox from '../../../../components/controls/SearchBox'
import TableEmptyRow from "../../../../components/controls/TableEmptyRow";
import Loader from "../../../../components/controls/Loader";
import Paginator from "../../../../components/controls/Paginator";
import CategoryTableRow from "./CategoryTableRow";
import CategoryDetail from "./CategoryDetail";
import CategoryEdit from "./CategoryEdit";
import SelectBox from '../../../../components/controls/SelectBox'
import { endOt } from "../../../../helpers";
import Modal from "../../../../components/controls/Modal";

const CategoryDataTable = () => {

    const [search, setSearch] = React.useState('')
    const { categories, pageCount, loading, fetchCategories, fetchCurrentCategory, currentCategory } = useCategories()
    const { searchCategories } = useSearchCategories()
    // eslint-disable-next-line no-unused-vars
    const editCategory = useEditCategory()
    const [page, setPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)
    const [parent, setParent] = React.useState(null)
    const [loadingCurrent, setLoadingCurrent] = React.useState(false)
    const modalView = React.useRef()
    const modalEdit = React.useRef()

    const query = React.useMemo(() => {
        let q = `?page=${page}&per_page=${perPage}`
        if (search != null)
            q += `&q=${search}`
        if (parent)
            q += `&parent=${parent.id}`
        return q
    }, [search, page, perPage, parent])

    const handleSearchChange = (s) => {
        setSearch(s)
    }

    function handlePerPageChange(e) {
        setPerPage(e.target.value)
    }

    const handleOpen = async (categoryId) => {
        modalView.current.open()
        setLoadingCurrent(true)
        await fetchCurrentCategory(categoryId)
        setLoadingCurrent(false)
    }

    const handleEdit = async (categoryId) => {
        modalEdit.current.open()
        setLoadingCurrent(true)
        await fetchCurrentCategory(categoryId)
        setLoadingCurrent(false)
    }

    function handleUpdateCategory() {
        fetchCategories(query)
    }

    React.useEffect(() => {
        fetchCategories(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    return <Wrapper>
        <div className="flex items-center py-4 border-b">
            <div className="w-80 mx-3">
                <SelectBox items={searchCategories}
                    onChange={(category) => setParent(category)}
                    displayField="name"
                    className="rounded-md"
                    contentClass="rounded-md"
                    style={{ padding: '6px 6px' }} />
            </div>
            <SearchBox onChange={handleSearchChange} placeholder="Поиск по названию" className="py-1 rounded-sm text-sm" />
        </div>
        <table className="table px-2 select-none">
            <thead>
                <tr>
                    <th>Ид</th>
                    <th>Название</th>
                    <th>Код 1C</th>
                    <th>Виден</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {loading ? <TableEmptyRow colspan={9} className="flex justify-center  py-5">
                    <Loader />
                </TableEmptyRow> : categories?.length > 0 ?
                    categories.map(category => <CategoryTableRow category={category} key={category.id} onSelected={handleOpen} onEdit={handleEdit}
                    />) : <TableEmptyRow colspan={9} className="text-sm text-center py-5">Нет категорий</TableEmptyRow>}
            </tbody>
        </table>
        {!loading && <div className="py-2 flex justify-between items-center bg-gray-50">
            {(categories.length > 0 && pageCount > 1) ? <Paginator className="text-blue-accent"
                itemClass=" text-sm rounded-full mx-1 w-6 h-6 focus:outline-none"
                activeItemClass="pagin-item-color"
                pages={pageCount}
                setCurrentPage={setPage} /> : <div />}
            <div className=" mx-3">
                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                    <option value="10">10</option>
                    <option value="30">30</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
        </div>}

        <Modal refId={modalView} style={{ width: '80%', height: '90%' }}>
            <Modal.Header>
                <div className="flex justify-between">
                    <span>{endOt(currentCategory?.name, 60)}</span>
                </div>
            </Modal.Header>
            <Modal.Body>
                <div className="flex  overflow-hidden overflow-y-auto" style={{ height: '100%' }}>
                    {loadingCurrent ? <Loader /> : <CategoryDetail category={currentCategory} />}
                </div>
            </Modal.Body>
        </Modal>

        <Modal refId={modalEdit} style={{ width: '80%', height: '90%' }} dismissible={false}>
            <Modal.Header>
                <div className="flex justify-between">
                    <span>{endOt(currentCategory?.name, 60)}</span>
                </div>
            </Modal.Header>
            <Modal.Body>
                <div className="flex  overflow-hidden overflow-y-auto" style={{ height: '100%' }}>
                    {loadingCurrent ? <Loader /> : <CategoryEdit category={currentCategory} onUpdate={handleUpdateCategory} />}
                </div>
            </Modal.Body>
        </Modal>
    </Wrapper>
}

const Wrapper = styled.div`
table{
    width: 100%;
    user-select: none;
}
table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`

export default CategoryDataTable;