import React from "react";
import Dropdown from "../../../../components/controls/Dropdown";
import Icon from "../../../../components/controls/Icon";

const CategoryTableRow = ({ category, onSelected = null, onEdit = null }) => {

    const handleShow = () => {
        if (onSelected)
            onSelected(category.id)
    }

    const handleEdit = () => {
        if (onEdit)
            onEdit(category.id)
    }

    return <tr>
        <td>#{category.id}</td>
        <td>{category.name}</td>
        <td>{category.code_1c}</td>
        <td>
            <input type="checkbox" checked={category.isvisible} readOnly={true} />
        </td>
        <td>
            <Dropdown className="relative">
                <Dropdown.Toggle>
                    <button className="inline-flex items-center justify-center  rounded-md border border-gray-300 shadow-sm w-8 h-8 bg-white text-sm font-medium text-gray-700 focus:outline-none focus:ring-1 focus:ring-offset-gray-500 focus:ring-blue-accent">
                        <Icon name="dotv" className="w-6 h-6 text-gray-600" />
                    </button>
                </Dropdown.Toggle>
                <Dropdown.Content className="dp absolute mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none right-0 top-5">
                    <Dropdown.Item className="dropdown-item py-3" onClick={handleShow}>Просмотр</Dropdown.Item>
                    <Dropdown.Item className="dropdown-item py-3" onClick={handleEdit}>Редактировать</Dropdown.Item>
                </Dropdown.Content>
            </Dropdown>
        </td>
    </tr>

}

export default CategoryTableRow;