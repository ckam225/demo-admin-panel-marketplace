import React, { useCallback } from "react";
import SelectBox from '../../../../components/controls/SelectBox'
import { useProperties } from "../../../../store/actions/properties";
import TableEmptyRow from "../../../../components/controls/TableEmptyRow";
import Loader from "../../../../components/controls/Loader";
import PropertyTableRow from "../properties/PropertyTableRow";
import { useEditCategory } from "../../../../store/actions/categories";
import { updateCategory } from "../../../../api/categories";
import Switch from '../../../../components/controls/Switch'

const CategoryEdit = ({ category, onUpdate }) => {
    const [property, setProperty] = React.useState()
    const propertiesAction = useProperties()
    const editCategoryAction = useEditCategory()
    // eslint-disable-next-line no-unused-vars
    const [perPage, setPerPage] = React.useState(100)
    // eslint-disable-next-line no-unused-vars
    const [page, setPage] = React.useState(1)
    const [search, setSearch] = React.useState('')
    const [state, setState] = React.useState({
        name: category?.name,
        description: category?.description ?? '',
        isvisible: category?.isvisible,
    })
    const [updateLoading, setUpdateLoading] = React.useState(false)

    function handleUpdateChange(e) {
        const obj = { ...state }
        obj[e.target.name] = e.target.type === 'checkbox' ? e.target.checked : e.target.value
        setState(obj)
    }

    const handleVisible = React.useCallback((e) => {
        setState({ ...state, isvisible: e })
    }, [state])

    const handleSaveCategory = useCallback(async (e) => {
        e.preventDefault()
        try {
            setUpdateLoading(true)
            await updateCategory(category.id, state)
            if (onUpdate) {
                onUpdate()
            }
        } catch (err) {
            console.error(err);
        }
        finally {
            setUpdateLoading(false)
        }

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [category, state])

    const handleButtonAdd = useCallback(() => {
        if (property) {
            editCategoryAction.addCategoryProperty(category.id, property)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [category, property])

    const handleDelete = (propId) => {
        editCategoryAction.deleteCategoryProperty(category.id, propId)
    }

    const handleOnSearch = (v) => {
        setSearch(v)
    }

    const normalizeSearchPropDisplayField = (item) => {
        return typeof item === 'object' ? `#${item['id']} ${item['name']}` : item
    }

    const query = React.useMemo(() => {
        let q = `?page=${page}&per_page=${perPage}`
        if (search != null) {
            q += `&q=${search}`
        }
        return q
    }, [search, perPage, page])

    React.useEffect(() => {
        propertiesAction.fetchProperties(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    React.useEffect(()=> {
        (async () => {
            if (category?.id) {
                await editCategoryAction.fetchCategoryProperties(category.id)
            }
        })()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [category])

    return <div className=" w-full select-none">
        <form className="p-4 w-2/3" onSubmit={handleSaveCategory}>
            <div>
                <label htmlFor="name">Название</label>
                <input type="text" name="name" id="name" className="textbox p-2"
                    value={state.name}
                    onChange={handleUpdateChange} />
            </div>
            <div className="flex my-2 flex-col">
                <Switch
                    checked={state.isvisible}
                    onChange={handleVisible} >
                    <span className="mr-4">Категория появляется</span>
                </Switch>
            </div>
            <div className="">
                <label htmlFor="description">Описание</label>
                <textarea cols={10} rows={8} name="description" id="description"
                    className="textbox p-2"
                    maxLength={255}
                    value={state.description}
                    onChange={handleUpdateChange}
                />
            </div>
            <div>
                {updateLoading ? <span>loading...</span> :
                    <button type="submit" onClick={handleButtonAdd} className="btn button">
                        Сохранить
                    </button>
                }
            </div>
        </form>
        <div className="text-2xl text-center">Характеристики</div>
        <div className="flex mx-3">
            <SelectBox items={propertiesAction.properties}
                onChange={(prop) => setProperty(prop)}
                onSearch={handleOnSearch}
                displayField={normalizeSearchPropDisplayField}
                className=" w-72"
                contentClass="rounded-md"
                style={{ padding: '6px 6px' }} />
            <button type="button" onClick={handleButtonAdd} className="btn btn-success">Добавить</button>
        </div>

        <table className="table px-2 select-none">
            <thead>
                <tr>
                    <th>Ид</th>
                    <th>Название</th>
                    <th>Тип</th>
                    <th>Описание</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {editCategoryAction.loading ? <TableEmptyRow colspan={9} className="flex justify-center  py-5">
                    <Loader />
                </TableEmptyRow> : editCategoryAction.properties?.length > 0 ?
                    editCategoryAction.properties.map((property, i) => <PropertyTableRow property={property} key={i} onDelete={handleDelete}
                    />) : <TableEmptyRow colspan={9} className="text-sm text-center py-5">Нет свойств</TableEmptyRow>}
            </tbody>
        </table>
    </div>
}

export default CategoryEdit;