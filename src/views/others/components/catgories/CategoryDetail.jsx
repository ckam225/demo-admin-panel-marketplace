import React from "react";
import Tab from "../../../../components/controls/Tab";
import { useCategories } from "../../../../store/actions/categories";

const CategoryDetail = ({ category }) => {

    const { loading, fetchCurrentCategoryProps, currentCategoryProps } = useCategories()

    React.useEffect(() => {
        if (category) {
            fetchCurrentCategoryProps(category.id)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [category])

    return <div className=" w-full select-none">

        <div className="text-2xl text-center">Характеристики</div>

        <Tab>
            <Tab.Headers className="flex sticky inset-x-0 mx-5 top-0 left-0 bg-white">
                {loading ? 'loading group...' : <>
                    {currentCategoryProps?.length > 0 ? currentCategoryProps.map((group, i) => (<Tab.Header tab={i} key={i}>
                        {group?.group}</Tab.Header>)) : <></>}
                </>}
            </Tab.Headers>
            <Tab.Content className="mx-5">
                {currentCategoryProps?.length > 0 ? currentCategoryProps.map((group, i) => (
                    <Tab.Item tab={i} key={i}>
                        <table className="table px-2 select-none">
                            <thead>
                                <tr>
                                    <th>Название</th>
                                    <th>Значение</th>
                                </tr>
                            </thead>
                            <tbody>
                                {group?.list.length > 0 ? (group.list.map((item) => (
                                    <tr>
                                        <td>{item.name}</td>
                                        <td>{item.values.map((value) => value.value).join(', ')}</td>
                                    </tr>
                                ))) : 'loading...'}
                            </tbody>
                        </table>
                    </Tab.Item>
                )) : null}
            </Tab.Content>
        </Tab>
    </div>
}

export default CategoryDetail;