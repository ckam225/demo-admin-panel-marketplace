import { useTariffValidation } from "../../../../hooks/validators";
import { useFormik } from "formik";
import { useTariffs } from "../../../../store/actions/tariffs";
import { toast } from "react-toastify";
import React from "react";
import Loader from "../../../../components/controls/Loader";

const TariffForm = ({ modalRef, tariff = null }) => {

    const { loading, updateTariff, createTariff } = useTariffs()

    const formik = useFormik({
        initialValues: {
            name: tariff?.name ?? '',
            commission: tariff?.commission ?? '',
            commission_acquiring: tariff?.commission_acquiring ?? '',
            min_commission_RUB: tariff?.min_commission_RUB ?? '',
        },
        validationSchema: useTariffValidation,
        onSubmit: async (values) => {
            if (tariff?.id) {
                const success = await updateTariff(tariff.id, values)
                if (success) {
                    formik.resetForm()
                    toast.success("Тариф успешно обновлен")
                    modalRef.current.close()
                }
            } else {
                const success = await createTariff(values)
                if (success) {
                    formik.resetForm()
                    toast.success("Тариф успешно создан")
                    modalRef.current.close()
                }
            }
        }
    })

    return <form onSubmit={formik.handleSubmit}>
        <div className="px-2">
            <label>Название</label>
            <input type="text" value={formik.values.name} onChange={formik.handleChange} name="name" className="form-control" />
            {formik.errors.name && formik.touched.name && (
                <small className="text-red-500">{formik.errors.name}</small>
            )}
        </div>
        <div className="px-2">
            <label>Комиссия</label>
            <input type="number" value={formik.values.commission} onChange={formik.handleChange} name="commission" className="form-control" />
            {formik.errors.commission && formik.touched.commission && (
                <small className="text-red-500">{formik.errors.commission}</small>
            )}
        </div>

        <div className="px-2">
            <label>Комиссионый сбор</label>
            <input type="number" value={formik.values.commission_acquiring} onChange={formik.handleChange} name="commission_acquiring" className="form-control" />
            {formik.errors.commission_acquiring && formik.touched.commission_acquiring && (
                <small className="text-red-500">{formik.errors.commission_acquiring}</small>
            )}
        </div>
        <div className="px-2">
            <label>Минимальная комиссия в рублях</label>
            <input type="number" value={formik.values.min_commission_RUB} onChange={formik.handleChange} name="min_commission_RUB" className="form-control" />
            {formik.errors.min_commission_RUB && formik.touched.min_commission_RUB && (
                <small className="text-red-500">{formik.errors.min_commission_RUB}</small>
            )}
        </div>
        <div className="px-2 mt-2">
            {loading ? <Loader /> : <button type="submit" className="mr-3 button ">{tariff?.id ? 'Обновить' : 'Добавить'}</button>}
        </div>
    </form>
}

export default TariffForm;