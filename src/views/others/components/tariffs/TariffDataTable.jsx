import React from "react";
import styled from "styled-components";
import Loader from "../../../../components/controls/Loader";
import Paginator from "../../../../components/controls/Paginator";
import { useTariffs } from "../../../../store/actions/tariffs";
import TableEmptyRow from "../../../../components/controls/TableEmptyRow";
import TariffTableRow from "./TariffTableRow";
import SearchBox from "../../../../components/controls/SearchBox";
import { useRecoilValue } from "recoil";
import { userRoleState } from "../../../../store/states/base";
import Confirm from "../../../../components/controls/Confirm";
import Modal from "../../../../components/controls/Modal";
import TariffForm from "./TariffForm";
import { toast } from "react-toastify";
import Icon from "../../../../components/controls/Icon";

const TariffDataTable = () => {

    const [search, setSearch] = React.useState('')
    const tariffAction = useTariffs()
    const [page, setPage] = React.useState(1)
    const [perPage, setPerPage] = React.useState(30)
    const modal = React.useRef()
    const confirmBox = React.useRef()
    const { isSuperuser } = useRecoilValue(userRoleState)
    const [loadingCurrent, setLoadingCurrent] = React.useState(false)
    const [confirmData, setConfirmData] = React.useState({
        title: '',
        message: '',
        action: () => { }
    })

    const query = React.useMemo(() => {
        let q = `?page=${page}&per_page=${perPage}`
        if (search != null)
            q += `&q=${search}`
        return q
    }, [search, page, perPage])

    const handleSearchChange = (s) => {
        setSearch(s)
    }

    const handlePerPageChange = (e) => {
        setPerPage(e.target.value)
    }

    const handleCreate = async () => {
        modal.current.open()
        setLoadingCurrent(true)
        await tariffAction.setCurrentTariff(null)
        setLoadingCurrent(false)
    }

    // eslint-disable-next-line no-unused-vars
    const handleUpdateClick = async (tariff) => {
        modal.current.open()
        setLoadingCurrent(true)
        await tariffAction.fetchCurrentTariff(tariff.id)
        setLoadingCurrent(false)
    }

    // eslint-disable-next-line no-unused-vars
    function handleDestroyClick(tariff) {
        setConfirmData({
            title: "Уверены ли вы?",
            message: 'Вы действительно хотите Удалить это? Удаление данных, это действие необратимо',
            action: async () => {
                if (isSuperuser) {
                    const success = await tariffAction.deleteTariff(tariff.id)
                    if (success) {
                        toast.success('Данные успешно удалены')
                    }
                }
            }
        })
        confirmBox.current.open()
    }

    React.useEffect(() => {
        tariffAction.fetchTariffs(query)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [query])

    return <Wrapper>
        <div className="flex items-center py-4 border-b">
            <SearchBox onChange={handleSearchChange} placeholder="Поиск по названию" className="py-1 rounded-sm text-sm" />
            <button onClick={handleCreate}
                className="button mx-3 flex items-center px-3 "
                title="Роли и доступ">
                <Icon name="plus" className=" mr-2" size="18" />
                <span className="text-xs">Создать</span>
            </button>
        </div>
        <table className="table px-2 select-none">
            <thead>
                <tr>
                    <th>Ид</th>
                    <th>Название</th>
                    <th>Комиссия</th>
                    <th>Комиссионый сбор</th>
                    <th>Минимальная комиссия в RUB</th>
                    <th>Код 1С</th>
                </tr>
            </thead>
            <tbody>
                {tariffAction.loading ? <TableEmptyRow colspan={9} className="flex justify-center  py-5">
                    <Loader />
                </TableEmptyRow> : tariffAction.tariffs?.length > 0 ?
                    tariffAction.tariffs.map(tariff => <TariffTableRow tariff={tariff} key={tariff.id}
                    />) : <TableEmptyRow colspan={9} className="text-sm text-center py-5">Нет тарифов</TableEmptyRow>}
            </tbody>
        </table>
        {!tariffAction.loading && <div className="py-2 flex justify-between items-center bg-gray-50">
            {(tariffAction.tariffs.length > 0 && tariffAction.pageCount > 1) ? <Paginator className="text-blue-accent"
                itemClass=" text-sm rounded-full mx-1 w-6 h-6 focus:outline-none"
                activeItemClass="pagin-item-color"
                pages={tariffAction.pageCount}
                setCurrentPage={setPage} /> : <div />}
            <div className=" mx-3">
                <select onChange={handlePerPageChange} className="py-1 focus:ring-0 text-base" value={perPage}>
                    <option value="10">10</option>
                    <option value="30">30</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </div>
        </div>}
        <Modal refId={modal}>
            <Modal.Header>
                <div className="flex justify-between">
                    <span>{tariffAction.currentTariff ? 'Редактирование тарифа' : 'Создание тарифа'}</span>
                </div>
            </Modal.Header>
            <Modal.Body>
                <div className="flex  overflow-hidden overflow-y-auto m-5" style={{ height: '100%' }}>
                    {loadingCurrent ? <Loader /> : <TariffForm tariff={tariffAction.currentTariff} modalRef={modal} />}
                </div>
            </Modal.Body>
        </Modal>
        <Confirm refId={confirmBox} actions={[
            {
                label: 'отменить',
                click: () => { }
            }, {
                label: 'продолжать',
                className: 'text-red-500',
                click: confirmData.action
            }
        ]}>
            <div className="text-center font-bold pt-8">
                {confirmData.title}
            </div>
            <div className="text-center p-4">
                {confirmData.message}
            </div>
        </Confirm>
    </Wrapper>
}

const Wrapper = styled.div`
table{
    width: 100%;
    user-select: none;
}
table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`

export default TariffDataTable;