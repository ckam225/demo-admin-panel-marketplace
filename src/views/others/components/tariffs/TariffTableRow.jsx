import React from 'react';

const TariffTableRow = ({ tariff }) => {
    return <tr>
        <td>#{tariff.id}</td>
        <td>{tariff.name}</td>
        <td>{tariff.commission}</td>
        <td>{tariff.commission_acquiring}</td>
        <td>{tariff.min_commission_RUB}</td>
        <td>{tariff.code_1c}</td>
    </tr>
}

export default TariffTableRow;