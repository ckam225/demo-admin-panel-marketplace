import React from 'react'
import styled from 'styled-components'
import { endOt } from '../../helpers'

const products = [
    'https://img.mvideo.ru/Pdb/50120282b.jpg',
    'https://img.mvideo.ru/Pdb/50143190b.jpg',
    'https://img.mvideo.ru/Pdb/50128993b.jpg',
    'https://img.mvideo.ru/Pdb/50136892b.jpg'
]
const PopularProduct = () => {

    return <Wrapper>
        <div className="bg-white p-3">
            <table className="table-3">
                <tbody>
                    {products.length > 0 && products.map((url, e) => <tr key={e}>
                        <td>
                            <img src={url} width="48" alt="dsk" />
                        </td>
                        <td>
                            <div className="flex">
                                <span >{endOt('Bluetooth Beats Studio3', 20)}</span>
                            </div>
                        </td>
                        <td>
                            <span >#45085406</span></td>
                        <td>
                            <span className="text-green-600">9 990 ₽</span>
                        </td>
                    </tr>)}
                </tbody>
            </table>
        </div>
    </Wrapper>
}
const Wrapper = styled.div`
table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`
export default PopularProduct;