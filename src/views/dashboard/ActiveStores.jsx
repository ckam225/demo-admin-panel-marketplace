import React from 'react'
import styled from 'styled-components'
import Icon from '../../components/controls/Icon'
import { useLatestStores } from '../../store/actions/stores'
import { BaseLink } from '../../components/shared/NavItem'
import { DateTime } from 'luxon'
import { currency } from '../../helpers'

const ActiveStores = () => {
    const { latestStores } = useLatestStores()
    return <Wrapper>
        <div className="bg-white p-3">
            <table className="table-3">
                <tbody>
                    {latestStores.length > 0 && latestStores.map(store => <tr key={store.id}>
                        <td>
                            <Icon name="user_group" className="text-gray-500" />
                        </td>
                        <td><BaseLink to={`/stores/${store.id}`}>{store.code_1c}</BaseLink></td>
                        <td><BaseLink to={`/stores/${store.id}`}>{store.name}</BaseLink></td>
                        <td><span>{store.contact}</span></td>
                        <td><span>{DateTime.fromISO(store.created_at).setLocale('ru').toFormat('dd LLL yyyy')}</span></td>
                        <td>
                            <span className="text-green-600">{currency.format(store.balance || 0)}</span>
                        </td>
                    </tr>)}
                </tbody>
            </table>
        </div>
    </Wrapper>
}
const Wrapper = styled.div`
table td {
    font-size: 0.80rem/* 12px */;
    line-height: 1rem/* 16px */;
    color: rgba(0, 0, 0, 0.8)
}
`
export default ActiveStores;