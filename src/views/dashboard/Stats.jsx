import React from "react";
import Icon from '../../components/controls/Icon'
import { currency } from "../../helpers";
import { useStats } from "../../store/actions/dashboard";


const Stats = () => {

  const { orderReport, storeReport } = useStats()


  return <div className="grid grid-cols-4 gap-2 xl:gap-5">

    <div className=" p-5 rounded-lg bg-white">
      <div className="flex justify-between mb-5">
        <Icon name="user_group" size="38" className="text-blue-400" />
        {/* <span>+23%</span> */}
      </div>
      <div className="font-light  text-sm lg:text-lg 2xl:text-3xl">{storeReport ? storeReport?.count : 0}</div>
      <div className="text-gray-500 mt-2 font-light">Партнеры</div>
    </div>
    <div className="p-2 xl:p-5 rounded-lg bg-white">
      <div className="flex justify-between mb-5">
        <Icon name="shopping_bag" size="38" className="text-indigo-400" />
        {/* <span>{orderReport ? orderReport.all.count : 0} шт</span> */}
      </div>
      <div className="font-light  text-sm lg:text-lg 2xl:text-3xl">
        {currency.format(orderReport ? orderReport.all.amount : 0)}
      </div>
      <div className="text-gray-500 mt-2 font-light">Заказы, {orderReport ? orderReport.all?.count : 0} шт</div>
    </div>
    <div className=" p-5 rounded-lg bg-white">
      <div className="flex justify-between mb-5">
        <Icon name="shopping_bag_plin" size="38" className="text-green-400" />
        {/* <span>{orderReport ? orderReport.sales.count : 0} шт</span> */}
      </div>
      <div className="font-light  text-sm lg:text-lg 2xl:text-3xl">
        {currency.format(orderReport ? orderReport.sales.amount : 0)}
      </div>
      <div className="text-gray-500 mt-2 font-light">Продажи, {orderReport ? orderReport.sales?.count : 0} шт</div>
    </div>
    <div className=" p-5 rounded-lg bg-white">
      <div className="flex justify-between mb-5">
        <Icon name="shopping_cart" size="38" className="text-red-400" />
        {/* <span>{orderReport ? orderReport.returns.count : 0} шт</span> */}
      </div>
      <div className="font-light  text-sm lg:text-lg 2xl:text-3xl">
        {currency.format(orderReport ? orderReport.returns.amount : 0)}
      </div>
      <div className="text-gray-500 mt-2 font-light">Возврат, {orderReport ? orderReport.returns?.count : 0} шт</div>
    </div>
  </div>
}

export default Stats;