import React from 'react'
import styled from 'styled-components'
import Icon from '../../components/controls/Icon'
import { useOrders } from '../../store/actions/orders'
import { DateTime } from 'luxon'
import { currency } from '../../helpers'

const RecentOrders = () => {
    const { latestOrders } = useOrders()
    return <Wrapper>
        <div className="bg-white p-3">
            <table className="table-3">
                <tbody>
                    {latestOrders.length > 0 && latestOrders.map(order => <tr key={order.id}>
                        <td>
                            <Icon name="shopping_cart" />
                        </td>
                        <td>
                            <div className="flex flex-col">
                                <span>{order.number}</span>
                                {order.created_at && <span className="text-gray-400">{DateTime.fromISO(order.created_at).setLocale('ru').toFormat('dd LLL yyyy')}</span>}
                            </div>
                        </td>
                        <td>
                            {order.is_return ? <span className="text-red-600">-{currency.format(order.amount || 0)}</span> :
                                <span className="text-green-600">{currency.format(order.amount || 0)}</span>
                            }
                        </td>
                    </tr>)}
                </tbody>
            </table>
        </div>
    </Wrapper>
}
const Wrapper = styled.div`
table td {
    font-size: 0.75rem/* 12px */;
    line-height: 1rem/* 16px */;
}
`
export default RecentOrders;