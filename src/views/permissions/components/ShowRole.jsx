import React from 'react'
import { useRecoilValue } from 'recoil';
import Confirm from '../../../components/controls/Confirm';
import Icon from '../../../components/controls/Icon';
import { toast } from 'react-toastify';
import { useRole } from '../../../store/actions/roles'
import { userRoleState } from '../../../store/states/base';
import PermissionPicker from './PermissionPicker';
import OwnPermissionList from './OwnPermissionList';


const ShowRole = ({ currentRole, onUpdate }) => {
    const { loading, updateRole, deleteRole } = useRole()
    const [name, setName] = React.useState(currentRole.name)
    const [description, setDesciption] = React.useState(currentRole.description)
    const [permissions, setPermissions] = React.useState(currentRole.permissions)
    const { isSuperuser } = useRecoilValue(userRoleState)
    const permisModal = React.useRef()
    const confirmBox = React.useRef()
    const [confirmData, setConfirmData] = React.useState({
        title: '',
        message: '',
        action: () => { }
    })

    React.useEffect(() => {
        setName(currentRole.name)
        setDesciption(currentRole.description)
        setPermissions(currentRole.permissions)
    }, [currentRole])


    function handleInputChange(e) {
        const { name, value } = e.target
        if (name === 'name') {
            setName(value)
        }
        if (name === 'description') {
            setDesciption(value)
        }
    }

    function handleUpdate(e) {
        e.preventDefault()
        setConfirmData({
            title: "Уверены ли вы?",
            message: 'Это может повлиять на других пользователей.' +
                'Тем не менее, сообщите им о любых необходимых изменениях.',
            action: async () => {
                if (isSuperuser) {
                    const payload = {
                        id: currentRole.id,
                        name,
                        description,
                        permissions
                    }
                    const success = await updateRole(payload.id, payload)
                    if (success) {
                        if (onUpdate)
                            onUpdate(payload)
                        toast.success('Данные успешно обновлены')
                    }
                }
            }
        })
        confirmBox.current.open()
    }

    function handleDestroyClick() {
        setConfirmData({
            title: "Уверены ли вы?",
            message: 'Вы действительно хотите Удалить это? Удаление данных, это действие необратимо',
            action: async () => {
                if (isSuperuser) {
                    const success = await deleteRole(currentRole.id)
                    if (success) {
                        setDesciption("")
                        setName("")
                        setPermissions([])
                        toast.success('Данные успешно удалены')
                    }
                }
            }
        })
        confirmBox.current.open()
    }

    function handleAddPermissionClick(e) {
        e.stopPropagation()
        permisModal.current.show()
    }

    function handleRemovePermission(perm) {
        const newPerms = permissions.filter(p => p.id !== perm.id)
        setPermissions(newPerms)
    }

    function handleSelectPerm(perms) {

        setPermissions(perms)
    }

    return (<>
        <div className="card my-4 mx-1  mt-4">
            <form className="" onSubmit={handleUpdate}>
                <div className=" px-2 py-5">
                    <table className="w-full">
                        <tbody>
                            <tr>
                                <td><span className="pr-2 text-gray-400">Роль</span></td>
                                <td>
                                    <input type="text" className="my-2 text-field" name="name" value={name} onChange={handleInputChange} />
                                </td>
                            </tr>
                            <tr>
                                <td><span className="pr-2 text-gray-400">Значение</span></td>
                                <td>
                                    <input type="text" className="text-field" name="description" value={description} onChange={handleInputChange} />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="">
                    <div className="flex justify-between py-2 px-2 mt-2 bg-black-10 ">
                        <span className="text-gray-500 ">Права</span>
                        {isSuperuser && <button type="button" className="focus:outline-none p-2" onClick={handleAddPermissionClick}>
                            <Icon name="plus" className="h-4 w-4 " />
                        </button>}
                    </div>
                    <OwnPermissionList permissions={permissions} onRemove={handleRemovePermission} />
                </div>
                <div className="flex justify-between">
                    <span></span>
                    {loading ? <div className="bg-white p-5 text-center">Loading...</div>
                        : <div className="flex px-4">
                            {isSuperuser && <button type="button" className="button btn-danger mr-2 my-10" onClick={handleDestroyClick}>
                                Удалить</button>}
                            {isSuperuser && <button type="submit" className="button  my-10">Сохранить</button>}
                        </div>}
                </div>
            </form>

        </div>
        <PermissionPicker refKey={permisModal} onSelected={handleSelectPerm} olders={permissions} />
        <Confirm refId={confirmBox} actions={[
            {
                label: 'отменить',
                click: () => { }
            }, {
                label: 'продолжать',
                className: 'text-red-500',
                click: confirmData.action
            }
        ]}>
            <div className="text-center font-bold pt-8">
                {confirmData.title}
            </div>
            <div className="text-center p-4">
                {confirmData.message}
            </div>
        </Confirm>
    </>
    )
}

export default ShowRole;