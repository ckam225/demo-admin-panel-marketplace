import React from 'react'
import Modal from '../../../components/controls/Modal';
import SearchBox from '../../../components/controls/SearchBox';
import usePermissions from '../../../store/actions/permissions';
import CreatePermissionModal from './CreatePermissionModal';

const PermissionPicker = ({ refKey, olders = [], onSelected }) => {
    const modal = React.useRef(null);
    const formModal = React.useRef(null);
    const { permissions, loading, updatePermissionList } = usePermissions()
    const [mutablePerms, setMutablePerms] = React.useState([])
    const [selected, setSelected] = React.useState([])

    React.useImperativeHandle(refKey, () => ({
        show() {
            modal.current.open();
        },
        close() {
            modal.current.close();
        },
    }));

    React.useEffect(() => {
        const perms = permissions.filter(not_in_olders)
        setMutablePerms(perms)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [permissions])


    function not_in_olders(perm) {
        return olders.findIndex(o => o.id === perm.id) === -1
    }

    function handleCreated(perm) {
        updatePermissionList(perm)
    }

    function handleClose() {
        if (onSelected)
            onSelected(selected)
        modal.current.close();
    }

    function handleCheck({ target: { value, checked } }) {
        if (value) {
            const tPerm = mutablePerms.find(p => parseInt(p.id) === parseInt(value))
            if (checked) {
                setSelected([...selected, tPerm])
            }
            else {
                setSelected(selected.filter(p => p.id !== tPerm.id))
            }
        }
    }

    return (
        <>
            <Modal refId={modal} style={{ width: '600px' }}>
                <Modal.Header>
                    <div className="flex items-center w-full pr-2">
                        <span>Select permission</span>
                        <button onClick={() => formModal.current.show()} className="button py-1 ml-5">create</button>
                    </div>
                </Modal.Header>
                <Modal.Body className="h-2/4">
                    <div className="flex flex-col px-2 py-4">
                        <SearchBox />
                        {loading ? <div>Loading...</div> :
                            <table className="table flex-none bg-white">
                                <tbody>
                                    {mutablePerms.length > 0 && mutablePerms.map(perm => {
                                        return <tr key={perm.id} onClick={handleCheck} className="focus:cursor-pointer">
                                            <td><input type="checkbox" className="p-0" value={perm.id} /></td>
                                            <td className="text-left">{perm.name}</td>
                                            <td>{perm.description}</td>
                                        </tr>
                                    })}
                                </tbody>
                            </table>
                        }

                    </div>
                    {selected.length > 0 && <div className="flex justify-end py-2 px-2 bg-blue-50">
                        <button className="button" onClick={handleClose}>Выбрать</button>
                    </div>}
                </Modal.Body>
            </Modal>

            <CreatePermissionModal refKey={formModal} onCreated={handleCreated} />
        </>
    );
}

export default PermissionPicker;