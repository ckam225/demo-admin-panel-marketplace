
import React from 'react'
import { useRecoilValue } from 'recoil'
import Icon from '../../../components/controls/Icon'
import { userRoleState } from '../../../store/states/base'

const OwnPermissionList = ({ permissions = [], onRemove }) => {

    const { isSuperuser } = useRecoilValue(userRoleState)

    return <table className="table flex-none bg-white">
        <tbody>
            {permissions.length > 0 && permissions.map(perm => {
                return <tr key={perm.id}>
                    <td className="text-left">{perm.name}</td>
                    <td>{perm.description}</td>
                    <td>
                        {isSuperuser && <button type="button" className="pr-2 focus:outline-none" onClick={(e) => onRemove(perm)}>
                            <Icon name="trash" className="h-4 w-4 hover:text-red-600" />
                        </button>}
                    </td>
                </tr>
            })}
        </tbody>
    </table>
}

export default OwnPermissionList;