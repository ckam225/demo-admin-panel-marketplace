import React from 'react'
import Modal from '../../../components/controls/Modal';
import { usePermission } from '../../../store/actions/permissions';
import { useFormik } from "formik";
import { roleValidationRule } from '../../../hooks/validators'

const CreatePermissionModal = ({ refKey, onCreated }) => {
    const modal = React.useRef(null);
    const { createPermission, loading } = usePermission()

    const formik = useFormik({
        initialValues: {
            name: "",
            description: "",
        },
        validationSchema: roleValidationRule,
        onSubmit: async (values) => {
            const response = await createPermission(values)
            if (response) {
                if (onCreated)
                    onCreated(response)
                formik.resetForm()
                modal.current.close();
            }
        },
    })

    React.useImperativeHandle(refKey, () => ({
        show() {
            modal.current.open();
        },
        close() {
            modal.current.close();
        },
    }));

    return (
        <Modal refId={modal} style={{ width: '400px' }}>
            <Modal.Header>
                <span>New Permission</span>
            </Modal.Header>
            <Modal.Body>
                <form className="flex flex-col shadow-lg p-10" onSubmit={formik.handleSubmit}>
                    <div className="mt-5">
                        <label htmlFor="">Название</label>
                        <input type="text" name="name" className=""
                            value={formik.values.name}
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.name && formik.touched.name && (
                            <small className="text-red-500">{formik.errors.name}</small>
                        )}
                    </div>
                    <div className="mt-3">
                        <label htmlFor="">Описание</label>
                        <input type="text" name="description" className=""
                            value={formik.values.description}
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                        />
                        {formik.errors.description && formik.touched.description && (
                            <small className="text-red-500">{formik.errors.description}</small>
                        )}
                    </div>

                    <div className="flex mt-5">
                        {loading ? <div>wait...</div> :
                            <button type="submit" className="button w-full">Сохранить</button>
                        }
                    </div>
                </form>
            </Modal.Body>
        </Modal>
    );
}

export default CreatePermissionModal;