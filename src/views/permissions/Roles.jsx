import React from 'react'
import styled from 'styled-components'
import TableEmptyRow from '../../components/controls/TableEmptyRow'
import TableLoader from '../../components/controls/TableLoader'
import { useRoleList, useRole } from '../../store/actions/roles'
import ShowRole from './components/ShowRole'
import { userRoleState } from '../../store/states/base'
import { useRecoilValue } from 'recoil'
import { useFormik } from 'formik'
import { roleValidationRule } from '../../hooks/validators'
import { DateTime } from 'luxon'

const Permissions = () => {
    const { roles, loading, updateRoleList } = useRoleList()
    const { createRole } = useRole()
    const [currentRole, setCurrentRole] = React.useState()
    const { isSuperuser, isAdmin } = useRecoilValue(userRoleState)

    const formik = useFormik({
        initialValues: {
            name: '',
            description: ''
        },
        validationSchema: roleValidationRule,
        onSubmit: async (values) => {
            const response = await createRole([values])
            if (response) {
                formik.resetForm()
            }
        }
    })

    function handleRoleClick(role) {
        setCurrentRole(role)
    }

    function handleUpdateRole(role) {
        updateRoleList(role)
    }

    function selectedItemClass(role) {
        return (currentRole && currentRole.id === role.id) ? 'active' : null
    }


    return <Wrapper>
        <div className="flex relative">
            <form onSubmit={formik.handleSubmit} className="flex flex-col flex-auto">
                <div className="card my-4 mx-3">
                    <div className="bg-white border-b border-gray-200 p-4 flex justify-between">
                        <h1 className="text-xl text-gray-500">Роли и доступ</h1>
                        <div className="flex">
                        </div>
                    </div>
                    <table className="table flex-none">
                        <thead>
                            <tr>
                                <th>Название</th>
                                <th>Значение</th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {(isSuperuser || isAdmin) && <tr>
                                <td >
                                    <div className="px-2">
                                        <input type="text" className="text-field" placeholder="Название"
                                            name="name"
                                            value={formik.values.name}
                                            onChange={formik.handleChange}
                                        />
                                        {formik.errors.name && formik.touched.name && (
                                            <small className="text-red-500">{formik.errors.name}</small>
                                        )}
                                    </div>
                                </td>
                                <td>
                                    <div className="px-2">
                                        <input type="text" className="text-field" placeholder="Описание"
                                            name="description"
                                            value={formik.values.description}
                                            onChange={formik.handleChange}
                                        />
                                        {formik.errors.description && formik.touched.description && (
                                            <small className="text-red-500">{formik.errors.description}</small>
                                        )}
                                    </div>
                                </td>
                                <td >
                                    <div className="px-2">
                                        {loading ? <div>...</div> : <button type="submit" className="mr-3 button ">Добавить</button>}
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>}
                            {loading ? <TableLoader cols={3} /> :
                                roles.length === 0 ? <TableEmptyRow colspan={3} className="text-center py-5">Нет данных</TableEmptyRow> :
                                    (roles.length > 0 && roles.map((role, i) => {
                                        return role.name !== 'owner' ? (
                                            <tr key={i} onClick={(e) => {
                                                e.stopPropagation()
                                                handleRoleClick(role)
                                            }} className={
                                                `cursor-pointer`
                                            }>
                                                {/* <td>{role.id}</td> */}
                                                <td className={`${selectedItemClass(role)}`} style={{ width: '300px' }}>{role.name}</td>
                                                <td className={`${selectedItemClass(role)}`} style={{ width: '300px' }}>{role.description}</td>
                                                <td className={`${selectedItemClass(role)}`}>{DateTime.fromISO(role.created_at).setLocale('ru').toFormat('dd LLL yyyy, hh:mm:ss')}</td>
                                                <td className={`${selectedItemClass(role)}`}></td>
                                                <td className={`${selectedItemClass(role)}`}></td>
                                            </tr>
                                        ) : null
                                    }))
                            }
                        </tbody>
                    </table>
                </div>
            </form>
            {currentRole && <div className="flex flex-col" style={{ width: '400px' }}>
                <ShowRole currentRole={currentRole} onUpdate={handleUpdateRole} />
            </div>}
        </div>
    </Wrapper>
}

const Wrapper = styled.div`
`

export default Permissions;