import React from 'react'
import MainLayout from '../../components/layouts/Main';
import { useRoleList } from '../../store/actions/roles'
import { usePermissions } from '../../store/actions/permissions'
import { useUser } from '../../store/actions/users'
import { createUserValidationRule } from '../../hooks/validators'
import { navigate } from '@reach/router'

import { useFormik } from "formik";
import { userRoleState } from '../../store/states/base';
import { useRecoilValue } from 'recoil';
import { BaseLink } from '../../components/shared/NavItem';
import Icon from '../../components/controls/Icon';

const CreateUserPage = () => {

    const { roles } = useRoleList()
    const { permissions } = usePermissions()
    const { loading, createUser } = useUser()
    const { isSuperuser } = useRecoilValue(userRoleState)

    const formik = useFormik({
        initialValues: {
            email: "",
            name: "",
            phone: "",
            role: '',
            permissions: []
        },
        validationSchema: createUserValidationRule,
        onSubmit: async (values) => {
            const user = await createUser(values)
            if (user.id) {
                navigate(`/users/${user.id}`)
            }
        },
    });


    function handlePermisions(event) {
        const { checked, value } = event.target;
        if (checked) {
            formik.setFieldValue("permissions", [...formik.values.permissions, value]);
        } else {
            formik.setFieldValue(
                "permissions",
                formik.values.permissions.filter((v) => v !== value)
            );
        }
    }

    return <MainLayout>
        <MainLayout.Head>

        </MainLayout.Head>
        <MainLayout.Container>
            <div className="flex flex-col w-full">
                <form className="card my-4 mx-3 flex flex-col" onSubmit={formik.handleSubmit}>
                    <div className="bg-white border-b border-gray-200 p-4 flex items-center">
                        <BaseLink to="/users" className="icon-button border-0 mr-3">
                            <Icon name="arrow_left" />
                        </BaseLink>
                        <h1 className="text-xl text-gray-700">Новый пользователь</h1>
                    </div>
                    <div className="flex" >
                        <div className="flex flex-col w-1/3 p-5">
                            <div className="flex flex-col mt-2 ">
                                <label htmlFor="" className="mr-2 text-gray-600">Роль</label>
                                <select
                                    className=" p-2  flex-auto "
                                    name="role"
                                    value={formik.values.role}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                >
                                    <option value="" className="text-gray-300" disabled>--  Выберите ролей  --</option>
                                    {roles.length > 0 && roles.map(role => {
                                        return (<option value={role.name} key={role.id}>{role.description}</option>)
                                    })}

                                </select>
                                {formik.errors.role && formik.touched.role && (
                                    <small className="text-red-500">{formik.errors.role}</small>
                                )}
                            </div>
                            <div className="flex flex-col mt-2">
                                <label htmlFor="" className="mr-2 text-gray-600">Имя</label>
                                <input
                                    type="text"
                                    className="border p-2   flex-auto "
                                    name="name"
                                    value={formik.values.name}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                />
                                {formik.errors.name && formik.touched.name && (
                                    <small className="text-red-500">{formik.errors.name}</small>
                                )}
                            </div>
                            <div className="flex flex-col mt-2">
                                <label htmlFor="" className="mr-2 text-gray-600">Электроная почта</label>
                                <input
                                    type="email"
                                    className="  flex-auto "
                                    name="email"
                                    value={formik.values.email}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                />
                                {formik.errors.email && formik.touched.email && (
                                    <small className="text-red-500">{formik.errors.email}</small>
                                )}
                            </div>
                            <div className="flex flex-col mt-2">
                                <label htmlFor="" className="mr-2 text-gray-600">Номер телефона</label>
                                <input
                                    type="text"
                                    className="flex-auto "
                                    name="phone"
                                    value={formik.values.phone}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                />
                                {formik.errors.phone && formik.touched.phone && (
                                    <small className="text-red-500">{formik.errors.phone}</small>
                                )}
                            </div>
                        </div>

                        {isSuperuser && <div className="pb-5 flex-none flex flex-col mt-5 shadow-lg border px-8 border-gray-300">
                            <label htmlFor="" className="text-gray-400 mb-3">-- permissions --</label>
                            {permissions.map(perm => <label htmlFor="" key={perm.id}>
                                <input type="checkbox" className="mr-2"
                                    value={perm.id}
                                    name="permission"
                                    onChange={handlePermisions}
                                />
                                <span>{perm.name}</span>
                            </label>)}
                        </div>}
                    </div>
                    <div className="flex border-t bg-gray-50 py-2 px-4 mt-5">
                        <span></span>
                        {loading ? <div>Loading...</div> : <button
                            type="submit"
                            className="button rounded-sm"
                        >
                            Добавить
                        </button>}
                    </div>
                </form>
            </div >
        </MainLayout.Container>
    </MainLayout>
}

export default CreateUserPage;