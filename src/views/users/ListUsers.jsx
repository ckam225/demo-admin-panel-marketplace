import React from 'react'
import MainLayout from '../../components/layouts/Main'
import { BaseLink } from '../../components/shared/NavItem'
import { useUser } from '../../store/actions/users'
import Icon from '../../components/controls/Icon'
import UserTableRow from './components/UserTableRow'
import TableEmptyRow from '../../components/controls/TableEmptyRow'
import TableLoader from '../../components/controls/TableLoader'
import { userRoleState } from '../../store/states/base'
import { useRecoilValue } from 'recoil'
import Confirm from '../../components/controls/Confirm'
import Permissions from '../permissions/Roles'
import Modal from '../../components/controls/Modal'


const ListUsersView = () => {
    const { loading, users, fetchUsers } = useUser()
    const { updateUser } = useUser()
    const { isSuperuser, isAdmin } = useRecoilValue(userRoleState)
    const confirmBox = React.useRef()
    const permModal = React.useRef()
    const [confirmData, setConfirmData] = React.useState({
        title: '',
        message: '',
        action: () => { }
    })

    // eslint-disable-next-line react-hooks/exhaustive-deps
    React.useEffect(async () => {
        await fetchUsers()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const handleBlock = (user = { is_blocked: false }) => {
        setConfirmData({
            title: "Уверены ли вы?",
            message: `Вы действительно хотите ${user.is_blocked ? 'Разблокировать' : 'Блокировать'} этого пользователя?`,
            action: async () => {
                await updateUser(user.id, { ...user, is_blocked: !user.is_blocked })
                await fetchUsers()
            }
        })
        confirmBox.current.open()
    }

    return <>
        <MainLayout>
            <MainLayout.Head>
                <title>Пользователя</title>
            </MainLayout.Head>
            <MainLayout.Container>
                <div className="">
                    <div className="card my-4 mx-3">
                        <div className="bg-white border-b border-gray-200 p-4 flex justify-between">
                            <h1 className="text-xl text-gray-500">Пользователя</h1>
                            <div className="flex">
                                <button click="fetchUsers" onClick={() => permModal.current.open()}
                                    className="button mx-3 flex items-center px-3 "
                                    title="Роли и доступ">
                                    <Icon name="lock" className=" mr-2" size="18" />
                                    <span className="text-xs">Роли и доступ</span>
                                </button>
                                {(isSuperuser || isAdmin) && <>
                                    <BaseLink
                                        to="/users/create"
                                        className="button  flex items-center"
                                    >
                                        <Icon className="mr-2" size="18" name="plus" />
                                        <span className="text-xs">добавить</span>
                                    </BaseLink>
                                    <BaseLink
                                        to="/users/create-test"
                                        className="button  flex items-center"
                                    >
                                        <Icon className="mr-2" size="18" name="plus" />
                                        <span className="text-xs">добавить тестового</span>
                                    </BaseLink>
                                </>
                                }
                            </div>
                        </div>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th scope="col">Роль</th>
                                    <th scope="col">Имя</th>
                                    <th scope="col">Емейл</th>
                                    {/* <th scope="col">Проверена</th>  */}
                                    <th scope="col">Номер телефона</th>
                                    <th scope="col">Статус</th>
                                    <th scope="col">Создано</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {loading ? <TableLoader cols={7} /> :
                                    users.length === 0 ? <TableEmptyRow colspan={7} className="text-center py-5">Нет пользователей</TableEmptyRow> :
                                        (users.map(user => <UserTableRow key={user.id} user={user} onBlocked={() => handleBlock(user)} />))
                                }
                            </tbody>
                        </table>
                    </div>
                </div>

            </MainLayout.Container>
        </MainLayout>
        <Confirm refId={confirmBox} actions={[
            {
                label: 'отменить',
                click: () => { }
            }, {
                label: 'продолжать',
                className: 'text-red-500',
                click: confirmData.action
            }
        ]}>
            <div className="text-center font-bold pt-8">
                {confirmData.title}
            </div>
            <div className="text-center p-4">
                {confirmData.message}
            </div>
        </Confirm>

        <Modal refId={permModal} style={{ width: '850px' }}>
            <Modal.Header>Роли и доступ</Modal.Header>
            <Modal.Body>
                <Permissions />
            </Modal.Body>
        </Modal>
    </>
}

export default ListUsersView
