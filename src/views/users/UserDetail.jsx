import React, { Fragment } from 'react'
import { useRecoilValue } from 'recoil'
import Icon from '../../components/controls/Icon'
import useUser from '../../store/actions/users'
import { userRoleState } from '../../store/states/base'
import styled from 'styled-components'
import { navigate } from '@reach/router'
import Tab from '../../components/controls/Tab'
import MainLayout from '../../components/layouts/Main'
import useRoleList from '../../store/actions/roles'
import PermissionPicker from '../permissions/components/PermissionPicker'
import Confirm from '../../components/controls/Confirm'
import { Popover, Transition } from '@headlessui/react'
import Loader from '../../components/controls/Loader'
import { BaseLink } from '../../components/shared/NavItem'
import { DateTime } from 'luxon'

const UserDetail = ({ userId }) => {
    const permisModal = React.useRef()
    const confirmBox = React.useRef()
    const { roles } = useRoleList()
    const { isSuperuser } = useRecoilValue(userRoleState)
    const { loading, fetchAnyUser, updateUser, deleteUser, getUserStatus } = useUser()
    const [isUserDataChanged, setIsUserDataChanged] = React.useState(false)
    const [user, setUser] = React.useState({
        permissions: [],
        role: {}
    })
    const [confirmData, setConfirmData] = React.useState({
        title: '',
        message: '',
        action: () => { }
    })


    React.useEffect(() => {
        (async () => {
            const data = await fetchAnyUser(userId)
            const rPerms = data?.role?.permissions || []
            const uPerms = data.permissions || []
            setUser({
                ...data,
                permissions: rPerms.concat(uPerms)
            })

        })()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [userId])


    const handleRoleChange = (role) => {
        setUser({ ...user, role: role })
        setIsUserDataChanged(true)
    }

    const handleSelectPermission = (perm) => {
        const newPerms = user.permissions;
        if (user.permissions.findIndex(p => p.id === perm.id) === -1) {
            newPerms.push(perm)
            setUser({ ...user, permissions: newPerms })
            setIsUserDataChanged(true)
        }
    }

    const handleRemovePermission = (perm) => {
        const newPerms = user.permissions.filter(p => p.id !== perm.id)
        setUser({ ...user, permissions: newPerms })
        setIsUserDataChanged(true)
    }

    const handleUpdate = async () => {
        await updateUser(user.id, user)
    }

    const handleBlock = () => {
        setConfirmData({
            title: "Уверены ли вы?",
            message: `Вы действительно хотите ${user.is_blocked ? 'Разблокировать' : 'Блокировать'} этого пользователя?`,
            action: async () => {
                const tmpUser = { ...user, is_blocked: !user.is_blocked }
                await updateUser(user.id, tmpUser)
                setUser(tmpUser)
            }
        })
        confirmBox.current.open()

    }

    const handleRemove = async () => {
        setConfirmData({
            title: "Уверены ли вы?",
            message: 'Вы действительно хотите Удалить это? Удаление данных, это действие необратимо',
            action: async () => {
                const success = await deleteUser(user.id)
                if (success) {
                    navigate('/users')
                }
            }
        })
        confirmBox.current.open()
    }

    return <>
        <MainLayout>
            <MainLayout.Head>

            </MainLayout.Head>
            <MainLayout.Container>
                {loading ? <div className="flex bg-white p-10 items-center justify-center w-full">
                    <Loader />
                </div> : <Wrapper>
                    <div className="bg-white shadow-sm flex justify-between mx-5 my-4 p-5">
                        <div className="flex items-center">
                            <BaseLink to="/users" className="icon-button border-0 mr-3">
                                <Icon name="arrow_left" />
                            </BaseLink>
                            <div className="w-20 h-20 rounded-full bg-gray-200 flex items-center justify-center">
                                <Icon name="user" />
                            </div>
                            <div className="flex flex-col mx-4">
                                <div className="relative w-60">
                                    <span className="text-3xl">{user.name}</span>
                                    <span
                                        className={`absolute top-0 right-0 rounded-full bg-${getUserStatus().color}-500 text-white text-xs px-1`}
                                    >{getUserStatus().title}</span>
                                </div>
                                <div className="text-gray-500">
                                    <a href={`mailto:${user.email}`}>{user.email}</a>
                                </div>
                                {user.phone && <div className="text-gray-500" >
                                    <a href={`tel:${user.phone}`}>{user.phone}</a>
                                </div>}
                                <div className="flex items-center">
                                    <div className={[`w-3 h-3 rounded-full`, getUserStatus().color].join(' ')}></div>
                                    <div className="ml-1">{getUserStatus().title}</div>
                                </div>
                            </div>
                        </div>
                        {isSuperuser && <div className="flex items-center">
                            {user.role && <Popover className="relative">
                                <Popover.Button className="flex items-center justify-between focus:outline-none py-3  rounded-lg w-full pl-0">
                                    <div className="flex items-center ">
                                        <div className="w-9 h-9 rounded-full  bg-blue-600 flex items-center justify-center  mr-2">
                                            <Icon name="lock" className="text-white" size={18} />
                                        </div>
                                        <span>{user.role?.name}</span>
                                    </div>
                                    <Icon name="chevron_down" className="h-3 w-3 mr-2" />
                                </Popover.Button>
                                <Transition
                                    as={Fragment}
                                    enter="transition ease-out duration-200"
                                    enterFrom="opacity-0 translate-y-1"
                                    enterTo="opacity-100 translate-y-0"
                                    leave="transition ease-in duration-150"
                                    leaveFrom="opacity-100 translate-y-0"
                                    leaveTo="opacity-0 translate-y-1"
                                >
                                    <Popover.Panel className="absolute z-50 right-0 shadow-lg border  bg-white">
                                        <div className="flex flex-col">
                                            {roles.length > 0 && roles.map(role => {
                                                return <div className={`flex w-full items-center cursor-pointer hover:bg-blue-50 p-2  
                                                ${role.name === user?.role?.name ? 'bg-blue-50' : ''}`} key={role.id} onClick={() => handleRoleChange(role)}>
                                                    <div className="w-9 h-9 rounded-full  bg-blue-400 flex items-center justify-center">
                                                        <Icon name="lock" className="text-white" />
                                                    </div>
                                                    <div className="flex flex-col ml-2">
                                                        <span className="leading-3">{role.name}</span>
                                                        <span className="text-gray-400">{role.description}</span>
                                                    </div>
                                                </div>
                                            })}
                                        </div>
                                    </Popover.Panel>

                                </Transition>
                            </Popover>}

                            {/* <button className="mr-3 rounded-full button">{user.is_blocked ? 'Разблокировать' : 'Блокировать'}</button>
                    <button className="mr-3 rounded-full button" color="red" > Удалить</button > */}
                        </div >}
                    </div>

                    {/* < !--permissions --> */}
                    <Tab>
                        <Tab.Headers className="flex mx-5">
                            <Tab.Header tab={0} className="px-4 py-2 cursor-pointer" activeClass="active">Profile</Tab.Header>
                            <Tab.Header tab={1} className="px-4 py-2 cursor-pointer" activeClass="active">Privileges</Tab.Header>
                        </Tab.Headers>
                        <Tab.Content className="bg-white mx-5">
                            <Tab.Item tab={0}>
                                <table className="table no-hover">
                                    <tbody>
                                        <tr>
                                            <td>Email</td>
                                            <td>{user.email}</td>
                                        </tr>
                                        <tr>
                                            <td>Phone</td>
                                            <td>{user.phone}</td>
                                        </tr>
                                        <tr>
                                            <td>Verified At</td>
                                            <td>{user.email_verified_at}</td>
                                        </tr>
                                        <tr>
                                            <td>Created At</td>
                                            <td>{DateTime.fromISO(user.created_at).setLocale('ru').toFormat('dd LLL yyyy')}</td>
                                        </tr>
                                        <tr>
                                            <td>Updated At</td>
                                            <td>{DateTime.fromISO(user.updated_at).setLocale('ru').toRelative()}</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </Tab.Item>
                            <Tab.Item tab={1}>
                                {/* user permissions */}
                                <div className="mx-5 p-5 flex flex-col w-2/3" style={{ minHeight: '50vh' }}>
                                    <div>
                                        {user?.permissions.map(perm => <div
                                            className="flex items-center float-left m-1 px-2 bg-green-300 text-gray-900 rounded-full"
                                            key={perm.id}
                                            title={perm.description}
                                        >
                                            {/* <!-- <span className="mr-1 text-gray-500 cursor-pointer">x</span> --> */}
                                            {perm.name}
                                            <button className="icon-button border-0 hover:text-red-500"
                                                onClick={() => handleRemovePermission(perm)}>
                                                <Icon name="trash" size={16} />
                                            </button>
                                        </div>)}
                                    </div>
                                </div>
                            </Tab.Item>
                        </Tab.Content>
                    </Tab>
                    {user && <div className="bg-white shadow-sm flex justify-end mx-5 my-4 p-5">
                        {isSuperuser && <button className="button" onClick={() => permisModal.current.show()}>Добавить разришения</button>}

                        {isSuperuser && <button className="button" onClick={handleBlock}>{user.is_blocked ? 'Разблокировать' : 'Блокировать'}</button>}
                        {isSuperuser && <button className="button btn-danger" onClick={handleRemove}>Удалить</button>}
                        {isUserDataChanged && <button className="button" onClick={handleUpdate}> Сохранить</button>}
                    </div>}
                </Wrapper>}

            </MainLayout.Container>
        </MainLayout>
        <PermissionPicker refKey={permisModal} onSelected={handleSelectPermission} />
        <Confirm refId={confirmBox} actions={[
            {
                label: 'отменить',
                click: () => { }
            }, {
                label: 'продолжать',
                className: 'text-red-500',
                click: confirmData.action
            }
        ]}>
            <div className="text-center font-bold pt-8">
                {confirmData.title}
            </div>
            <div className="text-center p-4">
                {confirmData.message}
            </div>
        </Confirm>
    </>
}

const Wrapper = styled.div`
.active{
    background: #fff;
    border-radius: 10px 10px 0 0;
}
.active:first-child{
    border-radius: 0 10px 0 0;
}
`
export default UserDetail;