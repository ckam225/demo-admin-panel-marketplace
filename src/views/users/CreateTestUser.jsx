import MainLayout from "../../components/layouts/Main";
import { useTestUser } from '../../store/actions/users'
import { useFormik } from "formik";
import { createTestUserValidationRule } from "../../hooks/validators";
import { BaseLink } from "../../components/shared/NavItem";
import Icon from "../../components/controls/Icon";
import React from 'react'

const CreateTestUserPage = () => {

    const { loading, createUser } = useTestUser()

    const formik = useFormik({
        initialValues: {
            email: null,
            name: null,
            count: 1,
            password: null,
        },
        validationSchema: createTestUserValidationRule,
        onSubmit: async (values) => {
            if (values.count > 1) {
                delete values.email;
                delete values.password;
            }
            const response = await createUser(values)
            if (response.status === 'success') {
                document.getElementById('create-dev-user-form').reset()
            }
        },
    });

    return <MainLayout>
        <MainLayout.Head>

        </MainLayout.Head>
        <MainLayout.Container>
            <div className="flex flex-col w-full">
                <form id="create-dev-user-form" className="card my-4 mx-3 flex flex-col" onSubmit={formik.handleSubmit}>
                    <div className="bg-white border-b border-gray-200 p-4 flex items-center">
                        <BaseLink to="/users" className="icon-button border-0 mr-3">
                            <Icon name="arrow_left" />
                        </BaseLink>
                        <h1 className="text-xl text-gray-700">Новый тестовой пользователь</h1>
                    </div>
                    <div className="flex" >
                        <div className="flex flex-col w-1/3 p-5">
                            
                            <div className="flex flex-col mt-2">
                                <label htmlFor="" className="mr-2 text-gray-600">Имя</label>
                                <input
                                    type="text"
                                    className="border p-2   flex-auto "
                                    name="name"
                                    value={formik.values.name}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                />
                                {formik.errors.name && formik.touched.name && (
                                    <small className="text-red-500">{formik.errors.name}</small>
                                )}
                            </div>
                            {formik.values.count <= 1 && <div className="flex flex-col mt-2">
                                <label htmlFor="" className="mr-2 text-gray-600">Электроная почта</label>
                                <input
                                    type="email"
                                    className="  flex-auto "
                                    name="email"
                                    value={formik.values.email}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    disabled={formik.values.count > 1}
                                />
                                {formik.errors.email && formik.touched.email && (
                                    <small className="text-red-500">{formik.errors.email}</small>
                                )}
                            </div>}
                            {formik.values.count <= 1 && <div className="flex flex-col mt-2">
                                <label htmlFor="" className="mr-2 text-gray-600">Пароль</label>
                                <input
                                    type="password"
                                    className="  flex-auto "
                                    name="password"
                                    value={formik.values.password}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                    disabled={formik.values.count > 1}
                                />
                                {formik.errors.password && formik.touched.password && (
                                    <small className="text-red-500">{formik.errors.password}</small>
                                )}
                            </div>}
                            <div className="flex flex-col mt-2">
                                <label htmlFor="" className="mr-2 text-gray-600">Количество пользователей</label>
                                <input
                                    type="number"
                                    className="  flex-auto "
                                    name="count"
                                    value={formik.values.count}
                                    onBlur={formik.handleBlur}
                                    onChange={formik.handleChange}
                                />
                                {formik.errors.email && formik.touched.email && (
                                    <small className="text-red-500">{formik.errors.email}</small>
                                )}
                            </div>
                            {/*<div className="flex flex-col mt-2">*/}
                            {/*<label htmlFor="" className="mr-2 text-gray-600">Номер телефона</label>*/}
                            {/*<input*/}
                            {/*type="text"*/}
                            {/*className="flex-auto "*/}
                            {/*name="phone"*/}
                            {/*value={formik.values.phone}*/}
                            {/*onBlur={formik.handleBlur}*/}
                            {/*onChange={formik.handleChange}*/}
                            {/*/>*/}
                            {/*{formik.errors.phone && formik.touched.phone && (*/}
                            {/*<small className="text-red-500">{formik.errors.phone}</small>*/}
                            {/*)}*/}
                            {/*</div>*/}
                        </div>

                        {/*{isSuperuser && <div className="pb-5 flex-none flex flex-col mt-5 shadow-lg border px-8 border-gray-300">*/}
                        {/*<label htmlFor="" className="text-gray-400 mb-3">-- permissions --</label>*/}
                        {/*{permissions.map(perm => <label htmlFor="" key={perm.id}>*/}
                        {/*<input type="checkbox" className="mr-2"*/}
                        {/*value={perm.id}*/}
                        {/*name="permission"*/}
                        {/*onChange={handlePermisions}*/}
                        {/*/>*/}
                        {/*<span>{perm.name}</span>*/}
                        {/*</label>)}*/}
                        {/*</div>}*/}
                    </div>
                    <div className="flex border-t bg-gray-50 py-2 px-4 mt-5">
                        <span></span>
                        {loading ? <div>Loading...</div> : <button
                            type="submit"
                            className="button"
                        >
                            Создать
                        </button>}
                    </div>
                </form>
            </div >
        </MainLayout.Container>
    </MainLayout>
}

export default CreateTestUserPage;
