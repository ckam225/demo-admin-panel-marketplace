import React from 'react'
import { Menu, Transition } from '@headlessui/react'
import useAuthUser from '../../../store/actions/auth'
import { navigate } from '@reach/router'
import Icon from '../../../components/controls/Icon'
import { BaseLink } from '../../../components/shared/NavItem'



const AuthUserItem = ({ children, className, buttonClass }) => {
    const { user, logout } = useAuthUser()

    async function handleSignout() {
        const success = await logout()
        if (success) {
            navigate('/login')
        }
    }

    return (<Menu>
        {({ open }) => (
            <>
                <Menu.Button className={["transition duration-150 ease-in-out  focus:outline-none", buttonClass].join(' ')}>
                    {children}
                </Menu.Button>

                <Transition
                    show={open}
                    enter="transition ease-out duration-100"
                    enterFrom="transform opacity-0 scale-95"
                    enterTo="transform opacity-100 scale-100"
                    leave="transition ease-in duration-75"
                    leaveFrom="transform opacity-100 scale-100"
                    leaveTo="transform opacity-0 scale-95"
                >
                    <Menu.Items
                        static
                        className={["dp absolute right-0 w-56 bg-white border border-gray-200 divide-y divide-gray-100 rounded-md shadow-lg outline-none", className].join(' ')}
                    >
                        <div className="px-4 py-3">
                            {/* <p className="text-sm leading-5">Signed in as</p> */}
                            <p className="text-sm font-medium leading-5 text-blue-400 truncate">
                                {user?.name}
                            </p>
                        </div>

                        <div className="py-1">
                            <Menu.Item>
                                {({ active }) => (
                                    <BaseLink
                                        to="/settings/profile"
                                        className={`${active
                                            ? "bg-gray-100 text-gray-900"
                                            : "text-gray-700"
                                            } flex justify-between w-full px-4 py-2 text-sm leading-5 text-left`}
                                    >
                                        Настройки аккаунта
                                    </BaseLink>
                                )}
                            </Menu.Item>
                        </div>

                        <div className="py-1">
                            <Menu.Item>
                                {({ active }) => (
                                    <a
                                        href="#sign-out"
                                        onClick={handleSignout}
                                        className={`${active
                                            ? "bg-gray-100 text-gray-900"
                                            : "text-gray-700"
                                            } flex justify-between w-full px-4 py-2 text-sm leading-5 text-left`}
                                    >
                                        Выйти
                                    </a>
                                )}
                            </Menu.Item>
                        </div>
                    </Menu.Items>
                </Transition>
            </>
        )}
    </Menu>
    )
}


export default AuthUserItem