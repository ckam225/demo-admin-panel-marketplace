import React from 'react'
import { useRecoilValue } from 'recoil'
import Dropdown from '../../../components/controls/Dropdown'
import Icon from '../../../components/controls/Icon'
import { BaseLink } from '../../../components/shared/NavItem'
import { userRoleState } from '../../../store/states/base'
import { DateTime } from 'luxon'

const UserTableRow = ({ user, onBlocked = null }) => {

    const { isSuperuser } = useRecoilValue(userRoleState)

    const state = () => {
        if (user.is_blocked) return 'bg-red-500'
        if (user.is_active) return 'bg-green-500'
        return 'bg-yellow-500'
    }
    const classes = [`w-2 h-2 rounded-full`, state()]

    function handlerBlock() {
        if (onBlocked)
            onBlocked()
    }

    return (
        <tr >
            <td>
                {user.role && <span >{user.role.description}</span>}
            </td>
            <td>{user.name}</td>
            <td><BaseLink to={`/users/${user.id}`}>{user.email}</BaseLink></td>
            {/* <td>{user.email_verified_at}</td> */}
            <td>{user.phone}</td>
            <td>
                <div className={classes.join(' ')}></div>
            </td>
            <td>{DateTime.fromISO(user.created_at).setLocale('ru').toFormat('dd LLL yyyy, hh:mm')}</td>
            <td>
                <Dropdown className="relative">
                    <Dropdown.Toggle>
                        <button className="inline-flex items-center justify-center  rounded-md border border-gray-300 shadow-sm w-8 h-8 bg-white text-sm font-medium text-gray-700 focus:outline-none focus:ring-1 focus:ring-offset-gray-500 focus:ring-blue-accent">
                            <Icon name="dotv" className="w-6 h-6 text-gray-600" />
                        </button>
                    </Dropdown.Toggle>
                    <Dropdown.Content className="dp absolute mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none right-0 top-5">
                        <Dropdown.Item>
                            <BaseLink className="dropdown-item py-3" to={`/users/${user.id}`}>Более</BaseLink>
                        </Dropdown.Item>
                        {isSuperuser && <Dropdown.Item className="dropdown-item py-3" onClick={handlerBlock}>
                            {user.is_blocked ? 'Разблокировать' : 'Блокировать'}
                        </Dropdown.Item>}
                    </Dropdown.Content>
                </Dropdown>
            </td>
        </tr>
    )
}


export default UserTableRow