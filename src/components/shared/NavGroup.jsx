import React from 'react'

const NavGroup = ({ children }) => {
    return (
        <div className="text-gray-600 ml-5 text-xs">
            { children}
        </div>
    )
}

export default NavGroup