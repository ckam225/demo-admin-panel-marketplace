import React from 'react'
import Icon from '../controls/Icon';
import Dropdown from '../controls/Dropdown';
import { useRecoilState } from "recoil";
import { hideSidebarState } from "../../store/states/base";
import { useAuthUser } from '../../store/actions/auth'
import { BaseLink } from './NavItem';
import logo from '../../assets/logo.svg'
import { navigate } from '@reach/router';


const Topbar = () => {
    const { user, logout } = useAuthUser()
    const [sidebarHidden, setSidebarVisible] = useRecoilState(hideSidebarState);

    function handlerToggleSidebar() {
        setSidebarVisible(!sidebarHidden)
    }

    async function handleSignout() {
        const success = await logout()
        if (success) {
            navigate('/login')
        }
    }

    return (
        <div
            className="flex p-2 items-center justify-between h-14 border-b border-gray-200 bg-white shadow-sm"
        >
            <div className="flex">
                <button className="mr-4 focus:outline-none" onClick={handlerToggleSidebar}>
                    <Icon name="menu" className="w-6 h-6 text-gray-600" />
                </button>
                {sidebarHidden && <img src={logo} width="48" alt="logo" />}
            </div>
            {/* <breadcrumb /> */}

            <div className="flex flex-auto justify-end h-full">
                <div className="relative group inline-block text-left">
                    {user && <Dropdown>
                        <Dropdown.Toggle>
                            <button className="flex items-center mr-2 focus:outline-none">
                                <span className="rounded-full bg-blue-fonce bg-opacity-80 flex p-1">
                                    <Icon name="user"
                                        className="w-4 h-4 text-white"
                                    />
                                </span>
                                <span className="px-2" v-if="guard">{user.name}</span>
                                <Icon name="chevron_down" className="h-3 w-3" />
                            </button>
                        </Dropdown.Toggle>
                        <Dropdown.Content
                            className="dp absolute mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none right-0 top-10">
                            <Dropdown.Item>
                                <BaseLink
                                    to="/settings/profile"
                                    className="dropdown-item px-4 py-2"
                                >
                                    Account settings
                                </BaseLink>
                            </Dropdown.Item>
                            <Dropdown.Item className="dropdown-item px-4 py-2"
                                onClick={handleSignout}
                            >
                                Sign out
                            </Dropdown.Item>
                        </Dropdown.Content>
                    </Dropdown>}

                </div>
            </div>
        </div >
    )
}

export default Topbar