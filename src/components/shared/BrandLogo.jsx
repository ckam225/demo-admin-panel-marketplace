import React from 'react'
import Logo from '../../assets/logo.svg'

const BrandLogo = ({ className = '' }) => {

    const classGroup = `flex flex-col items-center justify-center  ${className}`


    return (
        <div className={classGroup}>
            <img src={Logo} alt="logo" className="w-18" />
            <div className="text-xl text-white">Маркетплейс</div>
            <span className="text-gray-600">админка</span>
        </div>
    )

}

export default BrandLogo