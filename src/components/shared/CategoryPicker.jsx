import React, { useEffect, useRef, useState } from 'react';
import { fetchAllCategoriesHasProps } from '../../api/categories';
import { toast } from 'react-toastify';
import Loader from '../controls/Loader';
import Modal from '../controls/Modal';
import Paginator from '../controls/Paginator';
import SearchBox from '../controls/SearchBox';
import TableEmptyRow from '../controls/TableEmptyRow';

const CategoryPicker = ({ refKey, onSelected, zIndex = 21 }) => {

    const modal = useRef()
    const [loading, setLoading] = useState(false)
    const [categories, setCategories] = useState([])
    const [page, setPage] = React.useState(1)
    // const [perPage, setPerPage] = React.useState(30)
    const [search, setSearch] = React.useState('')
    const [pageCount, setPageCount] = useState(0)

    React.useImperativeHandle(refKey, () => ({
        open() {
            modal.current.open();
        },
        close() {
            modal.current.close();
        },
    }));

    const query = React.useMemo(() => {
        let q = `?page=${page}&per_page=30`
        if (search.length > 0)
            q = q + "&name=" + search
        return q
    }, [search, page])


    useEffect(() => {
        (async () => {
            setLoading(true)
            const res = await fetchAllCategoriesHasProps(query)
            if (!res?.error) {
                setCategories(res.data?.data || [])
                setPageCount(res.data?.pageCount || 0)
            } else {
                toast.error('error fetch')
            }
            setLoading(false)
        })()
    }, [query])

    const handleSearchChange = (s) => {
        setSearch(s)
    }


    return <Modal refId={modal} style={{ width: '400px' }} zIndex={zIndex}>
        <Modal.Header>
            <div className="flex items-center w-full pr-2">
                <span>Выбрать категорию</span>
            </div>
        </Modal.Header>
        <Modal.Body>
            <div className="m-2">
                <SearchBox onChange={handleSearchChange} />
            </div>
            <div style={{ maxHeight: '450px', overflowY: 'auto' }}>
                <table className="table bg-white">
                    <tbody>
                        {loading ? <TableEmptyRow colspan={2} className="flex justify-center  py-5 h-96">
                            <Loader />
                        </TableEmptyRow> :
                            categories.map(cate => {
                                return <tr key={cate.id} className="focus:cursor-pointer">
                                    <td className="pl-1">{cate.id}</td>
                                    <td className="text-left" onClick={() => onSelected(cate)}>
                                        {cate.name}
                                        <div className="text-gray-400">{cate?.parent?.name}</div>
                                    </td>
                                </tr>
                            })}
                    </tbody>
                </table>
            </div>
            <div className="py-2 flex justify-between items-center bg-gray-50">
                {(categories.length > 0 && pageCount > 1) ? <Paginator className="text-blue-accent"
                    itemClass=" text-xs rounded-full mx-1 w-4 h-4 focus:outline-none"
                    activeItemClass="pagin-item-color"
                    pages={pageCount}
                    setCurrentPage={setPage} /> : <div />}
            </div>
        </Modal.Body>
    </Modal>
}

export default CategoryPicker;