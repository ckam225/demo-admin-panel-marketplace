import React, { useMemo } from 'react';


function concatArrayValues(obj) {
    const p = []
    Object.values(obj).forEach(err => {
        if (typeof err === 'object') {
            p.push(concatArrayValues(err))
        } else {
            p.push(err)
        }
    })
    return p
}

function mergeFormikErrors(errors = {}) {
    let outputErrors = []
    if (typeof errors === 'object') {
        Object.values(errors).forEach(err => {
            if (typeof err === 'object') {
                outputErrors.push(...concatArrayValues(err))
            } else {
                outputErrors.push(err)
            }
        })
    }
    return outputErrors
}

const FormErrorHandler = ({ formik }) => {

    const formErrors = useMemo(() => {
        return mergeFormikErrors(formik.errors)
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [formik?.errors])


    return <>
        {!formik.isValid && <div className="border border-red-700 bg-red-100 m-3 p-3">
            {formErrors.map((err, i) => <li key={i} className="list-item ">{err}</li>)}
        </div>}
    </>
}

export default FormErrorHandler;