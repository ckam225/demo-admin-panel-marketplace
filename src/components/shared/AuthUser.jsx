import { navigate } from '@reach/router';
import React, { useEffect } from 'react';
import { endOt } from '../../helpers';
import useAuthUser from '../../store/actions/auth';
import Icon from '../controls/Icon';
import { BaseLink } from './NavItem';
import * as api from "../../api/notification";
import { toast } from "react-toastify";


const AuthUser = () => {
    const { user, logout } = useAuthUser()
    const [visible, setVisible] = React.useState(false)
    const [notificatonCount, setNotificatonCount] = React.useState(0)

    useEffect(() => {
        setTimeout(() => {
            window.Echo.private(`users.${user?.id}`)
            .notification((notification) => {
                setNotificatonCount(notification.notifications);
                toast.info(`У вас ${notification.notifications} новых уведомлений`);
            });

            api.getUnreadCountNotifications().then((response) => {
                setNotificatonCount(response.data['unread-count'])
            });
        }, 500)
       
    }, [user])

    async function handleSignout() {
        const success = await logout()
        if (success) {
            navigate('/login')
        }
    }

    const toggleVisibility = () => {
        setVisible(!visible)
    }

    return <div className="relative">
        {visible && <div className="shadow-xl rounded-md bg-white m-3" style={{ position: 'absolute', bottom: '50px', minWidth: '290px' }}>
            <div className="flex items-center  justify-between border-b p-4 ">
                <div className="flex items-center">
                    <span className="rounded-full h-10 w-10 flex items-center justify-center text-white bg-opacity-80 p-1 bg-blue-accent" style={{ background: '#0096c7' }}>
                        <Icon name="user" className="w-4 h-4 " />
                    </span>
                    <span className="ml-2">{endOt(user?.name, 25)}</span>
                </div>
                <button className="focus:outline-none focus:text-black " onClick={toggleVisibility}>
                    <Icon name="x" className="w-4 h-4 " />
                </button>
            </div>
            <ul>
                <li>
                    <BaseLink to="/settings/profile" className={`text-gray-700 flex justify-between w-full px-4 py-2 text-sm leading-5 text-left`}>
                        Настройки
                    </BaseLink>
                </li>
                <li>
                    <BaseLink to="/settings/notifications" className={`text-gray-700 flex justify-between w-full px-4 py-2 text-sm leading-5 text-left`}>
                        Уведомбления
                        <span className=" bg-red-400 rounded-lg text-xs text-white flex justify-center items-center p-1">
                            {notificatonCount}
                        </span>
                    </BaseLink>
                </li>
                <li>
                    <button className={`text-gray-700 flex justify-between w-full px-4 py-2 text-sm leading-5 text-left focus:outline-none`} onClick={handleSignout}>
                        Выити
                    </button>
                </li>
            </ul>
        </div>}
        <div className="flex justify-between items-center border-t border-white-10 px-2 py-4 ">
            <button className="relative flex items-center text-white px-1 focus:outline-none hover:text-blue-600" onClick={toggleVisibility}>
                <span className="rounded-full  bg-opacity-80 flex p-1 bg-blue-accent">
                    <Icon name="user" className="w-4 h-4 text-white" />
                </span>
                <Icon name="chevron_down" className="w-3 h-3 ml-1" />

                {notificatonCount > 0 && <span className=" bg-red-400 rounded-lg flex justify-center items-center" style={{
                    position: 'absolute',
                    top: "-8px",
                    right: "15px",
                    padding: '1px 3px',
                    fontSize: '8px'
                }}>
                    {notificatonCount}
                </span>}
            </button>

            <a href="https://mp.05.ru" className="focus:outline-none" title="переидти в ЛК" target="_blank" rel='noreferrer'>
                <Icon name="globe" className="w-4 h-4 mr-1 text-gray-500" />
            </a>



        </div>
    </div>
}

export default AuthUser;