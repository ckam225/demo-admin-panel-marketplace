import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { fetchAllBrands } from '../../api/brands';
import { toast } from 'react-toastify';
import Loader from '../controls/Loader';
import Modal from '../controls/Modal';
import SearchBox from '../controls/SearchBox';
import TableEmptyRow from '../controls/TableEmptyRow';

const BrandPicker = ({refKey, onSelected, zIndex=21}) => {

    const modal = useRef()
    const [loading, setLoading] = useState(false)
    const [brands, setBrands] = useState([])
    const [search, setSearch] = React.useState('')

    React.useImperativeHandle(refKey, () => ({
        open() {
            modal.current.open();
        },
        close() {
            modal.current.close();
        },
    }));

    const query = React.useMemo(() => {
        if (search.length > 0)
            return "?name=" + search
    }, [ search])

    useEffect(() => {
        (async () => {
            setLoading(true)
            const res = await fetchAllBrands(query)
            if(!res?.error){
                setBrands(res?.data || [])
            }else{
                toast.error('error fetch')
            }
            setLoading(false)
        })()
    }, [query])

    const handleSearchChange =(s) => {
        setSearch(s)
    }


    return  <Wrapper><Modal refId={modal} style={{ width: '400px' }} zIndex={zIndex}>
    <Modal.Header>
        <div className="flex items-center w-full pr-2">
            <span>Выбрать Бренд</span>
        </div>
    </Modal.Header>
    <Modal.Body>
       <div className="m-2">
        <SearchBox onChange={handleSearchChange}/>
       </div>
       <div style={{ maxHeight: '450px',  overflowY: 'auto' }}>
         <table className="table bg-white">
                <tbody>
                {loading ? <TableEmptyRow colspan={2} className="flex justify-center  py-5 h-96">
                    <Loader />
                </TableEmptyRow> : 
                    brands.map(brand => {
                        return <tr key={brand.id}  className="focus:cursor-pointer" onClick={() => onSelected(brand)}>
                            <td align="center">{brand.id}</td>
                            <td align="center">{brand?.name}</td>
                        </tr>
                    })}
                </tbody>
            </table>
       </div>
    </Modal.Body>
</Modal>
</Wrapper>
}
const Wrapper = styled.div`
table td{
   padding-left: 15px;
}
`
 
export default BrandPicker;