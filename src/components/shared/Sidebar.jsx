import React from 'react'
import BrandLogo from './BrandLogo'
import NavItem from './NavItem'
import Icon from '../controls/Icon'

import { useRecoilValue } from "recoil";
import { hideSidebarState } from "../../store/states/base";

import AuthUser from './AuthUser';

const Sidebar = () => {

    const toggled = useRecoilValue(hideSidebarState);

    const w = toggled ? 'w-0' : 'w-80'

    return (
        <div
            className={`sidebar ${w} bg-blue-fonce overflow-hidden flex-none transition-all ease-in-out duration-300 flex flex-col`}
        >
            <div className="py-5 bg-blue-900  bg-opacity-10">
                <BrandLogo className="h-28 "></BrandLogo>
            </div>
            <div className="flex flex-col flex-1 text-gray-400 justify-between overflow-y-auto" >

                <ul>
                    <NavItem to="/" exact>
                        <Icon className="w-6 h-6 mr-4" name="home" />
                        <span>Главная</span>
                    </NavItem>
                    <NavItem to="/stores">
                        <Icon className="w-6 h-6 mr-4" name="user_group" />
                        <span>Партнёры</span>
                    </NavItem>

                    <NavItem to="/products">
                        <Icon className="mr-4 w-6 h-6" name="shopping_cart" />
                        <span>Товары</span>
                    </NavItem>

                    <NavItem to="/moderation">
                        <Icon className="mr-4 w-6 h-6" name="check-circle" />
                        <span>Модерация</span>
                    </NavItem>

                    {/* <NavGroup>Синхронизация</NavGroup> */}
                    <NavItem to="/synchronization/">
                        <Icon className="mr-4 w-6 h-6" name="switch" />
                        <span>Синхронизация</span>
                    </NavItem>
                    <NavItem to="/users">
                        <Icon className="mr-4 w-6 h-6" name="users" />
                        <span>Пользователя  и доступ</span>
                    </NavItem>
                    <NavItem to="/settings">
                        <Icon className="mr-4 w-6 h-6" name="cog" />
                        <span>Настройки</span>
                    </NavItem>
                    <NavItem to="/others">
                        <Icon className="mr-4 w-6 h-6" name="view-list" />
                        <span>Еще</span>
                    </NavItem>

                </ul>
            </div>
            <AuthUser />

        </div >
    )
}

export default Sidebar
