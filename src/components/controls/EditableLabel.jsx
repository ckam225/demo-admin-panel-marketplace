import React, { useEffect, useRef, useState } from 'react';
import Icon from './Icon';

const EditableLabel = ({ value, type = "text", name, id, onChange, editable = true,className, ...rest }) => {
  const [isEditable, setEditable] = useState(false)
  const [isMouseOver, setIsMouseOver] = useState(false)
  const input = useRef()


  const handleBlur = ({ target }) => {
    if (target.value !== value) {
      onChange(target)
    }
    setEditable(false)
  }

  const handleEnableEdit = () => {
    setEditable(true)
  }

  const handleMouseOver = () => {
    setIsMouseOver(true)
  }

  const handleMouseLeave = () => {
    setIsMouseOver(false)
  }

  useEffect(() => {
    if (isEditable)
      input.current.focus()
  }, [isEditable])

  return <div className="flex relative" onMouseOver={handleMouseOver} onMouseLeave={handleMouseLeave}>
    <input type={type} id={id} name={name} ref={input} disabled={!isEditable}
      className={["textbox py-1",
        isEditable ? '' : 'border-transparent pl-0',
        className
      ].join(' ')}
      defaultValue={Object.assign(value)} onBlur={handleBlur} {...rest} />
    {(!isEditable && editable) && <button onClick={handleEnableEdit} className={["absolute top-0 bottom-0 right-0 ml-1 focus:outline-none hover:text-red-700", isMouseOver? 'visible': 'hidden'].join(' ')}>
      <Icon name="pencil" size={14} className={[].join(' ')}/>
    </button>}
  </div>
}

export default EditableLabel;