import React from "react";
import Icon from "./Icon";

const StatusButton = ({ status, iconOnly = false, iconSize = 14 }) => {

    const [icon, setIcon] = React.useState({
        class: '',
        name: '',
        iconColor: ''
    })

    React.useEffect(() => {
        switch (status) {
            case 'started':
                setIcon({
                    class: 'text-blue-500 border-blue-500',
                    name: 'info-circle'
                })
                break;
            case 'success':
                setIcon({
                    class: 'text-green-500 border-green-500',
                    name: 'check-circle'
                })
                break;
            case 'warning':
                setIcon({
                    class: 'text-yellow-500 border-yellow-500',
                    name: 'exclamation'
                })
                break;
            case 'error':
                setIcon({
                    class: 'text-red-500 border-red-500',
                    name: 'x-circle'
                })
                break;
            default:
                setIcon({
                    class: 'text-success-500 border-success-500',
                    name: 'info-circle'
                })
                break;
        }
    }, [status])

    if (iconOnly) {
        return <Icon name={icon.name} size={iconSize} className={icon.class} />
    }

    return <button className={`flex items-center rounded-xl px-1 border ${icon.class}`} style={{ width: '80px' }}>
        <Icon name={icon.name} size={iconSize} />
        <span className="pl-1">{status}</span>
    </button>
}

export default StatusButton;