import React, { useImperativeHandle, useState } from 'react';
import styled from 'styled-components';
import Icon from './Icon';

const LightBox = ({refId, className, url}) => {

    const [visible, setVisible] = useState(false);

    useImperativeHandle(refId, () => ({
        open() {
          setVisible(true);
        },
        hide() {
          setVisible(false);
        },
    }));
   
    return <Wp>
      <div className={["lightbox fixed top-0 right-0 left-0 bottom-0 bg-black-50 ",  visible ? "visible" : ""].join(' ')} style={{ zIndex: '9999' }}>
        <button className="absolute right-10 top-10 flex items-center text-gray-200 hover:text-white focus:outline-none" onClick={() => setVisible(false)}>
          <Icon name="x" size="35"/>
        </button>
        <div className="w-full h-full flex items-center justify-center">
          <div className="bg-white" style={{ width: '800px', height: '700px' }}>
             <img src={url} className=" object-cover" alt={url} />
          </div> 
        </div>
      </div>
    </Wp>
}

const Wp = styled.div`
.lightbox{
  visibility: collapse
}
.lightbox.visible{
  visibility: visible
}
`
 
export default LightBox;