import React from 'react'


const TableEmptyRow = ({ colspan = 0, className = null, children }) => {
    return (
        <tr>
            <td colSpan={colspan}>
                <div className={className}>{children}</div>
            </td>
        </tr >
    )
}

export default TableEmptyRow