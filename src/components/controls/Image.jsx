import React from 'react'

import imgph from '../../assets/img.svg'

export const Image = ({ placeholderImg, src = imgph, alt, className, ...props }) => {
  const [loaded, setLoaded] = React.useState(true);

  // eslint-disable-next-line jsx-a11y/alt-text
  return loaded ? <img {...props} src={src} alt={alt} onError={() => setLoaded(false)} onLoad={() => setLoaded(true)} /> : <img {...props} src={imgph} />;
}

export default Image
