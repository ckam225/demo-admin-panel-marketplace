import React from 'react';


const Paginator = ({
    pages = 10,
    setCurrentPage,
    itemClass,
    activeItemClass,
    className,
    onChange
}) => {

    const [currentButton, setCurrentButton] = React.useState(1)
    const [arrOfCurrButtons, setArrOfCurrButtons] = React.useState([])

    const numberOfPages = React.useMemo(() => {
        const numberOfPages = []
        for (let i = 1; i <= pages; i++) {
            numberOfPages.push(i)
        }
        return numberOfPages;
    }, [pages])


    React.useEffect(() => {
        let tempNumberOfPages = [...arrOfCurrButtons]

        let dotsInitial = '...'
        let dotsLeft = '... '
        let dotsRight = ' ...'

        if (numberOfPages.length < 10) {
            tempNumberOfPages = numberOfPages
        }
        else if (currentButton >= 1 && currentButton <= 5) {
            tempNumberOfPages = [1, 2, 3, 4, 5, 6, 7, 8, dotsInitial, numberOfPages.length]
        }
        else if (currentButton === 8) {
            const sliced = numberOfPages.slice(0, 9)
            tempNumberOfPages = [...sliced, dotsInitial, numberOfPages.length]
        }
        else if (currentButton > 8 && currentButton < numberOfPages.length - 4) {
            const sliced1 = numberOfPages.slice(currentButton - 4, currentButton)
            const sliced2 = numberOfPages.slice(currentButton, currentButton + 1)
            tempNumberOfPages = ([1, dotsLeft, ...sliced1, ...sliced2, dotsRight, numberOfPages.length])
        }
        else if (currentButton > numberOfPages.length - 5) {                 // > 7
            const sliced = numberOfPages.slice(numberOfPages.length - 8)       // slice(10-4) 
            tempNumberOfPages = ([1, dotsLeft, ...sliced])
        }
        else if (currentButton === dotsInitial) {
            setCurrentButton(arrOfCurrButtons[arrOfCurrButtons.length - 5] + 1)
        }
        else if (currentButton === dotsRight) {
            setCurrentButton(arrOfCurrButtons[5] + 4)
        }
        else if (currentButton === dotsLeft) {
            setCurrentButton(arrOfCurrButtons[5] - 4)
        }
        setArrOfCurrButtons(tempNumberOfPages)
        setCurrentPage(currentButton)
        // if (onChange) {
        //     onChange(currentButton)
        // }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentButton])



    return (
        <div className={className}>
            {currentButton > 1 ? <button
                className={[itemClass].join(' ')}
                onClick={() => setCurrentButton(prev => prev <= 1 ? prev : prev - 1)}
            >
                &#171;
            </button> : <span className={itemClass}></span>}

            {arrOfCurrButtons.map(((item, index) => {
                return <button
                    key={index}
                    className={[itemClass, currentButton === item ? activeItemClass : ''].join(' ')}
                    onClick={() => setCurrentButton(item)}
                >
                    {item}
                </button>
            }))}

            {currentButton < numberOfPages.length ? <button
                className={[itemClass].join(' ')}
                onClick={() => setCurrentButton(prev => prev >= numberOfPages.length ? prev : prev + 1)}
            >
                &#187;
            </button> : <span className={itemClass}></span>}
        </div>
    );
}

export default Paginator;