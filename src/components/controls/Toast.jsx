import React from 'react'
import Icon from './Icon'
import styled from 'styled-components'

const Wrapper = styled.div`
.toast {
  overflow: hidden;
  visibility: hidden;
  opacity: 0;
  bottom: -1000px;
  transition: all 0.8s ease-in-out;
}
.toast.show {
  visibility: visible;
  opacity: 1;
  bottom: 10px;
}
`
export const ToastContext = React.createContext({
    toast: {
        visible: false,
        show: () => { },
        success: () => { },
        warning: () => { },
        error: () => { },
        type: null,
        message: null
    }
})

const Types = {
    success: 'success',
    warning: 'warning',
    error: 'error',
}

const Toast = () => {

    const [visible, setVisible] = React.useState(false)
    const [message, setMessage] = React.useState()
    const [type, setType] = React.useState()

    const params = () => {
        switch (type) {
            case Types.success:
                return { color: "green", icon: "check-circle", encode: "&#10003;" };
            case Types.warning:
                return { color: "yellow", icon: "exclamation", encode: "&#9888;" };
            case Types.error:
                return { color: "red", icon: "x-circle", encode: "&#10006;" };
            default:
                return { color: "indigo", icon: "info-circle", encode: "&#8505;" };
        }
    }

    const show = (message, type = '') => {
        alert('osodsf')
        setVisible(true)
        setType(type)
        setMessage(message)
    }

    function success(message) {
        setVisible(true)
        setMessage(message)
        setType(Types.success)
    }

    function warning(message) {
        setVisible(true)
        setMessage(message)
        setType(Types.warning)
    }

    function error(message) {
        setVisible(true)
        setMessage(message)
        setType(Types.error)
    }

    function hide() {
        setVisible(false)
    }

    // React.useEffect(() => {
    //     if (visible) {
    //         setTimeout(() => {
    //             setVisible(false);
    //         }, 4000);
    //     }
    // }, [visible])

    return <Wrapper>
        <ToastContext.Provider value={{
            toast: { show, hide, success, warning, error, visible, type, message }
        }}>
            <div
                className={[
                    'toast fixed flex max-h-200 max-w-400 shadow-lg',
                    'bg-blue-fonce text-white right-0 bottom-0 m-5',
                    visible ? 'show' : null].join(' ')}
            >
                <div className={['border-2', `border-${params().color}-500`].join(' ')}></div>
                <div className="flex items-center py-5 pr-5">
                    <Icon className={['w-6 h-6 mx-2 flex-none', `text-${params().color}-500`].join(' ')}
                        name={params().icon}
                    />
                    <span className="flex-auto text-justify leading-5 px-3">{message}</span>
                    <span className="btn-md text-gray-400" onClick={hide}>&times;</span>
                </div>
            </div>
        </ToastContext.Provider>
    </Wrapper>
}

export default Toast;