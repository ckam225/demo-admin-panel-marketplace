import React from 'react'


const Textbox = ({ label, className, type = 'text', name, required = false, readOnly = false, placeholder = null, onChange }) => {
    const [value, setValue] = React.useState('')

    function handleChange(e) {
        setValue(e.target.value)
        if (onChange)
            onChange(e.target.value)
    }

    const focusClass = !readOnly ? 'focus:outline-none focus:ring-1 focus:ring-blue-500' : null

    return (
        <label className={"block " + className}>
            <span className="text-gray-700">{label}</span>
            <input
                type={type}
                className={"w-full border py-2 px-4 bg-white rounded-lg placeholder-gray-400 text-gray-900 appearance-none inline-block " + focusClass}
                placeholder={placeholder}
                name={name}
                readOnly={readOnly}
                required={required}
                onChange={handleChange}
                value={value}
            />
        </label>
    );
}

export default Textbox;