
import React from 'react';

const Context = React.createContext({ selected: '', setSelected: () => { } })

export const RadioButton = ({ children, value, name = null, className, activeClass = null }) => {

    const [radio, setRadio] = React.useState(value)
    const { selected, setSelected } = React.useContext(Context)


    function handlerChange(e) {
        setSelected(e.target.value)
        setRadio(e.target.value)
    }

    return (
        <label className={["inline-flex items-center", className, selected === value ? activeClass : null].join(' ')} >
            <input type="radio"
                name={name}
                style={{ width: '1.25rem', height: '1.25rem' }}
                checked={selected === value}
                value={radio}
                onChange={handlerChange} />
            {children}
        </label>
    )
}

const RadioGroup = ({ children, onChange, name, className, defaultValue = null }) => {
    const [selected, setSelected] = React.useState(defaultValue)

    React.useEffect(() => {
        if (onChange)
            onChange(selected)
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selected])

    return <div className={className}>
        <Context.Provider value={{ selected, setSelected }}>
            {children}
        </Context.Provider>
    </div>
}

RadioGroup.Option = RadioButton


export default RadioGroup