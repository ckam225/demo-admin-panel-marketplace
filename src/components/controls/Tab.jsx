import React from 'react'
import styled from 'styled-components';

const TabContext = React.createContext({ currentTab: 0, setCurrentTab: () => { }, onTabItemChange: () => { } })

const TabHeaders = ({ children, className }) => {
    return (<div className={['tab__header', className].join(' ')}>
        {children}
    </div>);
}

const TabHeader = ({ children, tab = 0, className, activeClass = "active" }) => {
    const { currentTab, setCurrentTab, onTabItemChange } = React.useContext(TabContext)
    return (<div className={['tab_header_item', className, tab === currentTab ? activeClass : ''].join(' ')}
        onClick={() => {
            setCurrentTab(tab)
            if (onTabItemChange)
                onTabItemChange(tab)
        }
        }>
        {children}
    </div>);
}

const TabItem = ({ children, tab = 0 }) => {
    const { currentTab } = React.useContext(TabContext)
    if (tab === currentTab)
        return <>{children}</>
    return null
}

const TabContent = ({ children, className }) => {
    // const { currentTab } = React.useContext(TabContext)
    // return (<div className={className}>{children[currentTab]}</div>);
    return (<div className={className}>{children}</div>);
}

const Tab = ({ children, onTabItemChange }) => {
    const [currentTab, setCurrentTab] = React.useState(0)
    return <TabContext.Provider value={{ currentTab, setCurrentTab, onTabItemChange }}>
        <Wrapper>{children}</Wrapper>
    </TabContext.Provider>
}

Tab.Headers = TabHeaders
Tab.Header = TabHeader
Tab.Content = TabContent
Tab.Item = TabItem

const Wrapper = styled.div`

.tab__header{
  background: #fff;
  display: flex;
  border-bottom: 1px solid #ccc;
}
.tab_header_item{
    font-size: 14px;
    font-style: normal;
    font-weight: 300;
    padding: 15px 10px;
    border-bottom: 2px solid transparent;
    color: rgba(0, 0, 0, 0.6)
}
.tab_header_item:hover{
    color: var(--color-accent);
    cursor: pointer;
}
.tab_header_item.active{
    color: var(--color-accent);
    border-color: var(--color-accent);
}
.link__sync::hover{
    color: var(--color-accent);
    text-decoration: underline;
}

`
export default Tab;