import React, { useImperativeHandle, useRef } from "react";
import styled from 'styled-components'

const Context = React.createContext({ closable: true, close: () => { } })


const Header = ({ children }) => {

  const { closable, close } = React.useContext(Context)

  return <div className="modal-header">
    <div className="modal-title">{children}</div>
    {closable && (
      <span className="modal-close" onClick={(e) => {
        e.stopPropagation()
        close()
      }}>
        &times;
      </span>
    )}
  </div>
}

const Content = ({ children }) => {
  return <div className="modal-body">{children}</div>
}

const Dialog = ({ children, width, refId, className, closable = true, dismissible = true }) => {
  const modal = useRef(null);

  function handleDismiss(e) {
    if (dismissible && e.target.id && e.target.id === 'modal-overlay') {
      close()
    }
  }

  function close() {
    modal.current.classList.remove("open");
    modal.current.classList.add("hidden");
    setTimeout(() => {
      modal.current.classList.remove("hidden");
    }, 200);
  };

  useImperativeHandle(refId, () => ({
    open() {
      modal.current.classList.add("open");
    },
    close() {
      close();
    },
  }));

  return <Wrapper>
    <Context.Provider value={{ close, closable }}>
      <div className="modal" ref={modal}>
        <div className="modal-overlay" onClick={handleDismiss} id="modal-overlay">
          <div className={"modal-content shadow-2xl " + className} style={{ width: `${width || 600}px` }} id="modal-content">
            {children}
          </div>
        </div>
      </div>
    </Context.Provider>
  </Wrapper>
};

Dialog.Header = Header
Dialog.Content = Content

const Wrapper = styled.div`

/* MODAL */
.modal.open .modal-overlay,
.modal.open .modal-content {
  opacity: 1;
  z-index: 1000;
}
.modal.open .modal-overlay {
  background: rgba(0, 0, 0, 0.4);
}
.modal.open .modal-content {
  transform: translateY(100px);
}

.modal.hidden .modal-overlay,
.modal.hidden .modal-content {
  opacity: 1;
  z-index: 1000;
}

.modal.hidden .modal-overlay {
  background: rgba(0, 0, 0, 0);
}
.modal.hidden .modal-content {
  transform: translateY(-200px);
}

.modal-overlay,
.modal-content {
  opacity: 0;
  z-index: -1;
}
.modal-overlay {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0);
  transition: background 0.3s ease-in-out;
}
.modal-content {
  background: #fff;
  margin: 0 auto;
  border-radius: 5px;
  transform: translateY(-200px);
  transition: all 0.3s ease-in-out;
}
.modal-header {
  padding: 10px 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid #eee;
}
.modal-title {
  font-size: 1.5rem;
}
.modal-close {
  font-size: 1.5rem;
  cursor: pointer;
}
.modal-body {
 
}
`
export default Dialog