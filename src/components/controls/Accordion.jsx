import React from "react";
import styled from 'styled-components'

const Context = React.createContext({
    active: false,
    toggle: () => { }
});

const Header = ({ children, className, ...rest }) => {
    const { toggle } = React.useContext(Context)
    return <div className={["accordion", className].join(" ")} {...rest} onClick={toggle}>
        {children}
    </div>
}

const Content = ({ children }) => {
    const { active } = React.useContext(Context)
    return <>
        {active && <div className={["accordion-panel"].join(' ')} >
            {children}
        </div>
        }
    </>
}


const Accordion = ({ children, className, ...rest }) => {
    const [active, setActive] = React.useState(false)

    function toggleActive() {
        setActive(!active)
    }

    return <Context.Provider value={{ active: active, toggle: toggleActive }}> <Wrapper className={className} {...rest}>
        {children}
    </Wrapper>
    </Context.Provider>
}

Accordion.Header = Header
Accordion.Content = Content


const Wrapper = styled.div /*css*/`
.accordion {
    cursor: pointer;
    outline: none;
    transition: 0.4s;
}

.accordion-panel {
    overflow: hidden;
    transition: height 0.4s ease-in-out;
}
`

export default Accordion;