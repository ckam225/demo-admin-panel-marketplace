import React from 'react';


const NumberField = ({ ...rest }) => {


    return <input {...rest} onInput={(e) => {
        e.currentTarget.value = e.currentTarget.value.replace(/[^0-9.]/g, '');
        e.currentTarget.value = e.currentTarget.value.replace(/(\..*)\./g, '$1');
    }} />
}

export default NumberField;