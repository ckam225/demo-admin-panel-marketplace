
import ReactMarkdown from 'react-markdown'
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { dracula } from 'react-syntax-highlighter/dist/esm/styles/prism'
import React from 'react'

const components = {
    code({ node, inline, className, children, ...props }) {
        const match = /language-(\w+)/.exec(className || '')
        return !inline && match ? (
            <SyntaxHighlighter style={dracula} language={match[1]} PreTag="div" children={String(children).replace(/\n$/, '')} {...props} />
        ) : (
            <code className={className} {...props} />
        )
    }
}

const Markdown = ({ markdown, title = '', tag = 'bash' }) => {

    const [mdown, setMdown] = React.useState()

    React.useLayoutEffect(() => {
        const markd = `${title}
~~~${tag}
${markdown}
~~~
        `
        setMdown(markd)
    }, [markdown])


    return <ReactMarkdown components={components} children={mdown} />
}

export default Markdown;