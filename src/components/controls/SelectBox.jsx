import React from 'react';
import { Menu, Transition } from '@headlessui/react'
import SearchBox from './SearchBox';


const Combox = ({ items = [], placeholder = '-- select --', displayField = 'name', onChange, className, contentClass, style, waterMark = 'No select', onSearch=null }) => {
    const [selectedItem, setSelectedItem] = React.useState(placeholder)
    const [selected, setSelected] = React.useState(false)
    const [cs, setCs] = React.useState("")


    function normalized(item) {
        if (typeof displayField === 'function') {
            return displayField(item)
        } else {
            return typeof item === 'object' ? item[displayField] : item
        }
    }

    const normalizedItems = React.useMemo(() => {
        return items.filter(item => normalized(item)?.toLowerCase().match(cs?.toLowerCase()))
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [items, cs])

    function handleActive() {
        setSelected(!selected)
    }

    function handleSelect(value) {
        if (value !== placeholder) {
            setSelectedItem(value)
            handleActive()
            if (onChange)
                onChange(value)
        }
    }

    function handleSearchChange(v) {
        setCs(v)
        if (onSearch)
            onSearch(v)
    }


    return (
        <div className="relative">
            <Menu>
                {({ open }) => (
                    <>
                        <Menu.Button className={
                            ["inline-flex justify-between w-full text-sm font-medium leading-5 text-gray-700 transition duration-150 ease-in-out bg-white border border-gray-300  hover:text-gray-500 focus:outline-none focus:border-blue-500 focus:shadow-outline-blue active:bg-gray-50 active:text-gray-800",
                                className
                            ].join(' ')} style={style}>
                            <span>{normalized(selectedItem)}</span>
                            <svg
                                className="w-5 h-5 ml-2 -mr-1"
                                viewBox="0 0 20 20"
                                fill="currentColor"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                                    clipRule="evenodd"
                                />
                            </svg>

                        </Menu.Button>
                        <Transition
                            show={open}
                            enter="transition ease-out duration-100"
                            enterFrom="transform opacity-0 scale-95"
                            enterTo="transform opacity-100 scale-100"
                            leave="transition ease-in duration-75"
                            leaveFrom="transform opacity-100 scale-100"
                            leaveTo="transform opacity-0 scale-95"
                        >
                            <Menu.Items
                                static
                                className={["dp absolute right-0 left-0  mt-1 bg-white border border-gray-200 divide-y divide-gray-100 shadow-lg outline-none", contentClass].join(' ')}

                            >
                                {/* <div className="flex items-center pr-1 border focus-within:border-gray-300 m-2">
                                    <input type="text"
                                        className="ring-0 focus:ring-0 border-0 py-1 rounded-sm"
                                        placeholder="" 
                                        value={cs}
                                        onChange={(e) => setCs(e.target.value)}
                                        />
                                    {cs.length > 0 ? <svg className="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" onClick={() => setCs('')}>
                                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                    </svg> : 
                                    <svg className="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                       <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                                    </svg>}
                                </div> */}

                                <SearchBox onChange={handleSearchChange} placeholder="" className="py-1 mx-2 my-1 rounded-sm text-sm" />

                                <div className="py-1" style={{ maxHeight: '300px', overflowY: 'auto' }}>
                                    <Menu.Item>
                                        <div className="px-4 py-2 text-gray-400" onClick={() => {
                                            handleSelect()
                                            setSelectedItem(placeholder)
                                        }}>{waterMark}</div>
                                    </Menu.Item>
                                    {normalizedItems.map((item, e) => {

                                        return <Menu.Item key={e} >
                                            {({ active }) => (<div className={`${active
                                                ? "bg-gray-100 text-gray-900 cursor-pointer"
                                                : "text-gray-700"
                                                } flex justify-between w-full px-4 py-2 leading-5 text-left text-sm ${selectedItem === item ? 'font-bold' : ''}`} onClick={() => handleSelect(item)}>
                                                {normalized(item)}
                                            </div>)}
                                        </Menu.Item>
                                    })}

                                </div>
                            </Menu.Items>
                        </Transition>
                    </>
                )}
            </Menu>
        </div>
    );
}

export default Combox;