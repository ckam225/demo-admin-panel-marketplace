import React from "react";
import styled from 'styled-components'


export const CircleLoader = ({ className }) => {

  return <Wrapper2>
    <div className={["spinner-wrapper", className].join(' ')}>
      <svg className="spinner" viewBox="0 0 50 50">
        <circle className="path" cx="25" cy="25" r="20" fill="none" strokeWidth="5"></circle>
      </svg>
    </div>
  </Wrapper2>
}


const Loader = ({
  message,
  size,
  orientation,
  align,
  valign,
  isFull = false,
}) => {
  const getSize = size ? `${size}px` : "40px";
  const verticalAlign = valign ? valign : "center";
  return (
    <Wrapper><div
      className={`progress-content${isFull ? " full" : ""}`}
      style={
        orientation === "vertical"
          ? { flexDirection: "column", justifyContent: "center" }
          : {
            flexDirection: "row",
            alignItems: verticalAlign,
            justifyContent: align,
          }
      }
    >
      <div
        className="progress-bar"
        style={{ width: getSize, height: getSize }}
      />
      {message && <span className="progress-msg">{message}</span>}
    </div>
    </Wrapper>
  );
};



const Wrapper2 = styled.div`

  
  .spinner-wrapper{
    position: relative;
  }
  .spinner{
    position:absolute;
    top: 50%;
    left: 50%;
    margin: -25px 0 0 -25px;
    width: 100%;
    height: 100%;
    z-index: 2;
    animation: rotate 2s linear infinite;
  }
  .spinner .path {
    stroke: var(--color-accent);
    stroke-linecap: round;
    animation: dash 1.5s ease-in-out infinite;
  }

  @keyframes dash {
    0%{
      stroke-dasharray: 1, 150;
      stroke-dashoffset: 0;
    }
    50%{
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -35;
    }
    100%{
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -124;
    }
  }

  @keyframes rotate {
    100%{
      transform: rotate(360deg);
    }
  }
`

const Wrapper = styled.div`
.progress-content {
  display: flex;
}
.progress-content.full {
  z-index: 9999;
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.3);
  display: flex;
  align-items: center;
  justify-content: center;
}
.progress-content.full .progress-msg {
  color: #fff;
  font-size: 18px;
}
.progress-bar {
  border: 3px solid #fff;
  border-radius: 50%;
  border-top: 3px solid var(--color-accent);
  animation: spin 0.8s linear infinite;
}
.progress-msg {
  margin: 5px;
  font-size: 12px;
}

@keyframes spin {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

`
export default Loader