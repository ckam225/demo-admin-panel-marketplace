
const Bounce = ({ className='w-2 h-2'}) => {
    return <div className={[ className, 'rounded-full'].join(' ')}></div>
}
 
export default Bounce;