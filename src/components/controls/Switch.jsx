import React from 'react';
import { Switch as BaseSwitch } from "@headlessui/react";

const Switch = ({ children, checked, onChange }) => {
    return <BaseSwitch.Group>
        <div className="flex items-center">
            <BaseSwitch.Label>{children}</BaseSwitch.Label>
            <BaseSwitch
                checked={checked}
                onChange={onChange}
                className={`${checked ? 'bg-blue-accent' : 'bg-gray-200'
                    } relative inline-flex items-center h-6 rounded-full w-11 transition-colors focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-accent`}
            >
                <span
                    className={`${checked ? 'translate-x-6' : 'translate-x-1'
                        } inline-block w-4 h-4 transform bg-white rounded-full transition-transform`}
                />
            </BaseSwitch>
        </div>
    </BaseSwitch.Group>
}

export default Switch;