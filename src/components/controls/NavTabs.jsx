import React from 'react'
import styled from 'styled-components'
import { BaseLink } from '../shared/NavItem';

const NavTabs = ({routes=[]}) => {
    return <Wrapper>
        <div className="mt-5 mx-4 bg-white flex border-b">
            {routes.map((route, i) => (
                <BaseLink key={i} to={`${route.path}`} activeClass="active"
                    className="tab-item-link" exact={route.exact}>
                    {route.title}
                </BaseLink>
            ))}
        </div>
</Wrapper>
}

const Wrapper = styled.div`
.tab-item-link {
  padding-bottom: 0.5rem;
  border-bottom-width: 2px;
  border-bottom-color: transparent;
  margin-right: 1.5rem;
}
.tab-item-link.active {
  border-color: rgba(96, 165, 250, 1);
  color: rgb(8, 95, 201);
}
 `
export default NavTabs;