import React from 'react'
import Dp, { registerLocale } from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import ru from 'date-fns/locale/ru'

registerLocale('ru', ru)

export const DatePicker = ({ onChange, className, placeholder, format = "dd.MM.yyyy" }) => {
    const [value, setValue] = React.useState()
    const handleClear = () => {
        setValue(null)
        onChange(null)
    }
    return <div className="flex items-center border focus-within:border-blue-600 ">
        <Dp selected={value}
            onChange={(date) => {
                setValue(date)
                onChange(date)
            }}
            locale="ru"
            placeholderText={placeholder}
            className={["py-2 rounded-sm bg-transparent text-sm focus:ring-0 border-0 h-full w-36", className].join(' ')}
            dateFormat={format} />
        {value && <button className="cursor-pointer focus:outline-none mx-1 text-sm font-bold" onClick={handleClear}>&times;</button>}
    </div>
}

export default DatePicker;