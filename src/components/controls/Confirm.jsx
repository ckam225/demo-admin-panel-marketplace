import React from 'react'
import Dialog from './Dialog';

const Confirm = ({ children, refId, actions = [], dismissible = true, className }) => {
    const dialog = React.useRef(null);

    React.useImperativeHandle(refId, () => ({
        open() {
            dialog.current.open();
        },
        close() {
            dialog.current.close()
        }
    }));

    return <Dialog refId={dialog} width="350" dismissible={dismissible} className={className}>
        <Dialog.Content>
            <div>{children}</div>
            <div className="bg-blue-50 flex p-5">
                {actions.map((action, index) => {
                    return <button
                        key={index}
                        className={"py-2 flex-1  button " + action.className}
                        onClick={() => {
                            action.click()
                            dialog.current.close()
                        }}
                    >
                        {`${action.label}`.toUpperCase()}
                    </button>
                })}
            </div>
        </Dialog.Content>
    </Dialog>
}

const initialValue = {
    title: '',
    message: '',
    action: () => { }
}
export const useConfirm = (option = initialValue) => {

    const confirmBox = React.useRef()
    const [confirmData, setConfirmData] = React.useState(option)

    function showDialog(option = initialValue) {
        setConfirmData(option)
        confirmBox.current.open()
    }

    const Provider = () => (
        <Confirm refId={confirmBox} dismissible={false} actions={[
            {
                label: 'Cancel',
                click: () => { }
            }, {
                label: 'Confirm',
                className: 'text-red-500',
                click: confirmData.action
            }
        ]}>
            <div className="text-center font-bold pt-8">
                {confirmData.title}
            </div>
            <div className="text-center my-4">
                {confirmData.message}
            </div>
        </Confirm>
    )

    return {
        Provider: Provider,
        showDialog
    }

}

export default Confirm;