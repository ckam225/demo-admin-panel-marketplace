import React from 'react'
import styled from 'styled-components'



const Select = ({ items = [], onChange, placeholder = '-- select --', displayField = 'name', className, style}) => {
    const [selected, setSelected] = React.useState(placeholder)
    const [active, setActive] = React.useState(false)
    // eslint-disable-next-line no-unused-vars
    const [focus, setFocus] = React.useState(false)
    const [cs, setCs] = React.useState()

    const normalizedItems = React.useMemo(() => {
        return items.filter(item => normalized(item).toLowerCase().match(cs?.toLowerCase()))
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [items, cs])

    // React.useEffect(() => {
    //     if (active)
    //         setTimeout(() => document.addEventListener('click', handleActive), 200)
    //     return () => {
    //         if (!focus)
    //         setTimeout(() => document.removeEventListener('click', handleActive), 200)
    //     }
    // }, [active, focus])

    function normalized(item) {
        return typeof item === 'object' ? item[displayField] : item
    }

    function handleActive() {
        setActive(!active)
    }

    function handleSelect(value) {
        if (value !== placeholder) {
            setSelected(value)
            handleActive()
            if (onChange)
                onChange(value)
        }
    }

    const classes = active ? "border-blue-500" : "border-gray-300"
    const placeClass = selected === placeholder ? "text-gray-400" : null

    return (
        <Wrapper className="relative m-2">
            <div onClick={handleActive} style={style} className={["relative w-full bg-white border text-left cursor-default  sm:text-sm ", classes, className].join(' ')}>
                <span className="flex items-center">
                    <span className={["ml-3 block truncate ", placeClass].join(' ')}>{normalized(selected)}</span>
                </span>
                <span className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                    <svg
                        className="w-5 h-5 ml-2 -mr-1"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                    >
                        <path
                            fillRule="evenodd"
                            d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
                            clipRule="evenodd"
                        />
                    </svg>
                </span>
            </div>
            {active && <div className="absolute transition duration-150 ease-in-out z-10 w-full bg-white 
            shadow-lg  rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                <div className="flex items-center pr-1 border focus-within:border-gray-300 m-2">
                    <input type="text"
                        className="ring-0 focus:ring-0 border-0 py-1 rounded-sm"
                        value={cs}
                        onChange={(e) => setCs(e.target.value)}
                        onFocus={() => setFocus(true)}
                        onBlur={() => setFocus(false)}
                        placeholder="" />
                    <svg className="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                    </svg>
                </div>
                <div className="border-b"></div>
                <div className="max-h-56">
                    {normalizedItems.map((item, e) => {
                        return <div key={e} className="flex justify-between w-full px-4 py-2 text-sm leading-5 text-left 
                        text-gray-700 hover:bg-blue-500 hover:text-white" onClick={() => handleSelect(item)}>
                            {normalized(item)}
                        </div>
                    })}
                </div>
            </div>}
        </Wrapper>
    )
}

const Wrapper = styled.div`

`

export default Select;