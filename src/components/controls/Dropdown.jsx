import React from 'react'


const Context = React.createContext({ visible: false, setVisible: () => { } })

const Toggle = ({ children, className, onClick = null }) => {
    const { visible, setVisible } = React.useContext(Context)

    function handleToggle() {
        setVisible(!visible)
        if (onClick)
            onClick()
    }

    React.useEffect(() => {
        if (visible)
            setTimeout(() => document.addEventListener('click', handleToggle), 200)
        return () => {
            setTimeout(() => document.removeEventListener('click', handleToggle), 200)
        }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [visible])


    return <div className={className}
        onClick={handleToggle}>
        {children}
    </div>
}

const Content = ({ children, className }) => {
    const { visible } = React.useContext(Context)
    return visible ? <div className={className}> {children}</div> : null
}

const Item = ({ children, className, onClick }) => {
    const { visible, setVisible } = React.useContext(Context)
    const handleClick = () => {
        if (onClick)
            onClick()
        setVisible(!visible)
    }
    return <div className={className} onClick={handleClick}>{children}</div>
}

const Dropdown = ({ children, className }) => {
    const [visible, setVisible] = React.useState(false)

    return <div className={className}>
        <Context.Provider value={{ visible, setVisible }}>
            {children}
        </Context.Provider>
    </div>
}

Dropdown.Toggle = Toggle
Dropdown.Content = Content
Dropdown.Item = Item

export default Dropdown