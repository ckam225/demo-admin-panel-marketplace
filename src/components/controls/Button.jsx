import React from 'react'


const Button = ({ children, type = 'button' }) => {
    return (
        <button type={type}
            className="transition duration-150 ease-in-out py-2 px-4 text-white font-semibold rounded-lg shadow-md bg-green-500 active:bg-green-700 focus:outline-none"
        >
            {children}
        </button>
    );
}

export default Button;