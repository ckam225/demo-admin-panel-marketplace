import React from 'react'


const SearchBox = ({ label, className, type = 'text', name, required = false, readOnly = false, placeholder = null, onChange, inputClass='py-1 px-4 bg-white' }) => {
    const [value, setValue] = React.useState('')
    // eslint-disable-next-line no-unused-vars
    const [focused, setFocused] = React.useState(false)

    function handleChange(e) {
        setValue(e.target.value)
        if (onChange)
            onChange(e.target.value)
    }

    function handleFocus(e) {
      setFocused(true)  
    }

    function handleBlur(e) {
        setFocused(false)  
      }

    const focusClass = !readOnly ? 'focus:outline-none focus:ring-0 focus:ring-indigo-500' : null

    return (
        <div className={"relative text-gray-400 focus-within:text-gray-600  " + className}>
            
            <input
                type={type}
                className={['w-full border   ring-0   placeholder-gray-400 text-gray-900 appearance-none inline-block', inputClass
                 , focusClass].join(' ') }
                placeholder={placeholder}
                name={name}
                readOnly={readOnly}
                required={required}
                onChange={handleChange}
                onFocus={handleFocus}
                onBlur={handleBlur}
                value={value}
            />
            {value.length > 0 ? <div className="absolute inset-y-0 right-0 pr-2 flex items-center hover:text-gray-700 cursor-pointer" onClick={() => setValue("")}>
                <svg className="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                   <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M6 18L18 6M6 6l12 12"></path>
                </svg>
            </div> :
            <div className="absolute inset-y-0 right-0 pr-2 flex items-center pointer-events-auto">
                <svg className="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
                </svg>
            </div>}
        </div>
    );
}

export default SearchBox;