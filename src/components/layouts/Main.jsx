import React from 'react'
import Sidebar from '../shared/Sidebar'
// import Topbar from '../shared/Topbar'
import { Head } from './Base'

const Container = ({ children, className, onScroll = null }) => {
    return <div className="flex flex-row w-screen h-screen overflow-hidden">
        <Sidebar />
        <div className="flex flex-col flex-auto overflow-y-auto">
            {/* <Topbar /> */}
            <div className={["flex flex-col", className].join(" ")} onScroll={onScroll}>
                {children}
            </div>
        </div>
    </div>
}
const MainLayout = ({ children }) => {
    return <>{children}</>
}
MainLayout.Head = Head
MainLayout.Container = Container

export default MainLayout