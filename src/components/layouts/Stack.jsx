import React from 'react'
import '../../assets/main.css'
import Logo from '../../assets/logo.svg'
import { Head } from './Base'

const Container = ({ children }) => {
    return (
        <div className="stack-layout bg-blue-fonce">
            <div className="container">
                <img src={Logo} alt="logo" className="logo" />
                <h1 className="title">Маркетплейс/Админка</h1>
                {children}
            </div>
        </div>
    )
}

const StackLayout = ({ children }) => {
    return <>{children}</>
}

StackLayout.Head = Head
StackLayout.Container = Container

export default StackLayout