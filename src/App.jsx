import { RecoilRoot } from "recoil";
import Router from "./router";
import {ToastContainer} from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';
import { useEffect } from "react";
import Echo from "laravel-echo";
import { bearerToken } from './api/base';

window.io = require("socket.io-client");

function App() {

  useEffect(() => {
    setTimeout(() => {
      if (typeof window.io !== "undefined" && bearerToken) {
        window.Echo = new Echo({
          broadcaster: "socket.io",
          host: process.env.REACT_APP_WEBSOCKET_URL,
          disableStats: false,
          auth: {
            headers: {
              Authorization: bearerToken
            }
          }
        });
      }
     }, 0) 
  })

  
  return (
    <RecoilRoot>
      <Router />
      <ToastContainer
        position="bottom-right"
        />
    </RecoilRoot>
  );
}

export default App;