

/**
 *
 * @param {*} array Array to paginate
 * @param {*} page_size  number of row to display
 * @param {*} page_number current page number
 */
export function paginate(array, page_size, page_number) {
    return array.slice((page_number - 1) * page_size, page_number * page_size);
}


export function range(size, startAt = 0) {
    return [...Array(size).keys()].map(i => i + startAt);
}

export function isNumeric(value) {
    return /^\d+$/.test(value)
}

export function isValidImage(
    file,
    maxSize,
    extensions = ["jpeg", "jpg", "png", "gif", "webp", "deb"]
) {
    if (extensions.indexOf(file.name.split(".")[1]) === -1) {
        console.error(`File is not image: (allowed=${extensions.join("|")} )`);
        return false;
    }
    const size = file.size / 1024 / 1024;
    if (size > maxSize) {
        console.error("File is to big: maximum size", `${maxSize} M`);
        return false;
    }
    return true;
}

export function endOt(str, max) {
    if (str && str.length > max) {
        const res = str.slice(0, max + 1);
        return res + "...";
    }
    return str;
}

export const currency = new Intl.NumberFormat('ru-RU', {
    style: 'currency',
    currency: 'RUB'
});


export const russianDays = ['понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье'];


export function downloadURI(uri = "", name = '') {
    const link = document.createElement("a");
    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}


