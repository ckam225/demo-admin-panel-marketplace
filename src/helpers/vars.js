

export const FORMOWNERSHIPS = [
    'ИП', 'АО', 'ООО', 'ПАО', 'ПК', 'УП', 'ФБГНУ', 'НКО', 'ЗАО', 'ОАО', 'НАО', 'АНО', 'Самозанятый'
]

export const TAXSYSTEMS = ['ОСНО', 'УСН']