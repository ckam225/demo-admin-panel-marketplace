## Админка Маркетплейса

#### Требовния к окружению
- NodeJS: 14

#### Переменные окружения

```bash 
cp .env.axample .env.local
```

```bash
## marketplace api url
REACT_APP_API_URL=http://localhost
## set environment mode: dev, production or stage
REACT_APP_MODE=dev
## marketplace websocket server url
REACT_APP_WEBSOCKET_URL=http://localhost:6001
```

